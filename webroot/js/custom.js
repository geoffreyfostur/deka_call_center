$(document).ready(function(){
	$('.datatable').DataTable({
		"lengthMenu": [[50, 100, 250, -1], [50, 100, 250, "All"]],
        'ordering': false
	});

    $('.datatable_customers').DataTable({
        'ordering': false,
        'paging': false,
        "scrollY":"500px",
        "scrollCollapse": true,
    });

    $('.datatable_show_all').DataTable({
        "paging": false,
        'ordering': false,
        "sScrollY": ($(window).height() - 400),
    });

    $('.datatable_show_all_edit').DataTable({
        'ordering': false,
    });

	$('.datatable_caravanes').DataTable({
		"ordering" : true,
        "lengthMenu": [[50, 100, 250, -1], [50, 100, 250, "All"]]
	});


    $('.price_edit').on('focusin', function(){
        console.log("Saving value " + $(this).val());
        $(this).data('old_value', $(this).val());
    });

    // $('.selectpicker').selectpicker();

    $(".price_edit").change(function(){
        var price = $(this).val();
        var product_id = $(this).parent().parent().find('.product_id').val();
        var token =  $('input[name="_csrfToken"]').val();
        if(price != ''){
            $.ajax({
                 url : ROOT_DIRECTORY+'/products/price',
                 type : 'POST',
                 data : {price : price, id : product_id},
                 headers : {
                    'X-CSRF-Token': token 
                 },
                 success : function(data, statut){

                 },
                 error : function(resultat, statut, erreur){
                  console.log(erreur)
                 }, 
                 complete : function(resultat, statut){
                    console.log(resultat)
                 }
            });
        }else{
            alert('Précisez un prix pour ce produit');
            $(this).val($(this).data('old_value'));
        }
        
    })
})
