<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Controller\CaravanesController;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\CaravanesController Test Case
 *
 * @uses \App\Controller\CaravanesController
 */
class CaravanesControllerTest extends TestCase
{
    use IntegrationTestTrait;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Caravanes',
        'app.Users',
        'app.Centers',
        'app.Calls',
        'app.Sales',
        'app.Categories',
        'app.Customers',
        'app.Zones',
        'app.CategoriesCaravanes',
        'app.CustomersCaravanes',
        'app.ZonesCaravanes',
    ];

    /**
     * Test index method
     *
     * @return void
     * @uses \App\Controller\CaravanesController::index()
     */
    public function testIndex(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     * @uses \App\Controller\CaravanesController::view()
     */
    public function testView(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     * @uses \App\Controller\CaravanesController::add()
     */
    public function testAdd(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     * @uses \App\Controller\CaravanesController::edit()
     */
    public function testEdit(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     * @uses \App\Controller\CaravanesController::delete()
     */
    public function testDelete(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
