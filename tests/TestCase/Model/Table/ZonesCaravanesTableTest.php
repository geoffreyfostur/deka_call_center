<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ZonesCaravanesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ZonesCaravanesTable Test Case
 */
class ZonesCaravanesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ZonesCaravanesTable
     */
    protected $ZonesCaravanes;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.ZonesCaravanes',
        'app.Zones',
        'app.Caravanes',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('ZonesCaravanes') ? [] : ['className' => ZonesCaravanesTable::class];
        $this->ZonesCaravanes = $this->getTableLocator()->get('ZonesCaravanes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->ZonesCaravanes);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\ZonesCaravanesTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\ZonesCaravanesTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
