<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ArticlesSalesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ArticlesSalesTable Test Case
 */
class ArticlesSalesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ArticlesSalesTable
     */
    protected $ArticlesSales;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.ArticlesSales',
        'app.ProductsSales',
        'app.Sales',
        'app.Articles',
        'app.Promotions',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('ArticlesSales') ? [] : ['className' => ArticlesSalesTable::class];
        $this->ArticlesSales = $this->getTableLocator()->get('ArticlesSales', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->ArticlesSales);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\ArticlesSalesTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\ArticlesSalesTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
