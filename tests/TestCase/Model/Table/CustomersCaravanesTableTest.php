<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CustomersCaravanesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CustomersCaravanesTable Test Case
 */
class CustomersCaravanesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\CustomersCaravanesTable
     */
    protected $CustomersCaravanes;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.CustomersCaravanes',
        'app.Caravanes',
        'app.Customers',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('CustomersCaravanes') ? [] : ['className' => CustomersCaravanesTable::class];
        $this->CustomersCaravanes = $this->getTableLocator()->get('CustomersCaravanes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->CustomersCaravanes);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\CustomersCaravanesTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\CustomersCaravanesTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
