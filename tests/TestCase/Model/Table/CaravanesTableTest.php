<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CaravanesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CaravanesTable Test Case
 */
class CaravanesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\CaravanesTable
     */
    protected $Caravanes;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Caravanes',
        'app.Users',
        'app.Centers',
        'app.Calls',
        'app.Sales',
        'app.Categories',
        'app.Customers',
        'app.Zones',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Caravanes') ? [] : ['className' => CaravanesTable::class];
        $this->Caravanes = $this->getTableLocator()->get('Caravanes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Caravanes);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\CaravanesTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\CaravanesTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
