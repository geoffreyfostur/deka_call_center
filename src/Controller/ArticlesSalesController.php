<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * ArticlesSales Controller
 *
 * @property \App\Model\Table\ArticlesSalesTable $ArticlesSales
 * @method \App\Model\Entity\ArticlesSale[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ArticlesSalesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['ProductsSales', 'Sales', 'Articles', 'Promotions'],
        ];
        $articlesSales = $this->paginate($this->ArticlesSales);

        $this->set(compact('articlesSales'));
    }

    /**
     * View method
     *
     * @param string|null $id Articles Sale id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $articlesSale = $this->ArticlesSales->get($id, [
            'contain' => ['ProductsSales', 'Sales', 'Articles', 'Promotions'],
        ]);

        $this->set(compact('articlesSale'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $articlesSale = $this->ArticlesSales->newEmptyEntity();
        if ($this->request->is('post')) {
            $articlesSale = $this->ArticlesSales->patchEntity($articlesSale, $this->request->getData());
            if ($this->ArticlesSales->save($articlesSale)) {
                $this->Flash->success(__('The articles sale has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The articles sale could not be saved. Please, try again.'));
        }
        $productsSales = $this->ArticlesSales->ProductsSales->find('list', ['limit' => 200]);
        $sales = $this->ArticlesSales->Sales->find('list', ['limit' => 200]);
        $articles = $this->ArticlesSales->Articles->find('list', ['limit' => 200]);
        $promotions = $this->ArticlesSales->Promotions->find('list', ['limit' => 200]);
        $this->set(compact('articlesSale', 'productsSales', 'sales', 'articles', 'promotions'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Articles Sale id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $articlesSale = $this->ArticlesSales->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $articlesSale = $this->ArticlesSales->patchEntity($articlesSale, $this->request->getData());
            if ($this->ArticlesSales->save($articlesSale)) {
                $this->Flash->success(__('The articles sale has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The articles sale could not be saved. Please, try again.'));
        }
        $productsSales = $this->ArticlesSales->ProductsSales->find('list', ['limit' => 200]);
        $sales = $this->ArticlesSales->Sales->find('list', ['limit' => 200]);
        $articles = $this->ArticlesSales->Articles->find('list', ['limit' => 200]);
        $promotions = $this->ArticlesSales->Promotions->find('list', ['limit' => 200]);
        $this->set(compact('articlesSale', 'productsSales', 'sales', 'articles', 'promotions'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Articles Sale id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $articlesSale = $this->ArticlesSales->get($id);
        if ($this->ArticlesSales->delete($articlesSale)) {
            $this->Flash->success(__('The articles sale has been deleted.'));
        } else {
            $this->Flash->error(__('The articles sale could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
