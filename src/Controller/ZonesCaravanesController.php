<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * ZonesCaravanes Controller
 *
 * @property \App\Model\Table\ZonesCaravanesTable $ZonesCaravanes
 * @method \App\Model\Entity\ZonesCaravane[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ZonesCaravanesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Zones', 'Caravanes'],
        ];
        $zonesCaravanes = $this->paginate($this->ZonesCaravanes);

        $this->set(compact('zonesCaravanes'));
    }

    /**
     * View method
     *
     * @param string|null $id Zones Caravane id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $zonesCaravane = $this->ZonesCaravanes->get($id, [
            'contain' => ['Zones', 'Caravanes'],
        ]);

        $this->set(compact('zonesCaravane'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $zonesCaravane = $this->ZonesCaravanes->newEmptyEntity();
        if ($this->request->is('post')) {
            $zonesCaravane = $this->ZonesCaravanes->patchEntity($zonesCaravane, $this->request->getData());
            if ($this->ZonesCaravanes->save($zonesCaravane)) {
                $this->Flash->success(__('The zones caravane has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The zones caravane could not be saved. Please, try again.'));
        }
        $zones = $this->ZonesCaravanes->Zones->find('list', ['limit' => 200]);
        $caravanes = $this->ZonesCaravanes->Caravanes->find('list', ['limit' => 200]);
        $this->set(compact('zonesCaravane', 'zones', 'caravanes'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Zones Caravane id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $zonesCaravane = $this->ZonesCaravanes->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $zonesCaravane = $this->ZonesCaravanes->patchEntity($zonesCaravane, $this->request->getData());
            if ($this->ZonesCaravanes->save($zonesCaravane)) {
                $this->Flash->success(__('The zones caravane has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The zones caravane could not be saved. Please, try again.'));
        }
        $zones = $this->ZonesCaravanes->Zones->find('list', ['limit' => 200]);
        $caravanes = $this->ZonesCaravanes->Caravanes->find('list', ['limit' => 200]);
        $this->set(compact('zonesCaravane', 'zones', 'caravanes'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Zones Caravane id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $zonesCaravane = $this->ZonesCaravanes->get($id);
        if ($this->ZonesCaravanes->delete($zonesCaravane)) {
            $this->Flash->success(__('The zones caravane has been deleted.'));
        } else {
            $this->Flash->error(__('The zones caravane could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
