<?php
declare(strict_types=1);

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\EventInterface;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/4/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('FormProtection');`
     *
     * @return void
     */
    public function initialize(): void
    {
        parent::initialize();

        date_default_timezone_set("America/New_York");

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');

        define('ROOT_DIREC', '/cc');

        $this->loadComponent('Auth', [
            'loginRedirect' => [
                'controller' => 'Calls',
                'action' => 'add'
            ],
            'logoutRedirect' => [
                'controller' => 'Users',
                'action' => 'login']
        ]);

        /*
         * Enable the following component for recommended CakePHP form protection settings.
         * see https://book.cakephp.org/4/en/controllers/components/form-protection.html
         */
        //$this->loadComponent('FormProtection');
    }


    public function beforeFilter(EventInterface $event){
        if($this->Auth->user()){
            $this->loadModel('Centers');
            $this->promotions();
            $this->set('connected_user', $this->Auth->user());
            $this->set('promotions', $this->getRequest()->getSession()->read("promotions"));
            $this->set('center', $this->Centers->get($this->Auth->user()['center_id']));
            if(empty($this->getRequest()->getSession()->read("from"))){
                $this->getRequest()->getSession()->write("from", date("Y-m-d"));
            }

            if(empty($this->getRequest()->getSession()->read("to"))){
                $this->getRequest()->getSession()->write("to", date("Y-m-d"));
            }

            $this->set("filterfrom", $this->getRequest()->getSession()->read("from"));
            $this->set("filterto", $this->getRequest()->getSession()->read("to"));
        }
    }

    public function filter($type, $name, $value){
        $variable = $this->getRequest()->getSession()->read($type); 
        if(empty($variable)){
            $this->getRequest()->getSession()->write($type, array($name => $value));
        }else{
            $variable[$name] = $value;
            $this->getRequest()->getSession()->write($type, $variable);
        }
        
    }

    public function init(){
        $call = $this->getRequest()->getSession()->read('call');
        $call['user_id'] = $this->Auth->user()['id'];
        $call['visible'] = 1;
        unset($call['customer_id']); unset($call['customer_info']);unset($call['products']);unset($call['articles']);
        $this->getRequest()->getSession()->write('call', $call);
    }

    private function promotions(){
        $this->loadModel('Promotions');
        $promotions = $this->Promotions->find("all", array("order" => array("created ASC"), "conditions" => array("start_date <=" => date("Y-m-d"), "end_date >=" => date("Y-m-d"), 'keep_active' => 1))); 
        $promos = array(); 
        foreach($promotions as $promotion){
            if($promotion->type == 1|| $promotion->type == 0){
                $promos[$promotion->product_id] = array(
                    "id" => $promotion->id,
                    "type" => $promotion->type,
                    "promotion_price" => $promotion->promotion_price,
                    "article_id" => $promotion->article_id,
                    "start_quantity" => $promotion->start_quantity,
                    'end_quantity' => $promotion->end_quantity,
                    'zone_id' => $promotion->zone_id,
                );
            }else{
                $promos[$promotion->product_id] = array(
                    "id" => $promotion->id,
                    "type" => $promotion->type,
                    "promotion_price" => $promotion->promotion_price,
                    "article_id" => $promotion->article_id,
                    "start_quantity" => $promotion->start_quantity,
                    'end_quantity' => $promotion->end_quantity,
                    'article' => $this->Promotions->Articles->get($promotion->article_id),
                    'zone_id' => $promotion->zone_id,
                );
            }
        }

        $this->getRequest()->getSession()->write("promotions", $promos);
    }
}
