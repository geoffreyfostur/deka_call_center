<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Datasource\ConnectionManager;


/**
 * Calls Controller
 *
 * @property \App\Model\Table\CallsTable $Calls
 * @method \App\Model\Entity\Call[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CallsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $from = $this->getRequest()->getSession()->read("from")." 00:00:00";
        $to = $this->getRequest()->getSession()->read("to")." 23:59:59";
        $calls = $this->Calls->find("all", array("order" => array("Calls.created DESC"), 'conditions' => array('Calls.user_id' => $this->Auth->user()['id'], "Calls.created >=" => $from, "Calls.created <=" => $to)))->contain(['Customers', 'Caravanes', 'Zones', 'Sales']);
        $breadcrumb = array('Historique des Appels' => '/calls');
        $this->set(compact('calls', 'breadcrumb'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $call = $this->getRequest()->getSession()->read("call");
        $customers = [];
        $zones = [];
        if(empty($call['caravane_id'])){
            $this->init();
        }else{
            $user_id = $this->Auth->user()['id'];
            $SQL = "SELECT DISTINCT z.*, c.user_id
                FROM zones z 
                LEFT JOIN customers cu ON cu.zone_id = z.id
                LEFT JOIN customers_caravanes cc ON cc.customer_id = cu.id
                LEFT JOIN caravanes c ON c.id = cc.caravane_id
                WHERE c.user_id = ".$user_id." AND c.id =".$call['caravane_id'];

            $conn = ConnectionManager::get('default');
            $zones = $conn->query($SQL); 
            if(!empty($call['zone_id'])){
                $customers = $this->Calls->Customers->find("all", array("conditions" => array("Customers.status" => 1, 'Customers.zone_id' => $call['zone_id'])))->contain(['Zones', 'CustomersCaravanes', 'Calls' => function($query){
                    return $query->where(['Calls.created >=' => date("Y-m-d 00:00:00"), 'Calls.created <=' => date("Y-m-d 23:59:59")]);
                }]);
            }else{
                $customers = $this->Calls->Customers->find("all", array("conditions" => array("Customers.status" => 1)))->contain(['Zones', 'CustomersCaravanes', 'Calls' => function($query){
                    return $query->where(['Calls.created >=' => date("Y-m-d 00:00:00"), 'Calls.created <=' => date("Y-m-d 23:59:59")]);
                }]);
            }
            
            $caravane = $call['caravane_id'];

            $customers = $customers->matching('CustomersCaravanes', function ($query) use ($caravane) {
                return $query->where(['CustomersCaravanes.caravane_id' => $caravane]);
            });
        }
        $caravanes = $this->Calls->Caravanes->find('all', array("conditions" => array("Caravanes.user_id" => $this->Auth->user()['id']), "order" => array("Caravanes.name asc")))->contain(['Users']);
        $breadcrumb = array('Nouvel Appel' => '/calls/add');
        $this->set(compact('caravanes','breadcrumb', 'call', 'customers', 'zones'));
    }
    

    public function setcaravane($id){
        $call = $this->getRequest()->getSession()->read('call');
        $call['caravane_id'] = $id;
        $call['zone_id'] = null;
        $call['customer_id'] = null;
        $call['customer_info'] = null;
        $call['caravane_info'] = $this->Calls->Caravanes->get($id);
        $this->getRequest()->getSession()->write('call', $call);
        return $this->redirect(['action' => 'add']);
    }


    public function setzone($id){
        $call = $this->getRequest()->getSession()->read('call');
        $call['zone_id'] = $id;
        $call['customer_id'] = null;
        $call['customer_info'] = null;
        $this->loadModel('Zones');
        $call['zone_info'] = $this->Zones->get($id);
        $this->getRequest()->getSession()->write('call', $call);
        return $this->redirect(['action' => 'add']);
    }

    public function setcustomer($id){
        $call = $this->getRequest()->getSession()->read('call');
        $call['customer_id'] = $id;
        $call['customer_info'] = $this->Calls->Customers->get($id);
        $this->getRequest()->getSession()->write('call', $call);
        return $this->redirect(['action' => 'add']);
    }

    /**
     * Save method
     *
     * @param string|null $id Call id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function save($id = null)
    {
        if ($this->request->is(['patch', 'post', 'put'])) {
            $ongoing_call = $this->getRequest()->getSession()->read('call');
            $call = $this->Calls->newEmptyEntity(); 
            $call->user_id = $ongoing_call['user_id'];
            $call->visible = 1; 
            $call->customer_id = $ongoing_call['customer_id'];
            $call->caravane_id = $ongoing_call['caravane_id'];
            $call->zone_id = $ongoing_call['customer_info']['zone_id'];
            $call->note = $this->request->getData()['note'];
            $call->answered = $this->request->getData()['answered'];
            $saved_call = $this->Calls->save($call); 
            $ongoing_call['call_id'] = $saved_call->id;
            $this->getRequest()->getSession()->write('call', $ongoing_call);
            if(!empty($this->request->getData()['customer_note'])){
                $customer = $this->Calls->Customers->get($ongoing_call['customer_id']);
                $customer->note = $this->request->getData()['customer_note'];
                $this->Calls->Customers->save($customer);
            }
            if($this->request->getData()['proforma'] == 1){
                return $this->redirect(['controller' => "sales", 'action' => 'add']);
            }else{
                $this->init();
                return $this->redirect(['action' => 'add']);
            }
        }
    }

    public function refresh(){
        $this->init();
        return $this->redirect(['action' => 'add']);
    }
}
