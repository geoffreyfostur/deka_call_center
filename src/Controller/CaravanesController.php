<?php
declare(strict_types=1);

namespace App\Controller;
use FPDF;

/**
 * Caravanes Controller
 *
 * @property \App\Model\Table\CaravanesTable $Caravanes
 * @method \App\Model\Entity\Caravane[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CaravanesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $caravanes = $this->Caravanes->find("all", array("conditions" => array("user_id" => $this->Auth->user()['id'])));
        $breadcrumb = array('Caravanes' => '/caravanes');
        $this->set(compact('caravanes', 'breadcrumb'));
    }

    public function routing($type = 0){
        if(empty($this->getRequest()->getSession()->read("routing"))){
            $this->initialize_routing();
        }
        $routing = $this->getRequest()->getSession()->read("routing");
        $caravane_id = "";
        $delivery_date = date("Y-m-d", strtotime('+1 day'));
        $customers = [];
        $breadcrumb = array('Feuille de Route' => '/caravanes/routing');
        $sales = [];
        if($this->request->is(['patch', 'put', 'post'])){

            $routing = $this->getRequest()->getSession()->read("routing");
            if(empty($routing['caravane_id'])){
                $this->initialize_routing();
            }
            $routing = $this->getRequest()->getSession()->read("routing");
            $caravane_id = $this->request->getData()['caravane_id'];
            $delivery_date = $this->request->getData()['delivery_date'];
            if(!empty($routing['caravane_id'])){
                if($this->request->getData()['caravane_id'] != $routing['caravane_id']){
                $routing['awaiting_customers'] = [];
                $routing['customers_to_convince'] = [];
            }
            }
            
            $routing['caravane_id'] = $caravane_id;
            $routing['delivery_date'] = $delivery_date;
            $this->getRequest()->getSession()->write("routing", $routing);
        }
        if(!empty($routing['caravane_id'])){
            $caravane_id = $routing['caravane_id'];
            $sales = $this->Caravanes->Sales->find("all", array("order" => array("Sales.created DESC"), "conditions" => array('Sales.caravane_id' => $routing['caravane_id'], "Sales.delivery_date" => $delivery_date, 'Sales.status' => 1, 'Sales.user_id' => $this->Auth->user()['id'])))->contain(['Customers', 'Caravanes', 'Zones']);

            $customers = $this->Caravanes->Customers->find("all", array("conditions" => array("Customers.status" => 1)))->contain(['Zones', 'CustomersCaravanes', 'Calls' => function($query){
                    return $query->where(['Calls.created >=' => date("Y-m-d 00:00:00"), 'Calls.created <=' => date("Y-m-d 23:59:59")]);
                }]);
            $customers = $customers->matching('CustomersCaravanes', function ($query) use ($caravane_id) {
                return $query->where(['CustomersCaravanes.caravane_id' => $caravane_id]);
            });
        }
        $caravanes = $this->Caravanes->find('list', array("conditions" => array("user_id" => $this->Auth->user()['id']), "order" => array("name asc")));
        $this->set(compact('caravane_id', 'breadcrumb', 'caravanes', 'sales', 'customers', 'type', 'routing', 'delivery_date'));
    }

    public function initialize_routing(){
        $routing = [];
        $routing['awaiting_customers'] = [];
        $routing['customers_to_convince'] = [];
        $routing['delivery_date'] = date("Y-m-d", strtotime('+1 day'));
        $this->getRequest()->getSession()->write("routing", $routing);
    }

    public function addcustomer(){
        if($this->request->is(['ajax'])){
            $routing = $this->getRequest()->getSession()->read("routing");
            if($this->request->getData()['type'] == 1){
                $routing['awaiting_customers'][$this->request->getData()['customer_id']] = $this->request->getData()['customer_id'];
            }else{
                $routing['customers_to_convince'][$this->request->getData()['customer_id']] = $this->request->getData()['customer_id'];
            }
            $this->getRequest()->getSession()->write("routing", $routing);
            echo json_encode($routing);
        }
        
        die();
    }

    public function removecustomer(){
        if($this->request->is(['ajax'])){
            $routing = $this->getRequest()->getSession()->read("routing");
            if($this->request->getData()['type'] == 1){
                unset($routing['awaiting_customers'][$this->request->getData()['customer_id']]);
            }else{
                unset($routing['customers_to_convince'][$this->request->getData()['customer_id']]);
            }
            $this->getRequest()->getSession()->write("routing", $routing);
            echo json_encode($routing);
        }
        
        die();
    }

    public function export(){
        // export routing to PDF
        $routing = $this->getRequest()->getSession()->read("routing");
        $caravane = $this->Caravanes->get($routing['caravane_id']);
        $sales = $this->Caravanes->Sales->find("all", array("order" => array("Sales.created DESC"), "conditions" => array('Sales.caravane_id' => $routing['caravane_id'], "Sales.delivery_date" => date("Y-m-d", strtotime("+1 day")), "Sales.type" => 1, 'Sales.status' => 1, 'Sales.user_id' => $this->Auth->user()['id'])))->contain(['Customers', 'Caravanes', 'Zones', 'Products']);
        require_once(ROOT . DS . 'vendor' . DS  . 'fpdf'  . DS . 'fpdf.php');
        $fpdf = new FPDF();
        $fpdf->AddPage('L');
        $fpdf->SetFont('Arial','B',9);
        $fpdf->Cell(50,0,"Feuille de Route : ". $caravane->identification . " -  " . $caravane->seller_name . " -  " . $caravane->seller_phone ,0,0, 'L');
        $fpdf->Cell(225,0,date("Y-m-d", strtotime($routing['delivery_date'])),0,0, 'R');
        
        $i=1;
        foreach($sales as $sale){
            $fpdf->Ln(10);
        $fpdf->Cell(275,0,"",'B',0, 'R');
        $fpdf->Ln(7);
            $fpdf->SetFont('Arial','B',8);
            $fpdf->Cell(95,7,'Proforma #'.$sale->sale_number,0,0, 'L');
            $fpdf->Ln();
            $fpdf->Cell(95,7,'Client : '.$sale->customer->name,0,0, 'L');
            $fpdf->Ln();
            $fpdf->Cell(60,7,'ZONE : ' . $sale->zone->name,0,0, 'L');
            $fpdf->Ln();
            $fpdf->Cell(60,7,'ADRESSE : '. $sale->customer->address,0,0, 'L');
            $fpdf->Ln();
            $fpdf->Cell(30,7,'TELEPHONE : '. $sale->customer->phone,0,0, 'L');
            $fpdf->Ln(10);
            if(!empty($sale->customer->phone2)){
                $fpdf->Cell(30,7,'TELEPHONE 2 : '. $sale->customer->phone2,0,0, 'L');
                $fpdf->Ln(10);
            }

            $fpdf->SetFont('Arial','B',8);
            $fpdf->Cell(105,7,'PRODUIT','T,L,B',0, 'L');
            $fpdf->Cell(40,7,'COMMANDE','T,L,B',0, 'C');
            $fpdf->Cell(40,7,'LIVRE','T,L,B',0, 'C');
            $fpdf->Cell(40,7,'PRIX','T,L,B',0, 'C');
            $fpdf->Cell(50,7,'TOTAL','T,R,L,B',0, 'C');
            $fpdf->SetFont('Arial','',8);
            foreach($sale->products as $product){
                $fpdf->Ln();
                $fpdf->Cell(105,7,$product->name,'T,L,B',0, 'L');
                $fpdf->Cell(40,7,$product->_joinData->quantity_ordered,'T,L,B',0, 'C');
                $fpdf->Cell(40,7,'','T,L,B',0, 'C');
                $fpdf->Cell(40,7,number_format((float)$product['_joinData']->price, 2, ".", ","),'T,L,B',0, 'C');
                $fpdf->Cell(50,7,number_format((float)$product['_joinData']->total_ordered, 2, ".", ","),'T,R,L,B',0, 'C');
            }
            $fpdf->SetFont('Arial','B',8);
            $fpdf->Ln();
            $fpdf->Cell(225,7,'TOTAL',0,0, 'R');
            $fpdf->Cell(50,7,number_format((float)$sale->total_ordered, 2, ".", ",")." USD",'R,L,B',0, 'C');
            
            $fpdf->SetFont('Arial','',8);
            $i++;

        }

        if($sales->count() > 0){
            $fpdf->AddPage('L');
        }
        
        $fpdf->Ln(5);
        $fpdf->Cell(275,7,'CLIENTS EN ATTENTE','T,L,R,B',0, 'L');
        $fpdf->Ln();
        $fpdf->SetFont('Arial','B',8);
        $fpdf->Cell(95,7,'NOM','L,B',0, 'L');
        $fpdf->Cell(60,7,'ZONE','L,B',0, 'C');
        $fpdf->Cell(60,7,'ADRESSE','L,B',0, 'C');
        $fpdf->Cell(30,7,'TELEPHONE','L,B',0, 'C');
        $fpdf->Cell(30,7,'TELEPHONE 2','L,R,B',0, 'C');
        $fpdf->SetFont('Arial','',8);
        $fpdf->Ln();
        foreach($routing['awaiting_customers'] as $key => $id){
            $customer = $this->Caravanes->Customers->get($id, ['contain' => ['Zones']]); 
            $fpdf->Cell(95,7,$customer->name,'L,B',0, 'L');
            $fpdf->Cell(60,7,$customer->zone->name,'L,B',0, 'C');
            $fpdf->Cell(60,7,$customer->address,'L,B',0, 'C');
            $fpdf->Cell(30,7,$customer->phone,'L,B',0, 'C');
            $fpdf->Cell(30,7,$customer->phone2,'L,R,B',0, 'C');
            $fpdf->Ln();
        }

        $fpdf->Ln(5);
        $fpdf->SetFont('Arial','B',8);
        $fpdf->Cell(275,7,'CLIENTS A CONVAINCRE','T,L,R,B',0, 'L');
        $fpdf->Ln();
        $fpdf->Cell(95,7,'NOM','L,B',0, 'L');
        $fpdf->Cell(60,7,'ZONE','L,B',0, 'C');
        $fpdf->Cell(60,7,'ADRESSE','L,B',0, 'C');
        $fpdf->Cell(30,7,'TELEPHONE','L,B',0, 'C');
        $fpdf->Cell(30,7,'TELEPHONE 2','L,R,B',0, 'C');
        $fpdf->SetFont('Arial','',8);
        $fpdf->Ln();
        foreach($routing['customers_to_convince'] as $key => $id){
            $customer = $this->Caravanes->Customers->get($id, ['contain' => ['Zones']]); 
            $fpdf->Cell(95,7,$customer->name,'L,B',0, 'L');
            $fpdf->Cell(60,7,$customer->zone->name,'L,B',0, 'C');
            $fpdf->Cell(60,7,$customer->address,'L,B',0, 'C');
            $fpdf->Cell(30,7,$customer->phone,'L,B',0, 'C');
            $fpdf->Cell(30,7,$customer->phone2,'L,R,B',0, 'C');
            $fpdf->Ln();
        }

        $fpdf->SetFont('Arial','B',8);
        $fpdf->Ln(5);
        $fpdf->Cell(275,10,'NOTE GENERALE','T,L,R',0, 'L');
        $fpdf->Ln();
        $fpdf->Cell(275,40,'','T,L,R,B',0, 'L');
        $fpdf->Output('I');
        die();
    }

}