<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $users = $this->Users->find("all", array("order" => array("Users.name ASC")))->contain(['Roles', 'Centers']);

        $breadcrumb = array('Utilisateurs' => '/users');

        $this->set(compact('users', 'breadcrumb'));
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Roles', 'Calls', 'Caravanes', 'Customers', 'Products', 'Sales'],
        ]);

        $this->set(compact('user'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEmptyEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $roles = $this->Users->Roles->find('list');
        $breadcrumb = array('Utilisateurs' => '/users', 'Ajouter' => '/users/add');
        $centers = $this->Users->Centers->find('list', array("order" => array("name ASC")));
        $this->set(compact('user', 'roles', 'breadcrumb', 'centers'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $roles = $this->Users->Roles->find('list', ['limit' => 200]);
        $breadcrumb = array('Utilisateurs' => '/users', 'Editer' => '#', $user->name => "/users/edit/".$user->id);
        $centers = $this->Users->Centers->find('list', array("order" => array("name ASC")));
        $this->set(compact('user', 'roles', 'breadcrumb', 'centers'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }


    public function login(){
        $this->viewBuilder()->setLayout('login');
        if($this->request->is('post')){
            $user = $this->Auth->identify();
            if ($user) {
                if($user['status'] == false){
                    $this->Flash->error(__('Ce compte est bloqué. Contactez votre administrateur'));
                }else{
                    if($user['role_id'] == 1 || $user['role_id'] == 2){
                        $this->Auth->setUser($user);
                        return $this->redirect($this->Auth->redirectUrl());
                    }else{
                        $this->Flash->error(__('Accès interdit. Contactez votre administrateur'));
                    }
                }
            }else{
                $this->Flash->error(__('Vos identifiants so incorrectes'));
            }
        }
    }

    public function logout(){
        $this->getRequest()->getSession()->write("promotions", []);
        $this->getRequest()->getSession()->write("sale", []);
        $this->getRequest()->getSession()->write("call", []);
        return $this->redirect($this->Auth->logout());
    }
}
