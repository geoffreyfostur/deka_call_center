<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * ProductsCenters Controller
 *
 * @property \App\Model\Table\ProductsCentersTable $ProductsCenters
 * @method \App\Model\Entity\ProductsCenter[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ProductsCentersController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Products', 'Centers'],
        ];
        $productsCenters = $this->paginate($this->ProductsCenters);

        $this->set(compact('productsCenters'));
    }

    /**
     * View method
     *
     * @param string|null $id Products Center id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $productsCenter = $this->ProductsCenters->get($id, [
            'contain' => ['Products', 'Centers'],
        ]);

        $this->set(compact('productsCenter'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $productsCenter = $this->ProductsCenters->newEmptyEntity();
        if ($this->request->is('post')) {
            $productsCenter = $this->ProductsCenters->patchEntity($productsCenter, $this->request->getData());
            if ($this->ProductsCenters->save($productsCenter)) {
                $this->Flash->success(__('The products center has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The products center could not be saved. Please, try again.'));
        }
        $products = $this->ProductsCenters->Products->find('list', ['limit' => 200]);
        $centers = $this->ProductsCenters->Centers->find('list', ['limit' => 200]);
        $this->set(compact('productsCenter', 'products', 'centers'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Products Center id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $productsCenter = $this->ProductsCenters->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $productsCenter = $this->ProductsCenters->patchEntity($productsCenter, $this->request->getData());
            if ($this->ProductsCenters->save($productsCenter)) {
                $this->Flash->success(__('The products center has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The products center could not be saved. Please, try again.'));
        }
        $products = $this->ProductsCenters->Products->find('list', ['limit' => 200]);
        $centers = $this->ProductsCenters->Centers->find('list', ['limit' => 200]);
        $this->set(compact('productsCenter', 'products', 'centers'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Products Center id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $productsCenter = $this->ProductsCenters->get($id);
        if ($this->ProductsCenters->delete($productsCenter)) {
            $this->Flash->success(__('The products center has been deleted.'));
        } else {
            $this->Flash->error(__('The products center could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
