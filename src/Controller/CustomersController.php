<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Customers Controller
 *
 * @property \App\Model\Table\CustomersTable $Customers
 * @method \App\Model\Entity\Customer[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CustomersController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $filters = $this->getRequest()->getSession()->read('customers_filters');
        $customers = $this->Customers->find("all", array("conditions" => array("Customers.status" => 1)))->contain(['Zones', 'Caravanes']);
        $caravane = '';

        if(!empty($filters['caravane_id'])){
            $caravane = $filters['caravane_id'];
            $customers = $customers->matching('Caravanes', function ($query) use ($caravane) {
                return $query->where(['Caravanes.id' => $caravane]);
            });
        }
        
        $user_id = $this->Auth->user()['id'];
        $customers = $customers->matching('Caravanes', function ($query) use ($user_id) {
            return $query->where(['Caravanes.user_id' => $user_id]);
        });
        $caravanes = $this->Customers->Caravanes->find('list', array("conditions" => array("user_id" => $user_id), "order" => array("name asc")));
        $breadcrumb = array('Clients' => '/customers');
        $this->set(compact('customers', 'breadcrumb', 'caravanes', 'caravane'));
    }

    public function setfilters(){
        if($this->request->is(['patch', 'put', 'post'])){

            if(!empty($this->request->getData()['caravane_id'])){
                $this->filter("customers_filters", "caravane_id", $this->request->getData()['caravane_id']);
            }else{
                $this->filter("customers_filters", "caravane_id", '');
            }
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Customer id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $customer = $this->Customers->get($id, [
            'contain' => ['Caravanes'],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $customer = $this->Customers->patchEntity($customer, $this->request->getData());
            if ($this->Customers->save($customer)) {

                return $this->redirect(['action' => 'index', $customer->zone_id]);
            }
        }
        $zones = $this->Customers->Zones->find('list', array("order" => array("name asc")));
        $routes = $this->Customers->Routes->find('list', array("order" => array("name asc")));
        $caravanes = $this->Customers->Caravanes->find('list', array("order" => array("name asc")));
        $breadcrumb = array('Clients' => '/customers', 'Editer' => '#', $customer->name => "/customers/edit/".$customer->id);
        $this->set(compact('customer', 'zones', 'routes', 'caravanes', 'breadcrumb'));
    }
}
