<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * CategoriesCaravanes Controller
 *
 * @property \App\Model\Table\CategoriesCaravanesTable $CategoriesCaravanes
 * @method \App\Model\Entity\CategoriesCaravane[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CategoriesCaravanesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Categories', 'Caravanes'],
        ];
        $categoriesCaravanes = $this->paginate($this->CategoriesCaravanes);

        $this->set(compact('categoriesCaravanes'));
    }

    /**
     * View method
     *
     * @param string|null $id Categories Caravane id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $categoriesCaravane = $this->CategoriesCaravanes->get($id, [
            'contain' => ['Categories', 'Caravanes'],
        ]);

        $this->set(compact('categoriesCaravane'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $categoriesCaravane = $this->CategoriesCaravanes->newEmptyEntity();
        if ($this->request->is('post')) {
            $categoriesCaravane = $this->CategoriesCaravanes->patchEntity($categoriesCaravane, $this->request->getData());
            if ($this->CategoriesCaravanes->save($categoriesCaravane)) {
                $this->Flash->success(__('The categories caravane has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The categories caravane could not be saved. Please, try again.'));
        }
        $categories = $this->CategoriesCaravanes->Categories->find('list', ['limit' => 200]);
        $caravanes = $this->CategoriesCaravanes->Caravanes->find('list', ['limit' => 200]);
        $this->set(compact('categoriesCaravane', 'categories', 'caravanes'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Categories Caravane id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $categoriesCaravane = $this->CategoriesCaravanes->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $categoriesCaravane = $this->CategoriesCaravanes->patchEntity($categoriesCaravane, $this->request->getData());
            if ($this->CategoriesCaravanes->save($categoriesCaravane)) {
                $this->Flash->success(__('The categories caravane has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The categories caravane could not be saved. Please, try again.'));
        }
        $categories = $this->CategoriesCaravanes->Categories->find('list', ['limit' => 200]);
        $caravanes = $this->CategoriesCaravanes->Caravanes->find('list', ['limit' => 200]);
        $this->set(compact('categoriesCaravane', 'categories', 'caravanes'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Categories Caravane id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $categoriesCaravane = $this->CategoriesCaravanes->get($id);
        if ($this->CategoriesCaravanes->delete($categoriesCaravane)) {
            $this->Flash->success(__('The categories caravane has been deleted.'));
        } else {
            $this->Flash->error(__('The categories caravane could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
