<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * CustomersCaravanes Controller
 *
 * @property \App\Model\Table\CustomersCaravanesTable $CustomersCaravanes
 * @method \App\Model\Entity\CustomersCaravane[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CustomersCaravanesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Caravanes', 'Customers'],
        ];
        $customersCaravanes = $this->paginate($this->CustomersCaravanes);

        $this->set(compact('customersCaravanes'));
    }

    /**
     * View method
     *
     * @param string|null $id Customers Caravane id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $customersCaravane = $this->CustomersCaravanes->get($id, [
            'contain' => ['Caravanes', 'Customers'],
        ]);

        $this->set(compact('customersCaravane'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $customersCaravane = $this->CustomersCaravanes->newEmptyEntity();
        if ($this->request->is('post')) {
            $customersCaravane = $this->CustomersCaravanes->patchEntity($customersCaravane, $this->request->getData());
            if ($this->CustomersCaravanes->save($customersCaravane)) {
                $this->Flash->success(__('The customers caravane has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The customers caravane could not be saved. Please, try again.'));
        }
        $caravanes = $this->CustomersCaravanes->Caravanes->find('list', ['limit' => 200]);
        $customers = $this->CustomersCaravanes->Customers->find('list', ['limit' => 200]);
        $this->set(compact('customersCaravane', 'caravanes', 'customers'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Customers Caravane id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $customersCaravane = $this->CustomersCaravanes->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $customersCaravane = $this->CustomersCaravanes->patchEntity($customersCaravane, $this->request->getData());
            if ($this->CustomersCaravanes->save($customersCaravane)) {
                $this->Flash->success(__('The customers caravane has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The customers caravane could not be saved. Please, try again.'));
        }
        $caravanes = $this->CustomersCaravanes->Caravanes->find('list', ['limit' => 200]);
        $customers = $this->CustomersCaravanes->Customers->find('list', ['limit' => 200]);
        $this->set(compact('customersCaravane', 'caravanes', 'customers'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Customers Caravane id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $customersCaravane = $this->CustomersCaravanes->get($id);
        if ($this->CustomersCaravanes->delete($customersCaravane)) {
            $this->Flash->success(__('The customers caravane has been deleted.'));
        } else {
            $this->Flash->error(__('The customers caravane could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
