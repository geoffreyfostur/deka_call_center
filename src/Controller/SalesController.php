<?php
declare(strict_types=1);

namespace App\Controller;
use FPDF;

/**
 * Sales Controller
 *
 * @property \App\Model\Table\SalesTable $Sales
 * @method \App\Model\Entity\Sale[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SalesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $from = $this->getRequest()->getSession()->read("from")." 00:00:00";
        $to = $this->getRequest()->getSession()->read("to")." 23:59:59";
        $caravane_id = '';
        $zone_id = '';
        $customer_id = '';
        $user_id = '';
        $type = '';
        $sales = $this->Sales->find("all", array("order" => array("Sales.created DESC"), "conditions" => array("Sales.created >=" => $from, "Sales.created <=" => $to, 'Sales.status' => 1, 'Sales.user_id' => $this->Auth->user()['id'])))->contain(['Customers', "Zones", "Caravanes", 'Users']);
        if ($this->request->is(['patch', 'post', 'put'])){
            if(!empty($this->request->getData()['caravane_id'])){
                $caravane_id = $this->request->getData()['caravane_id'];
                $sales->where(['Sales.caravane_id' => $caravane_id]);
            }
            if(!empty($this->request->getData()['user_id'])){
                $user_id = $this->request->getData()['user_id'];
                $sales->where(['Sales.user_id' => $user_id]);
            }
            if(!empty($this->request->getData()['customer_id'])){
                $customer_id = $this->request->getData()['customer_id'];
                $sales->where(['Sales.customer_id' => $customer_id]);
            }
            if(!empty($this->request->getData()['zone_id'])){
                $zone_id = $this->request->getData()['zone_id'];
                $sales->where(['Sales.zone_id' => $zone_id]);
            }

            if(!empty($this->request->getData()['type'])){
                $type = $this->request->getData()['type'];
                if($type == 4){
                    $sales->where(['Sales.status' => 0]);
                }else{
                    $sales->where(['Sales.type' => $type]);
                }
            }

            if(!empty($this->request->getData()['export']) && $this->request->getData()['export'] == 1){
                $this->excel($sales, $caravane_id, $zone_id, $user_id);
            }
        }
        $breadcrumb = array('Ventes' => '#');
        
        $caravanes = $this->Sales->Caravanes->find("list", ['order' => ['name ASC'], 'conditions' => ['user_id' => $this->Auth->user()['id']]]);
        $customers = $this->Sales->Customers->find("list", array("conditions" => array("Customers.status" => 1)))->contain(['Zones', 'Caravanes']);
        $user_id = $this->Auth->user()['id'];
        $customers = $customers->matching('Caravanes', function ($query) use ($user_id) {
            return $query->where(['Caravanes.user_id' => $user_id]);
        });
        
        $this->set(compact('sales', 'breadcrumb', 'caravanes','caravane_id', 'type', 'customers', 'customer_id'));
    }

    public function setDates(){
        if ($this->request->is(['put', 'patch', 'post'])){
            if(!empty($this->request->getData()["from"])){
                $this->getRequest()->getSession()->write("from", $this->request->getData()["from"]);
            }

            if(!empty($this->request->getData()["to"])){
                $this->getRequest()->getSession()->write("to", $this->request->getData()["to"]);
            }
        }

        return $this->redirect($this->referer());
    }

    /**
     * View method
     *
     * @param string|null $id Sale id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $sale = $this->Sales->get($id, [
            'contain' => ['Customers', 'Caravanes', 'Zones', 'Users', 'Products', 'ArticlesSales' => ['Articles']],
        ]);

        if($sale->user_id != $this->Auth->user()['id']){
            $this->redirect($this->referer());
        }

        $breadcrumb = array('Vente #'.$sale->sale_number => '#');

        $this->set(compact('sale', 'breadcrumb'));
    }

    public function search(){
        if ($this->request->is(['patch', 'post', 'put'])){
            $sale = $this->Sales->find("all", array("conditions" => array("sale_number" => $this->request->getData()['sale_number'])))->first();
            if($sale->user_id = $this->Auth->user()['id']){
               return $this->redirect(['action' => 'edit', $sale->id]); 
           }else{
            return $this->redirect($this->referer());
           }
            
        }
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $categories_list = [];
        $center_id = $this->Auth->user()['center_id'];
        $this->loadModel('Categories'); 
        if(!empty($this->getRequest()->getSession()->read("sale"))){
            $sale = $this->getRequest()->getSession()->read("sale");
        }else{
            $sale = array();
        }
        if(!empty($this->getRequest()->getSession()->read("call"))){
            $call = $this->getRequest()->getSession()->read("call");
            $breadcrumb = array('Nouveau Proforma' => '/sales/add', $call['caravane_info']->identification => '#', $call['customer_info']->name => '#');
            $categories = $this->Categories->find("all")->contain(['Caravanes', 'Products' => ['sort' => "Products.name ASC", 'ProductsCenters']]); 
            $caravane_id = $call['caravane_id'];
            $caravane = $this->Sales->Caravanes->get($call['caravane_id']); 
            $center_id = $caravane->center_id;
            $categories = $categories->matching('Caravanes', function ($query) use ($caravane_id) {
            return $query->where(['Caravanes.id' => $caravane_id]);
        });
            foreach($categories as $cat){
               $categories_list[$cat->id] = $cat->name; 
            }

        }else{
            $call = array();
            $categories = array();
            $breadcrumb = array('Nouveau Proforma' => '/sales/add');
        }
    
        $this->set(compact('sale', 'call', 'breadcrumb', 'categories', 'categories_list', 'center_id'));
    }

    public function direct()
    {
        $categories_list = [];
        $categories = [];
        $customers = [];
        $center_id = $this->Auth->user()['center_id'];
        $this->loadModel('Categories');
        $sale = $this->getRequest()->getSession()->read("sale");
        $promotions = $this->getRequest()->getSession()->read("promotions");
        // debug($promotions);
        // debug($sale); die();
        if(empty($sale["type"])){
            $this->initializeDirectSale();
        }else{
            if(!empty($sale['caravane_id'])){
                $customers = $this->Sales->Customers->find("all", array("conditions" => array("Customers.status" => 1)))->contain(['Zones', 'CustomersCaravanes', 'Calls' => function($query){
                    return $query->where(['Calls.created >=' => date("Y-m-d 00:00:00"), 'Calls.created <=' => date("Y-m-d 23:59:59")]);
                }]);

                $caravane_id = $sale['caravane_id'];
                $caravane = $this->Sales->Caravanes->get($sale['caravane_id']); 
                $center_id = $caravane->center_id;

                $customers = $customers->matching('CustomersCaravanes', function ($query) use ($caravane_id) {
                    return $query->where(['CustomersCaravanes.caravane_id' => $caravane_id]);
                });

                $categories = $this->Categories->find("all")->contain(['Caravanes', 'Products' => ['sort' => "Products.name ASC", 'ProductsCenters']]); 
                $categories = $categories->matching('Caravanes', function ($query) use ($caravane_id) {
                    return $query->where(['Caravanes.id' => $caravane_id]);
                });

                foreach($categories as $cat){
                   $categories_list[$cat->id] = $cat->name; 
                }
            }
        }
        $caravanes = $this->Sales->Caravanes->find('all', array("conditions" => array("Caravanes.user_id" => $this->Auth->user()['id']), "order" => array("Caravanes.name asc")))->contain(['Users']);
        $breadcrumb = array('Nouvelle Vente' => '/sales/direct');
        $this->set(compact('sale', 'breadcrumb', 'categories', 'categories_list', 'caravanes', 'customers', 'center_id'));
    }


    public function setcaravane($id){
        $sale = $this->getRequest()->getSession()->read('sale');
        $sale['caravane_id'] = $id;
        $sale['customer_id'] = null;
        $sale['customer_info'] = null;
        $sale['caravane_info'] = $this->Sales->Caravanes->get($id);
        $this->getRequest()->getSession()->write('sale', $sale);
        return $this->redirect(['action' => 'direct']);
    }

    public function setcustomer($id){
        $sale = $this->getRequest()->getSession()->read('sale');
        $sale['customer_id'] = $id;
        $sale['customer_info'] = $this->Sales->Customers->get($id);
        $this->getRequest()->getSession()->write('sale', $sale);
        return $this->redirect(['action' => 'direct']);
    }

    public function initializeDS(){
        $sale = array();
        $sale['type'] = 2;
        $this->getRequest()->getSession()->write("sale", $sale);
        return $this->redirect(['action' => 'direct']);
    }

    public function initializeDirectSale(){
        $sale = array();
        $sale['type'] = 2;
        $this->getRequest()->getSession()->write("sale", $sale);
    }

    private function setPromotion($product, $sale){
        $call = $this->getRequest()->getSession()->read("call");
        $promotions = $this->getRequest()->getSession()->read("promotions");
        if(!empty($promotions[$product['product_id']])){
            $promo = $promotions[$product['product_id']];
            if(empty($promo['zone_id']) || $promo['zone_id'] == $call['customer_info']['zone_id']){
                if($promo['type'] == 0 || $promo['type'] == 1){
                    $product['list_price'] = $product['price']; 
                    $product['price'] = $promo['promotion_price']; 
                    $product['promotion_id'] = $promo['id'];
                    $sale['products'][$product['product_id']] = $product;
                }else{
                    $quantity = $product['quantity'];
                    $start_quantity = $promo['start_quantity'];
                    $end_quantity = $promo['end_quantity'];
                    $multiple = intdiv((int)$quantity, (int)$start_quantity);
                    $toadd_quantity = $end_quantity*$multiple; 
                    if($toadd_quantity > 0){
                        if(!empty($sale['articles'][$promo['article_id']])){
                            if($sale['articles'][$promo['article_id']]['product_id'] == $product['product_id']){
                                $sale['articles'][$promo['article_id']]['quantity'] = $toadd_quantity;
                                $sale['articles'][$promo['article_id']]['price'] = $toadd_quantity*$promo['article']->price;
                            }else{
                                $sale['articles'][$promo['article_id']]['quantity'] = $sale['articles'][$promo['article_id']]['quantity'] + $toadd_quantity;
                                $sale['articles'][$promo['article_id']]['price'] = $sale['articles'][$promo['article_id']]['price'] + $toadd_quantity*$promo['article']->price;
                            }
                            
                        }else{
                            $new_article = array(); 
                            $new_article = array(
                                "quantity" => $toadd_quantity,
                                'article_id' => $promo['article_id'],
                                'price' => $promo['article']->price*$toadd_quantity,
                                'cost' => $promo['article']->cost*$toadd_quantity,
                                'name' => $promo['article']->name,
                                'promotion_id' => $promo['id'],
                                'product_id' => $product['product_id']
                            );

                            $sale['articles'][$promo['article_id']] = $new_article;
                        }
                    }
                }
            }
        }
            
        $this->getRequest()->getSession()->write("sale", $sale);
    }

    public function deletearticle(){
        if ($this->request->is(['ajax'])){
            $sale = $this->getRequest()->getSession()->read("sale");
            unset($sale['articles'][$this->request->getData()['article_id']]);
            $this->getRequest()->getSession()->write("sale", $sale);
            $total = 0;
            foreach($sale['products'] as $id => $product){
                $total = $total + $product['quantity']*$product['price'];
            }
            $articles_total = 0;
            if(!empty($sale['articles'])){
                foreach($sale['articles'] as $id => $article){
                    $articles_total = $articles_total + $article['price'];
                }
            }
            
            $sale['products_total'] = $total;
            $sale['articles_total'] = $articles_total;
            $sale['total'] = $total+ $articles_total;
            echo json_encode($sale);
        }
        die();
    }

    public function addproduct(){
        if ($this->request->is(['ajax'])){
            if(!empty($this->getRequest()->getSession()->read("sale"))){
                $sale = $this->getRequest()->getSession()->read("sale");
            }else{
                $sale = array();
                $sale['products'] = array();
            }
            if($this->request->getData()['quantity'] == 0){
                $quantity = $sale['products'][$this->request->getData()['product_id']]['quantity'];
                unset($sale['products'][$this->request->getData()['product_id']]);
                // delete related promotions 
                $promotions = $this->getRequest()->getSession()->read("promotions");
                if(!empty($promotions[$this->request->getData()['product_id']])){
                    $promo = $promotions[$this->request->getData()['product_id']];
                    if(!empty($sale['articles'][$promo['article_id']])){
                        $start_quantity = $promo['start_quantity']; 
                        $end_quantity = $promo['end_quantity']; 
                        $quantity_to_take_out = $end_quantity*$quantity/$start_quantity;
                        if($sale['articles'][$promo['article_id']]['quantity'] == $quantity_to_take_out){
                            unset($sale['articles'][$promo['article_id']]);
                        }else{
                            $sale['articles'][$promo['article_id']]['quantity'] = $sale['articles'][$promo['article_id']]['quantity'] - $quantity_to_take_out;
                            $sale['articles'][$promo['article_id']]['price'] = ($sale['articles'][$promo['article_id']]['quantity'] - $quantity_to_take_out)*$promo['article']->price;
                        }
                    }
                }
                $this->getRequest()->getSession()->write("sale", $sale);
                $sale = $this->getRequest()->getSession()->read("sale");
            }else{
                $product = array(
                    "quantity" => $this->request->getData()['quantity'],
                    'product_id' => $this->request->getData()['product_id'],
                    'price' => $this->request->getData()['price'],
                    'list_price' => $this->request->getData()['price'],
                    'name' => $this->request->getData()['product_name']
                );
                $sale['products'][$this->request->getData()['product_id']] = $product;
                $this->setPromotion($product, $sale);
            }
            $sale = $this->getRequest()->getSession()->read("sale");
            $total = 0;
            foreach($sale['products'] as $id => $product){
                $total = $total + $product['quantity']*$product['price'];
            }
            $articles_total = 0;
            if(!empty($sale['articles'])){
                foreach($sale['articles'] as $id => $article){
                    $articles_total = $articles_total + $article['price'];
                }
            }
            
            $sale['products_total'] = $total;
            $sale['articles_total'] = $articles_total;
            $sale['total'] = $total+ $articles_total;
            echo json_encode($sale);
        }
        die();
    }

    public function addproductedit(){
        if ($this->request->is(['ajax'])){
            $this->loadModel('ProductsSales');
            // get PS 
            $ps = $this->Sales->ProductsSales->find("all", array("conditions" => array('product_id' => $this->request->getData()['product_id'], 'sale_id' => $this->request->getData()['sale_id'])))->first();

            $sale = $this->Sales->get($this->request->getData()['sale_id']);

            // check if PS exists
            if(!empty($ps)){
                if($this->request->getData()['quantity'] != 0 || $this->request->getData()['quantity_delivered'] != 0){
                    $ps->quantity_ordered = $this->request->getData()['quantity'];
                    $ps->total_ordered = $this->request->getData()['quantity']*$this->request->getData()['price'];
                    $ps->quantity_delivered = $this->request->getData()['quantity_delivered'];
                    $ps->total_delivered = $this->request->getData()['quantity_delivered']*$this->request->getData()['price'];
                    if($this->ProductsSales->save($ps)){
                        // TODO : edit related promotion if exists
                    }
                }else{
                    if($this->ProductsSales->delete($ps)){
                        // TODO : delete related promotion if exists
                    }
                }
            }else{
               if($this->request->getData()['quantity'] != 0 || $this->request->getData()['quantity_delivered'] != 0){
                // create new PS 
                $prd = $this->Sales->Products->get($this->request->getData()['product_id']);
                $ps = $this->ProductsSales->newEmptyEntity();
                $ps->product_id = $this->request->getData()['product_id'];
                $ps->price = $this->request->getData()['price'];
                $ps->sale_id = $this->request->getData()['sale_id'];
                if(!empty($this->request->getData()['quantity'])){
                    $ps->quantity_ordered = $this->request->getData()['quantity'];
                    $ps->total_ordered = $this->request->getData()['quantity']*$this->request->getData()['price'];
                }else{
                    $ps->quantity_ordered = 0;
                    $ps->total_ordered = 0;
                }
                if(!empty($this->request->getData()['quantity_delivered'])){
                    $ps->quantity_delivered = $this->request->getData()['quantity_delivered'];
                    $ps->total_delivered = $this->request->getData()['quantity_delivered']*$this->request->getData()['price'];
                }else{
                    $ps->quantity_delivered = 0;
                    $ps->total_delivered = 0;
                }
                
                $ps->list_price = $prd->price;
                if($this->ProductsSales->save($ps)){
                    // add related promotion if exists
                    $this->addDirectPromo($ps->sale_id, $ps->product_id, $ps->quantity_ordered, $ps->quantity_delivered, $sale->zone_id);
                }
               }
            }

            // calculate new sale totals 
            $sale = $this->Sales->get($this->request->getData()['sale_id'], ['contain' => ['ProductsSales'=> ['Products'], 'ArticlesSales' => ['Articles']]]);
            $total_ordered = 0; $total_sold = 0; foreach($sale->products_sales as $ps){
                $total_ordered = $total_ordered + $ps->price*$ps->quantity_ordered; 
                $total_sold = $total_sold + $ps->price*$ps->quantity_delivered; 
            }

            foreach($sale->articles_sales as $article){
                $total_ordered = $total_ordered + $article->price*$article->quantity; 
                $total_sold = $total_sold + $article->price_delivered*$ps->quantity_delivered; 
            }

            $sale->total_ordered = $total_ordered;
            $sale->total_sold = $total_sold; 
            $this->Sales->save($sale);
            $sale->total_ordered = $total_ordered;
            $sale->total_sold = $total_sold; 
            $this->Sales->save($sale);
            echo json_encode($sale);
        }
        die();
    }

    public function addDirectPromo($sale_id, $product_id, $quantity_ordered, $quantity_delivered, $zone_id){
        $promotions = $this->getRequest()->getSession()->read("promotions");
        if(!empty($promotions[$product_id])){
            $promo = $promotions[$product_id];
            if(empty($promo['zone_id']) || $promo['zone_id'] == $zone_id){
                if($promo['type'] == 2 || $promo['type'] == 3){
                    $start_quantity = $promo['start_quantity'];
                    $end_quantity = $promo['end_quantity'];
                    $multiple_ordered = intdiv((int)$quantity_ordered, (int)$start_quantity);
                    $multiple_delivered = intdiv((int)$quantity_delivered, (int)$start_quantity);
                    $toadd_quantity_ordered = $end_quantity*$multiple_ordered; 
                    $toadd_quantity_delivered = $end_quantity*$multiple_delivered;

                    $article = $this->Sales->ArticlesSales->newEmptyEntity(); 
                    $article->sale_id = $sale_id;
                    $article->quantity = $toadd_quantity_ordered;
                    $article->price = $toadd_quantity_ordered*$promo['article']->price;
                    $article->cost = $toadd_quantity_delivered*$promo['article']->cost;
                    $article->quantity_delivered = $toadd_quantity_delivered;
                    $article->price_delivered = $toadd_quantity_delivered*$promo['article']->price;
                    $article->article_id = $promo['article']->id;
                    $article->promotion_id = $promo['id'];
                    $this->Sales->ArticlesSales->save($article);
                }   
            }
        }
    }

    public function editarticle(){
        if ($this->request->is(['ajax'])){
            $as = $this->Sales->ArticlesSales->get($this->request->getData()['as_id'], ['contain' => ['Articles']]);

            if($this->request->getData()['quantity_delivered'] == 0 && $this->request->getData()['quantity'] == 0){
                $this->Sales->ArticlesSales->delete($as);
            }else{
                $as->quantity = $this->request->getData()['quantity'];
                $as->quantity_delivered = $this->request->getData()['quantity_delivered'];
                $as->price = $this->request->getData()['quantity']*$as->article->price;
                $as->price_delivered = $this->request->getData()['quantity_delivered']*$as->article->price;
                $this->Sales->ArticlesSales->save($as);
            }

            $sale = $this->Sales->get($this->request->getData()['sale_id'], ['contain' => ['ProductsSales'=> ['Products'], 'ArticlesSales' => ['Articles']]]);
            $total_ordered = 0; $total_sold = 0; 

            foreach($sale->products_sales as $ps){
                $total_ordered = $total_ordered + $ps->price*$ps->quantity_ordered; 
                $total_sold = $total_sold + $ps->price*$ps->quantity_delivered; 
            }

            foreach($sale->articles_sales as $article){
                $total_ordered = $total_ordered + $article->price; 
                $total_sold = $total_sold + $article->price_delivered; 
            }

            $sale->total_ordered = $total_ordered;
            $sale->total_sold = $total_sold; 
            $this->Sales->save($sale);
            echo json_encode($sale);
       } 
       die();
    }

    public function returnsession(){
        if ($this->request->is(['ajax'])){
            if(!empty($this->getRequest()->getSession()->read("sale"))){
                $sale = $this->getRequest()->getSession()->read("sale");
            }else{
                $sale = array();
                $sale['products'] = array();
            }
            $total = 0;
            foreach($sale['products'] as $id => $product){
                $total = $total + $product['quantity']*$product['price'];
            }
            $sale['total'] = $total;
            echo json_encode($sale);
        }

        die();
    }

    public function deleteproductedit(){
        if ($this->request->is(['ajax'])){
            $ps = $this->Sales->ProductsSales->get($this->request->getData()['ps_id']);

            $ps_id = $ps->id;
            $sale_id = $ps->sale_id;
            $this->Sales->ProductsSales->delete($ps);
            $sale = $this->recalculate($sale_id);
            echo json_encode($sale);
        }
        die();
    }


    public function deletearticleedit(){
        if ($this->request->is(['ajax'])){
            $as = $this->Sales->ArticlesSales->get($this->request->getData()['as_id']);

            $as_id = $as->id;
            $sale_id = $as->sale_id;
            $this->Sales->ArticlesSales->delete($as);
            $sale = $this->recalculate($sale_id);
            echo json_encode($sale);
        }
        die();
    }

    public function recalculate($id){
        $sale = $this->Sales->get($id, ['contain' => ['ProductsSales' => ['Products'], 'ArticlesSales' => ['Articles']]]);
        $total_ordered = 0;
        $total_sold = 0; 
        foreach($sale->products_sales as $ps){
            $total_sold = $total_sold + $ps->quantity_delivered*$ps->price;
            $total_ordered = $total_ordered + $ps->quantity_ordered*$ps->price;
        }

        foreach($sale->articles_sales as $article){
            $total_ordered = $total_ordered + $article->price*$article->quantity; 
            $total_sold = $total_sold + $article->price_delivered*$ps->quantity_delivered; 
        }

        $sale->total_ordered = $total_ordered;
        $sale->total_sold = $total_sold; 
        $this->Sales->save($sale);
        return $sale;
    }

    public function getsale(){
        if ($this->request->is(['ajax'])){
            $sale = $this->Sales->get($this->request->getData()['sale_id'], ['contain' => ['ProductsSales' => ['Products']]]);
            echo json_encode($sale);
        }
        die();
    }

    public function update(){
        if ($this->request->is(['patch', 'post', 'put'])){
            $sale = $this->Sales->get($this->request->getData()['sale_id']);
            $this->loadModel('ProductsSales');
            if($sale->total_sold > 0 || $this->request->getData()['delivered'] == 1){
                $sale->delivered = 1;
                $sale->type = 2;
            }
            if($sale->delivery_date < $this->request->getData()['delivery_date']){
                $sale->last_delivery_date = $sale->delivery_date; 
                $sale->delivery_date = $this->request->getData()['delivery_date'];
            }else{
                $sale->last_delivery_date = null;
            }

            if(!empty($this->request->getData()['last_delivery_date'])){
                $sale->last_delivery_date = $this->request->getData()['last_delivery_date'];
            }

            $sale->problem = $this->request->getData()['problem'];
            $sale->note = $this->request->getData()['note'];
            $sale->status  = $this->request->getData()['status'];

            $this->Sales->save($sale);

            return $this->redirect(['action' => 'add', 'controller' => "Calls"]);
        }
    }

    public function refresh(){
        $sale = array();
        $sale['products'] = array();
        $this->getRequest()->getSession()->write("sale", $sale);
        return $this->redirect(['action' => 'add']);
    }

    public function directrefresh(){
        $sale = $this->getRequest()->getSession()->read("sale");
        $sale['products'] = array();
        $sale['customer_id'] = null;
        $sale['customer_info'] = null;
        $sale['caravane_id'] = null;
        $this->getRequest()->getSession()->write("sale", $sale);
        return $this->redirect(['action' => 'direct']);
    }


    public function cancel(){
        $this->getRequest()->getSession()->write("sale", []);
        $this->getRequest()->getSession()->write("call", []);
        return $this->redirect(['controller' => 'Calls', 'action' => 'add']);
    }

    public function removeproduct($id){
        $this->removePromotions($id);
        $ongoing_sale = $this->getRequest()->getSession()->read("sale");
        unset($ongoing_sale['products'][$id]);
        $this->getRequest()->getSession()->write("sale", $ongoing_sale);
        return $this->redirect(['action' => 'add']);
    }

    public function removePromotions($id){
        $ongoing_sale = $this->getRequest()->getSession()->read("sale");
        $promotions = $this->getRequest()->getSession()->read("promotions");
        if(!empty($promotions[$id])){
            $promo = $promotions[$id];
            if($promo['type'] == 2 || $promo['type'] == 3){
                $product_to_remove = $promo['target_product_id']; 
                $product = $ongoing_sale['products'][$product_to_remove];
                if($product['price'] == 0){
                    unset($ongoing_sale['products'][$product_to_remove]);
                }
            }
        }
        $this->getRequest()->getSession()->write("sale", $ongoing_sale);
    }

    public function removedirectproduct($id){
        $this->removePromotions($id);
        $ongoing_sale = $this->getRequest()->getSession()->read("sale");
        unset($ongoing_sale['products'][$id]);
        $this->getRequest()->getSession()->write("sale", $ongoing_sale);
        return $this->redirect(['action' => 'direct']);
    }

    public function resetsales(){
        $sales = $this->Sales->find("all");
        $sale_number = 2000;
        foreach($sales as $sale){
            $sale_number++;
            $sale->sale_number = $sale_number;
            $this->Sales->save($sale);
        }
        die("Done Resetting sale numbers");
    }

    public function savedirect(){
        $ongoing_sale = $this->getRequest()->getSession()->read("sale");
        // debug($ongoing_sale); die();
        $sale = $this->Sales->newEmptyEntity();
        $hour = date("H:i:s");
        $created = $this->request->getData()['created'] . " " . $hour;
        $sale->created = $created;
        $sale->sale_number = $this->Sales->find("all")->count() + 2000;
        do{
           $sale->sale_number = $sale->sale_number + 1; 
        }while($this->Sales->find("all", array("conditions" => array("sale_number" => $sale->sale_number)))->count() > 0);
        $sale->user_id = $this->Auth->user()['id'];
        $caravane = $this->Caravanes->get($ongoing_sale['caravane_id']);
        $sale->center_id = $caravane->center_id;
        $sale->customer_id = $ongoing_sale['customer_id'];
        $sale->caravane_id = $ongoing_sale['caravane_id'];
        $sale->zone_id = $ongoing_sale['customer_info']->zone_id;
        $sale->status = 1; // 1 : active - 0 : canceled
        if ($this->request->is(['patch', 'post', 'put'])){
            $sale->delivery_date = $this->request->getData()['delivery_date'];
            $sale->note = $this->request->getData()['note'];
        }
        $total_sold = 0;
        foreach($ongoing_sale['products'] as $id => $product){
            $total_sold = $total_sold + $product['quantity']*$product['price'];
        }
        if(!empty($ongoing_sale['articles'])){
            foreach($ongoing_sale['articles'] as $id => $article){
            $total_sold = $total_sold + $article['price'];
        }
        }
        
        $sale->type = 3; // 1 :proforma - 2 : vente
        $sale->total_sold = $total_sold;
        if($ident = $this->Sales->save($sale)){
            $this->loadModel('ProductsSales');
            foreach($ongoing_sale['products'] as $id => $product){
                $prd = $this->Sales->Products->get($id);
                $ps = $this->ProductsSales->newEmptyEntity();
                $ps->product_id = $id;
                $ps->price = $product['price'];
                $ps->sale_id = $ident->id;
                $ps->quantity_delivered = $product['quantity'];
                $ps->total_delivered = $product['quantity']*$product['price'];
                $ps->list_price = $product['list_price'];
                $prdsale = $this->ProductsSales->save($ps);
            }


            if(!empty($ongoing_sale['articles'])){
                $this->loadModel('ArticlesSales');
                foreach($ongoing_sale['articles'] as $id => $article){
                    $as = $this->ArticlesSales->newEmptyEntity();
                    $as->sale_id = $ident->id;
                    $as->quantity = 0;
                    $as->price = 0;
                    $as->cost = $article['cost'];
                    $as->article_id = $id;
                    $as->promotion_id = $article['promotion_id'];
                    $as->price_delivered = $article['price'];
                    $as->quantity_delivered = $article['quantity'];
                    $this->ArticlesSales->save($as);
                }
            }

            $this->init();
            $this->getRequest()->getSession()->write("sale", []);
        }

        return $this->redirect(['controller' => 'Calls', 'action' => 'add']);
    }

    public function save(){
        $ongoing_sale = $this->getRequest()->getSession()->read("sale");
        if ($this->request->is(['patch', 'post', 'put'])){
            $ongoing_sale['delivery_date'] = $this->request->getData()['delivery_date'];
        }
        $call = $this->getRequest()->getSession()->read("call");
        $sale = $this->Sales->newEmptyEntity();
        $sale->sale_number = $this->Sales->find("all")->count() + 2000;
        do{
           $sale->sale_number = $sale->sale_number + 1; 
        }while($this->Sales->find("all", array("conditions" => array("sale_number" => $sale->sale_number)))->count() > 0);

        $hour = date("H:i:s");
        $created = $this->request->getData()['created'] . " " . $hour;
        $sale->created = $created;
        $sale->user_id = $this->Auth->user()['id'];
        $caravane = $this->Caravanes->get($ongoing_sale['caravane_id']);
        $sale->center_id = $caravane->center_id;
        $sale->customer_id = $call['customer_id'];
        $sale->caravane_id = $call['caravane_id'];
        $sale->zone_id = $call['customer_info']->zone_id;
        $sale->status = 1; // 1 : active - 0 : canceled
        if ($this->request->is(['patch', 'post', 'put'])){
            $sale->delivery_date = $this->request->getData()['delivery_date'];
            $sale->note = $this->request->getData()['note'];
        }
        $total_ordered = 0;
        foreach($ongoing_sale['products'] as $id => $product){
            $total_ordered = $total_ordered + $product['quantity']*$product['price'];
        }
        foreach($ongoing_sale['articles'] as $id => $article){
            $total_ordered = $total_ordered + $article['price'];
        }
        $sale->type = 1; // 1 :proforma - 2 : vente
        $sale->total_ordered = $total_ordered;
        if($ident = $this->Sales->save($sale)){
            $this->loadModel('Calls');
            $call = $this->Calls->get($call['call_id']);
            $call->sale_id = $ident->id;
            $this->Calls->save($call);
            $this->loadModel('ProductsSales');
            foreach($ongoing_sale['products'] as $id => $product){
                $prd = $this->Sales->Products->get($id);
                $ps = $this->ProductsSales->newEmptyEntity();
                $ps->product_id = $id;
                $ps->price = $product['price'];
                $ps->price_delivered = 0;
                $ps->quantity_delivered = 0;
                $ps->sale_id = $ident->id;
                $ps->quantity_ordered = $product['quantity'];
                $ps->total_ordered = $product['quantity']*$product['price'];
                $ps->list_price = $product['list_price'];
                if(!empty($product['promotion_id'])){
                    $ps->promotion_id = $product['promotion_id'];
                }
                $this->ProductsSales->save($ps);
            }

            if(!empty($ongoing_sale['articles'])){
                $this->loadModel('ArticlesSales');
                foreach($ongoing_sale['articles'] as $id => $article){
                    $as = $this->ArticlesSales->newEmptyEntity();
                    $as->sale_id = $ident->id;
                    $as->article_id = $id;
                    $as->promotion_id = $article['promotion_id'];
                    $as->price = $article['price'];
                    $as->cost = $article['cost'];
                    $as->quantity = $article['quantity'];
                    $this->ArticlesSales->save($as);
                }
            }

            $this->init();
            $this->getRequest()->getSession()->write("sale", []);
        }

        return $this->redirect(['controller' => 'Calls', 'action' => 'add']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Sale id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $sale = $this->Sales->get($id, [
            'contain' => ['ProductsSales' => ['Products'] , 'Customers', 'Caravanes', 'Zones', 'ArticlesSales' => ['Articles']],
        ]);
        ini_set('memory_limit', '-1');
        $categories_list = [];
        $this->loadModel('Categories');
        $breadcrumb = array('Editer Proforma' => '/sales/edit/'.$id, $sale->caravane->name => '#', $sale->customer->name => '#', $sale->sale_number => '#');
            $categories = $this->Categories->find("all")->contain(['Caravanes', 'Products' => ['sort' => "Products.name ASC", 'ProductsCenters']]); 
            $caravane_id = $sale->caravane_id;
            $categories = $categories->matching('Caravanes', function ($query) use ($caravane_id) {
            return $query->where(['Caravanes.id' => $caravane_id]);
        });
            foreach($categories as $cat){
               $categories_list[$cat->id] = $cat->name; 
            }
        $this->set(compact('sale', 'categories', 'breadcrumb', 'categories_list'));
    }

    public function load($id){
        
        $this->getRequest()->getSession()->write("sale", $sale);
    }


    public function export($id){
        require_once(ROOT . DS . 'vendor' . DS  . 'fpdf'  . DS . 'fpdf.php');

        $sale = $this->Sales->get($id, array("contain" => array('Customers', 'Caravanes', 'Zones', 'Products')));

        $fpdf = new FPDF();
        $fpdf->AddPage('L');
        $fpdf->Image(ROOT.'/webroot/img/logo.jpg',10,4,20);
        $fpdf->Ln(18);
        $fpdf->SetFont('Arial','B',9);
        $fpdf->Cell(135,7,"#",'T,B',0, 'L');
        $fpdf->Cell(140,7,$sale->sale_number,'T,B',0, 'R');
        $fpdf->Ln();
        $fpdf->Cell(135,7,"Type",'B',0, 'L');
        if($sale->type == 1){
            $fpdf->Cell(140,7,'PROFORMA','B',0, 'R');
        }elseif($sale->type == 2){
            $fpdf->Cell(140,7,'VENTE','B',0, 'R');
        }else{
            $fpdf->Cell(140,7,'VENTE DIRECTE','B',0, 'R');
        }
        
        $fpdf->Ln();
        $fpdf->Cell(135,7,"Client",'B',0, 'L');
        $fpdf->Cell(140,7,utf8_decode($sale->customer->name),'B',0, 'R');
        $fpdf->Ln();
        $fpdf->Cell(135,7,"Zone",'B',0, 'L');
        $fpdf->Cell(140,7,$sale->zone->name,'B',0, 'R');
        $fpdf->Ln();
        $fpdf->Cell(135,7,"Caravane",'B',0, 'L');
        $fpdf->Cell(140,7,$sale->caravane->identification,'B',0, 'R');
        $fpdf->Ln();
        if(!empty($sale->problem)){
            $fpdf->Cell(135,7,utf8_decode("Problème"),'B',0, 'L');
            $fpdf->Cell(140,7,'OUI','B',0, 'R');
            $fpdf->Ln();
        }
        if(!empty($sale->note)){
            $fpdf->Cell(135,7,"Note",'B',0, 'L');
            $fpdf->Cell(140,7,$sale->note,'B',0, 'R');
            $fpdf->Ln();
        }

        $fpdf->Cell(135,7,"Statut",'B',0, 'L');
        if($sale->status == 1){
            $fpdf->Cell(140,7,'Actif','B',0, 'R');
        }else{
            $fpdf->Cell(140,7,'Annulé','B',0, 'R');
        }
        $fpdf->Ln();

        $fpdf->Cell(135,7,utf8_decode("Livré"),'B',0, 'L');
        if($sale->delivered == 1){
            $fpdf->Cell(140,7,'OUI','B',0, 'R');
        }else{
            $fpdf->Cell(140,7,'NON','B',0, 'R');
        }
        $fpdf->Ln();
        
        $fpdf->Cell(135,7,"Date de Livraison",'B',0, 'L');
        $fpdf->Cell(140,7,$sale->delivery_date,'B',0, 'R');

        $fpdf->Ln();
        
        $fpdf->Cell(135,7,utf8_decode("Dernière Date de Livraison"),'B',0, 'L');
        $fpdf->Cell(140,7,$sale->last_delivery_date,'B',0, 'R');
        $fpdf->Ln(15);

        $fpdf->SetFont('Arial','B',8);
        $fpdf->Cell(105,7,'PRODUIT','T,L',0, 'L');
        $fpdf->Cell(30,7,'COMMANDE','T,L',0, 'C');
        $fpdf->Cell(30,7,'LIVRE','T,L',0, 'C');
        $fpdf->Cell(30,7,'PRIX','T,L',0, 'C');
        $fpdf->Cell(40,7,'TOTAL','T,L',0, 'C');
        $fpdf->Cell(40,7,'TOTAL LIVRE','T,R,L',0, 'C');
        $fpdf->SetFont('Arial','',8);

        foreach($sale->products as $product){
            $fpdf->Ln();
            $fpdf->Cell(105,7,$product->name,'T,L,B',0, 'L');
            $fpdf->Cell(30,7,$product->_joinData->quantity_ordered,'T,L,B',0, 'C');
            $fpdf->Cell(30,7,'','T,L,B',0, 'C');
            $fpdf->Cell(30,7,number_format((float)$product['_joinData']->price, 2, ".", ","),'T,L,B',0, 'C');
            $fpdf->Cell(40,7,number_format((float)$product['_joinData']->total_ordered, 2, ".", ","),'T,R,L,B',0, 'C');
            $fpdf->Cell(40,7,number_format((float)$product['_joinData']->total_delivered, 2, ".", ","),'T,R,L,B',0, 'C');
        }

        $fpdf->SetFont('Arial','B',8);
        $fpdf->Ln();
        $fpdf->Cell(195,7,'',0,0, 'R');
        $fpdf->Cell(40,7,number_format((float)$sale->total_ordered, 2, ".", ",")." USD",'R,L,B',0, 'C');
        $fpdf->Cell(40,7,number_format((float)$sale->total_sold, 2, ".", ",")." USD",'R,L,B',0, 'C');
        $fpdf->Output('I');
        die();
    }

    /**
     * Delete method
     *
     * @param string|null $id Zone id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $sale = $this->Sales->get($id);
        $this->Sales->delete($sale);

        return $this->redirect($this->referer());
    }


}
