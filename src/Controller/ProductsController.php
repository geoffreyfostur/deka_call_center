<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Datasource\ConnectionManager;

/**
 * Products Controller
 *
 * @property \App\Model\Table\ProductsTable $Products
 * @method \App\Model\Entity\Product[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ProductsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Categories', 'Groupings', 'Users'],
        ];
        $products = $this->paginate($this->Products);

        $this->set(compact('products'));
    }

    /**
     * View method
     *
     * @param string|null $id Product id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $product = $this->Products->get($id, [
            'contain' => ['Categories', 'Groupings', 'Users', 'Centers', 'Sales', 'Promotions'],
        ]);

        $this->set(compact('product'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $product = $this->Products->newEmptyEntity();
        if ($this->request->is('post')) {
            $product = $this->Products->patchEntity($product, $this->request->getData());
            if ($this->Products->save($product)) {
                $this->Flash->success(__('The product has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The product could not be saved. Please, try again.'));
        }
        $categories = $this->Products->Categories->find('list', ['limit' => 200]);
        $groupings = $this->Products->Groupings->find('list', ['limit' => 200]);
        $users = $this->Products->Users->find('list', ['limit' => 200]);
        $centers = $this->Products->Centers->find('list', ['limit' => 200]);
        $sales = $this->Products->Sales->find('list', ['limit' => 200]);
        $this->set(compact('product', 'categories', 'groupings', 'users', 'centers', 'sales'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Product id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $product = $this->Products->get($id, [
            'contain' => ['Centers', 'Sales'],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $product = $this->Products->patchEntity($product, $this->request->getData());
            if ($this->Products->save($product)) {
                $this->Flash->success(__('The product has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The product could not be saved. Please, try again.'));
        }
        $categories = $this->Products->Categories->find('list', ['limit' => 200]);
        $groupings = $this->Products->Groupings->find('list', ['limit' => 200]);
        $users = $this->Products->Users->find('list', ['limit' => 200]);
        $centers = $this->Products->Centers->find('list', ['limit' => 200]);
        $sales = $this->Products->Sales->find('list', ['limit' => 200]);
        $this->set(compact('product', 'categories', 'groupings', 'users', 'centers', 'sales'));
    }

    public function report(){
        $this->loadModel('Caravanes');$this->loadModel('Zones');
        $from = $this->getRequest()->getSession()->read("from")." 00:00:00";
        $to = $this->getRequest()->getSession()->read("to")." 23:59:59";
        $caravane_id = '';
        $zone_id = '';
        $condition = '';
        if ($this->request->is(['patch', 'post', 'put'])){
            if(!empty($this->request->getData()['caravane_id'])){
                $caravane_id = $this->request->getData()['caravane_id'];
                $condition .= " AND s.caravane_id =".$this->request->getData()['caravane_id'];
            }
            if(!empty($this->request->getData()['zone_id'])){
                $zone_id = $this->request->getData()['zone_id'];
                $condition .= " AND s.zone_id =".$this->request->getData()['zone_id'];
            }
            if(!empty($this->request->getData()['category_id'])){
                $category_id = $this->request->getData()['category_id'];
                $condition .= " AND c.id =".$this->request->getData()['category_id'];
            }
        }
        
        $SQL = "SELECT c.name as cat_name, p.id, p.name, SUM(ps.quantity_ordered) as quantity_ordered, SUM(ps.quantity_delivered) as quantity_delivered, SUM(ps.total_ordered) as total_ordered, SUM(ps.total_delivered) as total_delivered 
            FROM `products_sales` ps 
            LEFT JOIN sales s ON s.id = ps.sale_id
            LEFT JOIN products p ON ps.product_id = p.id
            LEFT JOIN categories c ON c.id = p.category_id
            WHERE s.created >= '".$from."' AND s.created <= '".$to."' AND s.user_id = ".$this->Auth->user()['id']." AND s.status = 1 ".$condition."
            GROUP BY p.id ORDER BY p.id";

        $conn = ConnectionManager::get('default');
        $caravanes = $this->Caravanes->find("list", ['order' => ['name ASC'], 'conditions' => ['user_id' => $this->Auth->user()['id']]]);

        $user_id = $this->Auth->user()['id'];
        $SQL2 = "SELECT DISTINCT z.*, c.user_id
                FROM zones z 
                LEFT JOIN customers cu ON cu.zone_id = z.id
                LEFT JOIN customers_caravanes cc ON cc.customer_id = cu.id
                LEFT JOIN caravanes c ON c.id = cc.caravane_id
                WHERE c.user_id = ".$user_id." ORDER BY z.name ASC";

        $conn = ConnectionManager::get('default');
        $zs = $conn->query($SQL2); 
        $zones = [];
        foreach($zs as $z){
            $zones[$z['id']] = $z['name'];
        }

        $this->loadModel('Centers');
        $products = $conn->query($SQL); 

        if ($this->request->is(['patch', 'post', 'put'])){
            if(!empty($this->request->getData()['export']) && $this->request->getData()['export'] == 1){
                $this->export($products, $caravane_id, $zone_id, $category_id, $center_id);
            }
        }
        $breadcrumb = array('Rapport Produits' => '/products/report');
        $this->set(compact('products', 'from', 'to', 'breadcrumb', 'caravanes', 'zones',  'zone_id', 'caravane_id'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Product id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $product = $this->Products->get($id);
        if ($this->Products->delete($product)) {
            $this->Flash->success(__('The product has been deleted.'));
        } else {
            $this->Flash->error(__('The product could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
