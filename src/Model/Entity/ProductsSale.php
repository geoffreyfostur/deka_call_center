<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ProductsSale Entity
 *
 * @property int $id
 * @property int $product_id
 * @property int $sale_id
 * @property string $list_price
 * @property string $price
 * @property int $quantity_ordered
 * @property int|null $quantity_delivered
 * @property string $total_ordered
 * @property string|null $total_delivered
 * @property int|null $promotion_id
 *
 * @property \App\Model\Entity\Product $product
 * @property \App\Model\Entity\Sale $sale
 * @property \App\Model\Entity\Promotion $promotion
 */
class ProductsSale extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'product_id' => true,
        'sale_id' => true,
        'list_price' => true,
        'price' => true,
        'quantity_ordered' => true,
        'quantity_delivered' => true,
        'total_ordered' => true,
        'total_delivered' => true,
        'promotion_id' => true,
        'product' => true,
        'sale' => true,
        'promotion' => true,
    ];
}
