<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CategoriesCaravane Entity
 *
 * @property int $id
 * @property int $category_id
 * @property int $caravane_id
 *
 * @property \App\Model\Entity\Category $category
 * @property \App\Model\Entity\Caravane $caravane
 */
class CategoriesCaravane extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'category_id' => true,
        'caravane_id' => true,
        'category' => true,
        'caravane' => true,
    ];
}
