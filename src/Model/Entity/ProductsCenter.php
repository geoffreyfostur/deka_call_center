<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ProductsCenter Entity
 *
 * @property int $id
 * @property int $product_id
 * @property int $center_id
 * @property float $price
 *
 * @property \App\Model\Entity\Product $product
 * @property \App\Model\Entity\Center $center
 */
class ProductsCenter extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'product_id' => true,
        'center_id' => true,
        'price' => true,
        'product' => true,
        'center' => true,
    ];
}
