<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Product Entity
 *
 * @property int $id
 * @property string $name
 * @property int $category_id
 * @property int|null $grouping_id
 * @property float $price
 * @property float|null $promotion_price
 * @property \Cake\I18n\FrozenDate|null $promotion_start
 * @property \Cake\I18n\FrozenDate|null $promotion_end
 * @property int $user_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $visible
 * @property int $status
 *
 * @property \App\Model\Entity\Category $category
 * @property \App\Model\Entity\Grouping $grouping
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Promotion[] $promotions
 * @property \App\Model\Entity\Center[] $centers
 * @property \App\Model\Entity\Sale[] $sales
 */
class Product extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'category_id' => true,
        'grouping_id' => true,
        'price' => true,
        'promotion_price' => true,
        'promotion_start' => true,
        'promotion_end' => true,
        'user_id' => true,
        'created' => true,
        'modified' => true,
        'visible' => true,
        'status' => true,
        'category' => true,
        'grouping' => true,
        'user' => true,
        'promotions' => true,
        'centers' => true,
        'sales' => true,
    ];
}
