<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property int $id
 * @property string $name
 * @property string $username
 * @property string $password
 * @property string|null $email
 * @property int $status
 * @property int $role_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $visible
 * @property string|null $phone
 * @property int|null $caravane_id
 * @property int $center_id
 *
 * @property \App\Model\Entity\Role $role
 * @property \App\Model\Entity\Caravane[] $caravanes
 * @property \App\Model\Entity\Center $center
 * @property \App\Model\Entity\Call[] $calls
 * @property \App\Model\Entity\Customer[] $customers
 * @property \App\Model\Entity\Product[] $products
 * @property \App\Model\Entity\Promotion[] $promotions
 * @property \App\Model\Entity\Sale[] $sales
 */
class User extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'username' => true,
        'password' => true,
        'email' => true,
        'status' => true,
        'role_id' => true,
        'created' => true,
        'modified' => true,
        'visible' => true,
        'phone' => true,
        'caravane_id' => true,
        'center_id' => true,
        'role' => true,
        'caravanes' => true,
        'center' => true,
        'calls' => true,
        'customers' => true,
        'products' => true,
        'promotions' => true,
        'sales' => true,
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password',
    ];
}
