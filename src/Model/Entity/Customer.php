<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Customer Entity
 *
 * @property int $id
 * @property string $name
 * @property string|null $phone
 * @property string|null $address
 * @property string|null $lattitude
 * @property string|null $longitude
 * @property string|null $note
 * @property int $status
 * @property int $zone_id
 * @property int|null $route_id
 * @property int $user_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $visible
 * @property string|null $second_phone
 * @property string|null $nif
 * @property string|null $company
 *
 * @property \App\Model\Entity\Zone $zone
 * @property \App\Model\Entity\Route $route
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Call[] $calls
 * @property \App\Model\Entity\Sale[] $sales
 * @property \App\Model\Entity\Caravane[] $caravanes
 */
class Customer extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'phone' => true,
        'address' => true,
        'lattitude' => true,
        'longitude' => true,
        'note' => true,
        'status' => true,
        'zone_id' => true,
        'route_id' => true,
        'user_id' => true,
        'created' => true,
        'modified' => true,
        'visible' => true,
        'second_phone' => true,
        'nif' => true,
        'company' => true,
        'zone' => true,
        'route' => true,
        'user' => true,
        'calls' => true,
        'sales' => true,
        'caravanes' => true,
    ];
}
