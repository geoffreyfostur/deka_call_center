<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Call Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $customer_id
 * @property int $zone_id
 * @property int $route_id
 * @property int $caravane_id
 * @property int $answered
 * @property string|null $note
 * @property int|null $sale_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $visible
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Customer $customer
 * @property \App\Model\Entity\Zone $zone
 * @property \App\Model\Entity\Route $route
 * @property \App\Model\Entity\Caravane $caravane
 * @property \App\Model\Entity\Sale $sale
 */
class Call extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'customer_id' => true,
        'zone_id' => true,
        'route_id' => true,
        'caravane_id' => true,
        'answered' => true,
        'note' => true,
        'sale_id' => true,
        'created' => true,
        'modified' => true,
        'visible' => true,
        'user' => true,
        'customer' => true,
        'zone' => true,
        'route' => true,
        'caravane' => true,
        'sale' => true,
    ];
}
