<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Route Entity
 *
 * @property int $id
 * @property int $zone_id
 * @property string $name
 * @property int $visible
 *
 * @property \App\Model\Entity\Zone $zone
 * @property \App\Model\Entity\Call[] $calls
 * @property \App\Model\Entity\Customer[] $customers
 * @property \App\Model\Entity\Sale[] $sales
 */
class Route extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'zone_id' => true,
        'name' => true,
        'visible' => true,
        'zone' => true,
        'calls' => true,
        'customers' => true,
        'sales' => true,
    ];
}
