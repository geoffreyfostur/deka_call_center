<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Caravane Entity
 *
 * @property int $id
 * @property string $name
 * @property string|null $seller_name
 * @property string|null $seller_phone
 * @property string|null $identification
 * @property string|null $registration
 * @property int $user_id
 * @property int $status
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $visible
 * @property int $center_id
 *
 * @property \App\Model\Entity\User[] $users
 * @property \App\Model\Entity\Center $center
 * @property \App\Model\Entity\Call[] $calls
 * @property \App\Model\Entity\Sale[] $sales
 * @property \App\Model\Entity\Category[] $categories
 * @property \App\Model\Entity\Customer[] $customers
 * @property \App\Model\Entity\Zone[] $zones
 */
class Caravane extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'seller_name' => true,
        'seller_phone' => true,
        'identification' => true,
        'registration' => true,
        'user_id' => true,
        'status' => true,
        'created' => true,
        'modified' => true,
        'visible' => true,
        'center_id' => true,
        'users' => true,
        'center' => true,
        'calls' => true,
        'sales' => true,
        'categories' => true,
        'customers' => true,
        'zones' => true,
    ];
}
