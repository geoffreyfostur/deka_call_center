<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ArticlesSale Entity
 *
 * @property int $id
 * @property int $products_sale_id
 * @property int $sale_id
 * @property int $article_id
 * @property int $promotion_id
 * @property float $price
 * @property int $quantity
 * @property int $created
 * @property int $modified
 *
 * @property \App\Model\Entity\ProductsSale $products_sale
 * @property \App\Model\Entity\Sale $sale
 * @property \App\Model\Entity\Article $article
 * @property \App\Model\Entity\Promotion $promotion
 */
class ArticlesSale extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'products_sale_id' => true,
        'sale_id' => true,
        'article_id' => true,
        'promotion_id' => true,
        'quantity_delivered' => true,
        'price_delivered' => true,
        'price' => true,
        'quantity' => true,
        'created' => true,
        'modified' => true,
        'products_sale' => true,
        'sale' => true,
        'article' => true,
        'promotion' => true,
    ];
}
