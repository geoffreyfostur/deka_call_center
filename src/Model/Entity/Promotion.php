<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Promotion Entity
 *
 * @property int $id
 * @property int $type
 * @property \Cake\I18n\FrozenDate|null $start_date
 * @property \Cake\I18n\FrozenDate|null $end_date
 * @property int $product_id
 * @property float|null $promotion_price
 * @property int|null $target_product_id
 * @property float|null $start_quantity
 * @property float|null $end_quantity
 * @property int $user_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $keep_active
 *
 * @property \App\Model\Entity\Product $product
 * @property \App\Model\Entity\TargetProduct $target_product
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\ProductsSale[] $products_sales
 */
class Promotion extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'type' => true,
        'start_date' => true,
        'end_date' => true,
        'product_id' => true,
        'promotion_price' => true,
        'article_id' => true,
        'start_quantity' => true,
        'end_quantity' => true,
        'user_id' => true,
        'created' => true,
        'modified' => true,
        'keep_active' => true,
        'product' => true,
        'target_product' => true,
        'user' => true,
        'products_sales' => true,
        'zone_id' => true
    ];
}
