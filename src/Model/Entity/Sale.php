<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Sale Entity
 *
 * @property int $id
 * @property string $sale_number
 * @property int $customer_id
 * @property int $caravane_id
 * @property int $zone_id
 * @property int $route_id
 * @property int $status
 * @property \Cake\I18n\FrozenDate $delivery_date
 * @property int $delivered
 * @property int $type
 * @property string|null $note
 * @property int $problem
 * @property string $total_ordered
 * @property string $total_sold
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $user_id
 * @property \Cake\I18n\FrozenDate|null $last_delivery_date
 * @property int $visible
 *
 * @property \App\Model\Entity\Customer $customer
 * @property \App\Model\Entity\Caravane $caravane
 * @property \App\Model\Entity\Zone $zone
 * @property \App\Model\Entity\Route $route
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Call[] $calls
 * @property \App\Model\Entity\Product[] $products
 */
class Sale extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'sale_number' => true,
        'customer_id' => true,
        'caravane_id' => true,
        'zone_id' => true,
        'route_id' => true,
        'status' => true,
        'delivery_date' => true,
        'delivered' => true,
        'type' => true,
        'note' => true,
        'problem' => true,
        'total_ordered' => true,
        'total_sold' => true,
        'created' => true,
        'modified' => true,
        'user_id' => true,
        'last_delivery_date' => true,
        'visible' => true,
        'customer' => true,
        'caravane' => true,
        'zone' => true,
        'route' => true,
        'user' => true,
        'calls' => true,
        'products' => true,
        'center_id' => true
    ];
}
