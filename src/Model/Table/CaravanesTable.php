<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Caravanes Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\CentersTable&\Cake\ORM\Association\BelongsTo $Centers
 * @property \App\Model\Table\CallsTable&\Cake\ORM\Association\HasMany $Calls
 * @property \App\Model\Table\SalesTable&\Cake\ORM\Association\HasMany $Sales
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\HasMany $Users
 * @property \App\Model\Table\CategoriesTable&\Cake\ORM\Association\BelongsToMany $Categories
 * @property \App\Model\Table\CustomersTable&\Cake\ORM\Association\BelongsToMany $Customers
 * @property \App\Model\Table\ZonesTable&\Cake\ORM\Association\BelongsToMany $Zones
 *
 * @method \App\Model\Entity\Caravane newEmptyEntity()
 * @method \App\Model\Entity\Caravane newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Caravane[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Caravane get($primaryKey, $options = [])
 * @method \App\Model\Entity\Caravane findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Caravane patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Caravane[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Caravane|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Caravane saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Caravane[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Caravane[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Caravane[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Caravane[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CaravanesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('caravanes');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Centers', [
            'foreignKey' => 'center_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Calls', [
            'foreignKey' => 'caravane_id',
        ]);
        $this->hasMany('Sales', [
            'foreignKey' => 'caravane_id',
        ]);
        $this->hasMany('Users', [
            'foreignKey' => 'caravane_id',
        ]);
        $this->belongsToMany('Categories', [
            'foreignKey' => 'caravane_id',
            'targetForeignKey' => 'category_id',
            'joinTable' => 'categories_caravanes',
        ]);
        $this->belongsToMany('Customers', [
            'foreignKey' => 'caravane_id',
            'targetForeignKey' => 'customer_id',
            'joinTable' => 'customers_caravanes',
        ]);
        $this->belongsToMany('Zones', [
            'foreignKey' => 'caravane_id',
            'targetForeignKey' => 'zone_id',
            'joinTable' => 'zones_caravanes',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('seller_name')
            ->maxLength('seller_name', 255)
            ->allowEmptyString('seller_name');

        $validator
            ->scalar('seller_phone')
            ->maxLength('seller_phone', 255)
            ->allowEmptyString('seller_phone');

        $validator
            ->scalar('identification')
            ->maxLength('identification', 255)
            ->allowEmptyString('identification');

        $validator
            ->scalar('registration')
            ->maxLength('registration', 255)
            ->allowEmptyString('registration');

        $validator
            ->notEmptyString('status');

        $validator
            ->notEmptyString('visible');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'), ['errorField' => 'user_id']);
        $rules->add($rules->existsIn(['center_id'], 'Centers'), ['errorField' => 'center_id']);

        return $rules;
    }
}
