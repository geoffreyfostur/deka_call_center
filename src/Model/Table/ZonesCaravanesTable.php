<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ZonesCaravanes Model
 *
 * @property \App\Model\Table\ZonesTable&\Cake\ORM\Association\BelongsTo $Zones
 * @property \App\Model\Table\CaravanesTable&\Cake\ORM\Association\BelongsTo $Caravanes
 *
 * @method \App\Model\Entity\ZonesCaravane newEmptyEntity()
 * @method \App\Model\Entity\ZonesCaravane newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\ZonesCaravane[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ZonesCaravane get($primaryKey, $options = [])
 * @method \App\Model\Entity\ZonesCaravane findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\ZonesCaravane patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ZonesCaravane[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\ZonesCaravane|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ZonesCaravane saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ZonesCaravane[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ZonesCaravane[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\ZonesCaravane[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ZonesCaravane[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class ZonesCaravanesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('zones_caravanes');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Zones', [
            'foreignKey' => 'zone_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Caravanes', [
            'foreignKey' => 'caravane_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['zone_id'], 'Zones'), ['errorField' => 'zone_id']);
        $rules->add($rules->existsIn(['caravane_id'], 'Caravanes'), ['errorField' => 'caravane_id']);

        return $rules;
    }
}
