<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Zone[]|\Cake\Collection\CollectionInterface $zones
 */
?>

<div class="zones index content">
    <div class="table-responsive mt-10">
        <table class="table datatable">
            <thead>
                <tr>
                    <th style="text-align:left">Nom</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($zones as $zone): ?>
                <tr>
                    <td style="text-align:left"><a href="<?= ROOT_DIREC ?>/customers/filterbyurl/<?= $zone['id'] ?>"><?= $zone['name'] ?></a></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
