<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ProductsCenter[]|\Cake\Collection\CollectionInterface $productsCenters
 */
?>
<div class="productsCenters index content">
    <?= $this->Html->link(__('New Products Center'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Products Centers') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('product_id') ?></th>
                    <th><?= $this->Paginator->sort('center_id') ?></th>
                    <th><?= $this->Paginator->sort('price') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($productsCenters as $productsCenter): ?>
                <tr>
                    <td><?= $this->Number->format($productsCenter->id) ?></td>
                    <td><?= $productsCenter->has('product') ? $this->Html->link($productsCenter->product->name, ['controller' => 'Products', 'action' => 'view', $productsCenter->product->id]) : '' ?></td>
                    <td><?= $productsCenter->has('center') ? $this->Html->link($productsCenter->center->name, ['controller' => 'Centers', 'action' => 'view', $productsCenter->center->id]) : '' ?></td>
                    <td><?= $this->Number->format($productsCenter->price) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $productsCenter->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $productsCenter->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $productsCenter->id], ['confirm' => __('Are you sure you want to delete # {0}?', $productsCenter->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
