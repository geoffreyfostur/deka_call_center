<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ProductsCenter $productsCenter
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Products Center'), ['action' => 'edit', $productsCenter->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Products Center'), ['action' => 'delete', $productsCenter->id], ['confirm' => __('Are you sure you want to delete # {0}?', $productsCenter->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Products Centers'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Products Center'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="productsCenters view content">
            <h3><?= h($productsCenter->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Product') ?></th>
                    <td><?= $productsCenter->has('product') ? $this->Html->link($productsCenter->product->name, ['controller' => 'Products', 'action' => 'view', $productsCenter->product->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Center') ?></th>
                    <td><?= $productsCenter->has('center') ? $this->Html->link($productsCenter->center->name, ['controller' => 'Centers', 'action' => 'view', $productsCenter->center->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($productsCenter->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Price') ?></th>
                    <td><?= $this->Number->format($productsCenter->price) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
