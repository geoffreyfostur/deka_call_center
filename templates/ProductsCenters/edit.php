<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ProductsCenter $productsCenter
 * @var string[]|\Cake\Collection\CollectionInterface $products
 * @var string[]|\Cake\Collection\CollectionInterface $centers
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $productsCenter->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $productsCenter->id), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('List Products Centers'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="productsCenters form content">
            <?= $this->Form->create($productsCenter) ?>
            <fieldset>
                <legend><?= __('Edit Products Center') ?></legend>
                <?php
                    echo $this->Form->control('product_id', ['options' => $products]);
                    echo $this->Form->control('center_id', ['options' => $centers]);
                    echo $this->Form->control('price');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
