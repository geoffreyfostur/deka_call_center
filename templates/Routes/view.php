<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Route $route
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Route'), ['action' => 'edit', $route->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Route'), ['action' => 'delete', $route->id], ['confirm' => __('Are you sure you want to delete # {0}?', $route->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Routes'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Route'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="routes view content">
            <h3><?= h($route->name) ?></h3>
            <table>
                <tr>
                    <th><?= __('Zone') ?></th>
                    <td><?= $route->has('zone') ? $this->Html->link($route->zone->name, ['controller' => 'Zones', 'action' => 'view', $route->zone->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Name') ?></th>
                    <td><?= h($route->name) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($route->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Visible') ?></th>
                    <td><?= $this->Number->format($route->visible) ?></td>
                </tr>
            </table>
            <div class="related">
                <h4><?= __('Related Calls') ?></h4>
                <?php if (!empty($route->calls)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('User Id') ?></th>
                            <th><?= __('Customer Id') ?></th>
                            <th><?= __('Zone Id') ?></th>
                            <th><?= __('Route Id') ?></th>
                            <th><?= __('Caravane Id') ?></th>
                            <th><?= __('Answered') ?></th>
                            <th><?= __('Note') ?></th>
                            <th><?= __('Sale Id') ?></th>
                            <th><?= __('Created') ?></th>
                            <th><?= __('Modified') ?></th>
                            <th><?= __('Visible') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($route->calls as $calls) : ?>
                        <tr>
                            <td><?= h($calls->id) ?></td>
                            <td><?= h($calls->user_id) ?></td>
                            <td><?= h($calls->customer_id) ?></td>
                            <td><?= h($calls->zone_id) ?></td>
                            <td><?= h($calls->route_id) ?></td>
                            <td><?= h($calls->caravane_id) ?></td>
                            <td><?= h($calls->answered) ?></td>
                            <td><?= h($calls->note) ?></td>
                            <td><?= h($calls->sale_id) ?></td>
                            <td><?= h($calls->created) ?></td>
                            <td><?= h($calls->modified) ?></td>
                            <td><?= h($calls->visible) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Calls', 'action' => 'view', $calls->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Calls', 'action' => 'edit', $calls->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Calls', 'action' => 'delete', $calls->id], ['confirm' => __('Are you sure you want to delete # {0}?', $calls->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
            <div class="related">
                <h4><?= __('Related Customers') ?></h4>
                <?php if (!empty($route->customers)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('Name') ?></th>
                            <th><?= __('Phone') ?></th>
                            <th><?= __('Address') ?></th>
                            <th><?= __('Lattitude') ?></th>
                            <th><?= __('Longitude') ?></th>
                            <th><?= __('Note') ?></th>
                            <th><?= __('Status') ?></th>
                            <th><?= __('Zone Id') ?></th>
                            <th><?= __('Route Id') ?></th>
                            <th><?= __('User Id') ?></th>
                            <th><?= __('Created') ?></th>
                            <th><?= __('Modified') ?></th>
                            <th><?= __('Visible') ?></th>
                            <th><?= __('Second Phone') ?></th>
                            <th><?= __('Nif') ?></th>
                            <th><?= __('Company') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($route->customers as $customers) : ?>
                        <tr>
                            <td><?= h($customers->id) ?></td>
                            <td><?= h($customers->name) ?></td>
                            <td><?= h($customers->phone) ?></td>
                            <td><?= h($customers->address) ?></td>
                            <td><?= h($customers->lattitude) ?></td>
                            <td><?= h($customers->longitude) ?></td>
                            <td><?= h($customers->note) ?></td>
                            <td><?= h($customers->status) ?></td>
                            <td><?= h($customers->zone_id) ?></td>
                            <td><?= h($customers->route_id) ?></td>
                            <td><?= h($customers->user_id) ?></td>
                            <td><?= h($customers->created) ?></td>
                            <td><?= h($customers->modified) ?></td>
                            <td><?= h($customers->visible) ?></td>
                            <td><?= h($customers->second_phone) ?></td>
                            <td><?= h($customers->nif) ?></td>
                            <td><?= h($customers->company) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Customers', 'action' => 'view', $customers->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Customers', 'action' => 'edit', $customers->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Customers', 'action' => 'delete', $customers->id], ['confirm' => __('Are you sure you want to delete # {0}?', $customers->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
            <div class="related">
                <h4><?= __('Related Sales') ?></h4>
                <?php if (!empty($route->sales)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('Sale Number') ?></th>
                            <th><?= __('Customer Id') ?></th>
                            <th><?= __('Caravane Id') ?></th>
                            <th><?= __('Zone Id') ?></th>
                            <th><?= __('Route Id') ?></th>
                            <th><?= __('Status') ?></th>
                            <th><?= __('Delivery Date') ?></th>
                            <th><?= __('Delivered') ?></th>
                            <th><?= __('Type') ?></th>
                            <th><?= __('Note') ?></th>
                            <th><?= __('Problem') ?></th>
                            <th><?= __('Total Ordered') ?></th>
                            <th><?= __('Total Sold') ?></th>
                            <th><?= __('Created') ?></th>
                            <th><?= __('Modified') ?></th>
                            <th><?= __('User Id') ?></th>
                            <th><?= __('Last Delivery Date') ?></th>
                            <th><?= __('Visible') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($route->sales as $sales) : ?>
                        <tr>
                            <td><?= h($sales->id) ?></td>
                            <td><?= h($sales->sale_number) ?></td>
                            <td><?= h($sales->customer_id) ?></td>
                            <td><?= h($sales->caravane_id) ?></td>
                            <td><?= h($sales->zone_id) ?></td>
                            <td><?= h($sales->route_id) ?></td>
                            <td><?= h($sales->status) ?></td>
                            <td><?= h($sales->delivery_date) ?></td>
                            <td><?= h($sales->delivered) ?></td>
                            <td><?= h($sales->type) ?></td>
                            <td><?= h($sales->note) ?></td>
                            <td><?= h($sales->problem) ?></td>
                            <td><?= h($sales->total_ordered) ?></td>
                            <td><?= h($sales->total_sold) ?></td>
                            <td><?= h($sales->created) ?></td>
                            <td><?= h($sales->modified) ?></td>
                            <td><?= h($sales->user_id) ?></td>
                            <td><?= h($sales->last_delivery_date) ?></td>
                            <td><?= h($sales->visible) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Sales', 'action' => 'view', $sales->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Sales', 'action' => 'edit', $sales->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Sales', 'action' => 'delete', $sales->id], ['confirm' => __('Are you sure you want to delete # {0}?', $sales->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
