<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Route $route
 * @var string[]|\Cake\Collection\CollectionInterface $zones
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $route->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $route->id), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('List Routes'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="routes form content">
            <?= $this->Form->create($route) ?>
            <fieldset>
                <legend><?= __('Edit Route') ?></legend>
                <?php
                    echo $this->Form->control('zone_id', ['options' => $zones]);
                    echo $this->Form->control('name');
                    echo $this->Form->control('visible');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
