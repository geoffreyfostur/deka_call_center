<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Route $route
 * @var \Cake\Collection\CollectionInterface|string[] $zones
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Routes'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="routes form content">
            <?= $this->Form->create($route) ?>
            <fieldset>
                <legend><?= __('Add Route') ?></legend>
                <?php
                    echo $this->Form->control('zone_id', ['options' => $zones]);
                    echo $this->Form->control('name');
                    echo $this->Form->control('visible');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
