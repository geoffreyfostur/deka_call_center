<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Customer $customer
 * @var \Cake\Collection\CollectionInterface|string[] $zones
 * @var \Cake\Collection\CollectionInterface|string[] $routes
 * @var \Cake\Collection\CollectionInterface|string[] $users
 * @var \Cake\Collection\CollectionInterface|string[] $caravanes
 */
$status = array(0 => "Inactif", 1 => "Actif");
?>

<?= $this->Form->create($customer) ?>
<div class="row">
    <div class="col-md-4 col-lg-4">
        <?= $this->Form->control('name', array("placeholder" => "Représentant", "label" => "Représentant *", 'class' => "form-control focus_input")); ?>
    </div>
    <div class="col-md-4 col-lg-4">
        <?= $this->Form->control('company', array("placeholder" => "Companie", "label" => "Companie", 'class' => "form-control focus_input")); ?>
    </div>
    <div class="col-md-4 col-lg-4">
        <?= $this->Form->control('nif', array("placeholder" => "NIF", "label" => "NIF", 'class' => "form-control focus_input")); ?>
    </div>
    
</div>
<hr>
<div class="row">
    <div class="col-md-6 col-lg-6">
        <span data-toggle="modal"  data-target="#newzone"  style="font-size:17px;font-weight:bold;background:green;padding:0px 6px;border-radius:3px;color:white;float:right;margin-top:-5px;cursor:pointer">+</span>
        <?= $this->Form->control('zone_id', array("empty" => "-- Choisissez --", "label" => "Zone *", 'class' => "form-control", "options" => $zones)); ?>
    </div>
    <div class="col-md-6 col-lg-6">
        <?= $this->Form->control('status', array("empty" => "-- Choisissez --", "label" => "Statut *", 'class' => "form-control", "options" => $status)); ?>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-md-3 col-lg-3">
        <?= $this->Form->control('phone', array("placeholder" => "Téléphone", "label" => "Téléphone *", 'class' => "form-control")); ?>
    </div>
    <div class="col-md-3 col-lg-3">
        <?= $this->Form->control('second_phone', array("placeholder" => "Téléphone 2", "label" => "Téléphone 2", 'class' => "form-control")); ?>
    </div>
    <div class="col-md-3 col-lg-3">
        <?= $this->Form->control('address', array("placeholder" => "Adresse", "label" => "Adresse", 'class' => "form-control")); ?>
    </div>
    <div class="col-md-3 col-lg-3">
        <?= $this->Form->control('note', array("placeholder" => "Note", "label" => "Note", 'class' => "form-control")); ?>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-md-6"><?= $this->Form->control('lattitude', array('class' => 'form-control lattitude', "label" => "Lattitude *", "placeholder" => "Lattitude")); ?></div>
    <div class="col-md-6"><?= $this->Form->control('longitude', array('class' => 'form-control longitude', "label" => "Longitude *", "placeholder" => "Longitude")); ?></div>
</div> 
<hr>
<div class="row">
    <div class="col-md-12">
        <div id="map"></div>
    </div>
</div>  
<hr>
<div class="row">
    <div class="col-md-12">
        <a class="btn btn-warning float-left" href = '<?= ROOT_DIREC ?>/customers'><i class="ti-arrow-left"></i></a>
        <a class="btn btn-primary float-left ml-10" href = '<?= ROOT_DIREC ?>/customers/add'><i class="ti-plus"></i></a>
        <?= $this->Form->button('', array(
    "class" => "btn btn-success float-right ti-check", 
    'escape' => false
)); 
?>
    </div>
</div>  





<?= $this->Form->end() ?>
<style type="text/css">
    .page-body{
        padding-bottom: 14px;
    }
</style>
<script type="text/javascript">
    $(document).ready(function(){
        $(".focus_input").focus();
    })
</script>
<?php if(empty($customer->lattitude) || empty($customer->longitude)) : ?>
<?php  

    echo "<script>";
    echo "var lattitude = 18.514590042933047;";
    echo "var longitude = -72.28853747057839;";
    echo "</script>";
?>
<?php else : ?>
    <?php  

    echo "<script>";
    echo "var lattitude = ".$customer->lattitude.";";
    echo "var longitude = ".$customer->longitude.";";
    echo "</script>";
?>
<?php   endif; ?>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBx60vagKl63rQiL0k4peXeEnXG8GwSb9E&callback=initMap&libraries=&v=weekly" async></script>

<script type="text/javascript">
function initMap() {
  const myLatlng = { lat: lattitude, lng: longitude };
  const map = new google.maps.Map(document.getElementById("map"), {
    zoom: 16,
    center: myLatlng,
  });
  // Create the initial InfoWindow.
  let infoWindow = new google.maps.InfoWindow({
    content: "Location",
    position: myLatlng,
  });
  infoWindow.open(map);
  // Configure the click listener.
  map.addListener("click", (mapsMouseEvent) => {
    // Close the current InfoWindow.
    infoWindow.close();
    // Create a new InfoWindow.
    infoWindow = new google.maps.InfoWindow({
      position: mapsMouseEvent.latLng,
    });
    infoWindow.setContent(
      JSON.stringify(mapsMouseEvent.latLng.toJSON(), null, 2)
    );
    document.getElementById("lattitude").value = mapsMouseEvent.latLng.toJSON().lat;
    document.getElementById("longitude").value = mapsMouseEvent.latLng.toJSON().lng;
    console.log(mapsMouseEvent.latLng.toJSON());
    infoWindow.open(map);
  });
}
</script>
<style type="text/css">
    #map {
      height: 500px;
    }
</style>


<?php  

echo "<script>";
echo "var ROOT_DIRECTORY = '".ROOT_DIREC."'";
echo "</script>";

?>

