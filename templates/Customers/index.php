<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Customer[]|\Cake\Collection\CollectionInterface $customers
 */
?>
<div class="caravanes index content">
    <?= $this->Form->create(null, array("url" => '/customers/setfilters')) ?>
<div class="row">   
    <div class="col-md-3 col-lg-3">
        <?= $this->Form->control('caravane_id', array("empty" => "-- Filtrer par Caravane --", "label" => false, 'class' => "form-control", "options" => $caravanes, 'value' => $caravane)); ?>
    </div>
    <div class="col-md-1"> 
        <?= $this->Form->button('', array(
            "class" => "btn btn-success float-left ti-check", 
            'style' => "padding:9px 20px",
            'escape' => false
        )); ?>
    </div>  
</div> 

<?= $this->Form->end() ?> 
    <div class="table-responsive mt-10">
        <table class="table datatable table-striped table-hover">
            <thead>
                <tr>
                    <th>Client</th>
                    <th>Zone</th>
                    <th>Téléphone</th>
                    <th>Téléphone</th>
                    <th class="text-right">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($customers as $customer): ?>
                <tr>
                    <?php if(!empty($customer->company)) : ?>
                        <?php if(!empty($customer->name)) : ?>
                            <td><strong><?= h($customer->company) ?></strong> - <?= h($customer->name) ?></td>
                        <?php else : ?>
                            <td><strong><?= h($customer->name) ?></td>
                        <?php endif; ?>
                    <?php else : ?>
                        <td><?= h($customer->name) ?></td>
                    <?php endif; ?>
                    <td><a href="<?= ROOT_DIREC ?>/customers/index/<?= $customer->zone_id ?>"><?= $customer->zone->name ?></a></td>
                    <td><?= h($customer->phone) ?></td>
                    <td><?= h($customer->second_phone) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('<i class="ti-pencil"></i>'), ['action' => 'edit', $customer->id], ['class' => 'edit_link', 'escape' => false]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>