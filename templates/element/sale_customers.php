<div class="caravanes index content">
    <div class="table-responsive mt-10">
        <table class="table datatable_customers table-striped table-hover">
            <thead>
                <tr>
                    <th>Client</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($customers as $customer): ?>
                    <?php if(!empty($selected) && $selected == $customer->id) : ?>
                    <tr style="background:#E0E8FF">
                    <?php else : ?>
                        <tr>
                    <?php endif; ?>
                    <?php if(!empty($customer->company)) : ?>
                        <?php if(!empty($customer->name)) : ?>
                            <td><strong><?= h($customer->company) ?></strong> - <?= h($customer->name) ?></td>
                        <?php else : ?>
                            <td><strong><?= h($customer->name) ?></td>
                        <?php endif; ?>
                    <?php else : ?>
                        <td><?= h($customer->name) ?></td>
                    <?php endif; ?>
                    <td><?= $customer->zone->name ?></td>
                    <td><?= h($customer->phone) ?></td>
                    <td><?= h($customer->second_phone) ?></td>
                    <td><a href="<?= ROOT_DIREC ?>/sales/setcustomer/<?= $customer->id ?>"><button class="btn btn-round btn-primary">Continuer</button></a></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>