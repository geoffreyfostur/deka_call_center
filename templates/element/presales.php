<div class="caravanes index content">
    <div class="table-responsive mt-10">
        <table class="table datatable">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Type</th>
                    <th>Client</th>
                    <th>Caravane</th>
                    <th>Zone</th>
                    <th>Total Commandé</th>
                    <th>Date</th>
                    <th>Heure</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($sales as $sale): ?>
                <tr>
                    <td data-toggle="tooltip" data-placement="top" data-trigger="hover" title="" data-original-title="<?= $sale->note ?>"><a href="<?= ROOT_DIREC ?>/sales/view/<?= $sale->id ?>"><?= h($sale->sale_number) ?></a></td>
                    <?php if($sale->type == 1) : ?>
                        <td><span class="label label-primary">PROFORMA</span></td>
                    <?php elseif($sale->type == 2) : ?>
                        <td><span class="label label-success">VENTE</span></td>
                    <?php else : ?>
                        <td><span class="label label-warning">VENTE DIRECTE</span></td>
                    <?php endif; ?>
                    <td><?= h($sale->customer->name) ?></td>
                    <td><?= h($sale->caravane->identification) ?></td>
                    <td><?= h($sale->zone->name) ?></td>
                    <td><?= number_format($sale->total_ordered, 2, ".", ",") ?> USD</td>
                    <td><?= date("Y-m-d", strtotime($sale->created)) ?></td>
                    <td><?= date("H:i", strtotime($sale->created)) ?></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>