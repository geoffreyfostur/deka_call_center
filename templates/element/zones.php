<div class="zones index content">
    <div class="table-responsive mt-10">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Nom</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($zones as $zone): ?>
                	<?php if(!empty($selected) && $selected == $zone['id']) : ?>
                	<tr style="background:#E0E8FF">
                	<?php else : ?>
                		<tr>
                	<?php endif; ?>
                    <td><?= $zone['name'] ?></td>
                    <td><a href="<?= ROOT_DIREC ?>/calls/setzone/<?= $zone['id'] ?>"><button class="btn btn-round btn-primary">Continuer</button></a></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>