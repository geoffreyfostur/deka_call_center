<div class="caravanes index content">
    <div class="table-responsive mt-10">
        <table class="table table-hover">
            <thead>
                <tr>
                	<th>#</th>
                    <th>Nom</th>
                    <th>Vendeur</th>
                    <th>Téléphone</th>
                    <th>Immatriculation</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($caravanes as $caravane): ?>
                	<?php if(!empty($selected) && $selected == $caravane->id) : ?>
                	<tr style="background:#E0E8FF">
                	<?php else : ?>
                		<tr>
                	<?php endif; ?>
                    <td><?= h($caravane->identification) ?></td>
                    <td><a><?= h($caravane->name) ?></a></td>
                    <td><?= h($caravane->seller_name) ?></td>
                    <td><?= h($caravane->seller_phone) ?></td>
                    <td><?= h($caravane->registration) ?></td>
                    <td><a href="<?= ROOT_DIREC ?>/calls/setcaravane/<?= $caravane->id ?>"><button class="btn btn-round btn-primary">Continuer</button></a></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>