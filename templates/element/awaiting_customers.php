<div class="caravanes index content">
    <div class="table-responsive mt-10">
        <table class="table datatable table-striped table-hover">
            <thead>
                <tr>
                    <th>Client</th>
                    <th>Zone</th>
                    <th>Téléphone</th>
                    <th>Téléphone</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($customers as $customer): ?>
                <tr>
                    <?php  
                    if(count($customer->calls) > 0){
                        $called = true;
                    }else{
                        $called = false;
                    }
                    ?>
                    <?php if(!empty($customer->company)) : ?>
                        <?php if(!empty($customer->name)) : ?>
                            <td <?= ($called) ? "style='color:red'" : '' ?>><strong><?= h($customer->company) ?></strong> - <?= h($customer->name) ?></td>
                        <?php else : ?>
                            <td <?= ($called) ? "style='color:red'" : '' ?>><strong><?= h($customer->name) ?></td>
                        <?php endif; ?>
                    <?php else : ?>
                        <td <?= ($called) ? "style='color:red'" : '' ?>><?= h($customer->name) ?></td>
                    <?php endif; ?>
                    <td><?= $customer->zone->name ?></td>
                    <td><?= h($customer->phone) ?></td>
                    <td id="actions"><?= h($customer->second_phone) ?></td>
                    <?php if(empty($included[$customer->id])) : ?>
                        <td><input type="hidden" class="customer_id" value="<?= $customer->id ?>"><button class="btn btn-success add" style="padding: 5px 3px 3px 6px"><i class="ti ti-plus"></i></button></td>
                    <?php else : ?>
                        <td><input type="hidden" class="customer_id " value="<?= $customer->id ?>"><button class="btn btn-danger remove"  style="padding: 5px 3px 3px 6px"><i class="ti ti-minus"></i></button></a></td>
                    <?php endif; ?>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(){

    $(document.body).on("click", ".add", function(){
        var customer_id = $(this).parent().find('.customer_id').val();
        var token =  $('input[name="_csrfToken"]').val();
        var element = $(this);
        $.ajax({
            url : ROOT_DIRECTORY+'/caravanes/addcustomer/',
            type : 'POST',
            dataType : 'json',
            data : {customer_id : customer_id, type : 1},
            headers : {
                'X-CSRF-Token': token 
            },
            success : function(data, statut){
                element.removeClass('btn-success');
                element.removeClass('add');
                element.addClass('btn-danger');
                element.addClass('remove');
                element.empty();
                element.append('<i class="ti ti-minus"></i>');

            },
            error : function(resultat, statut, erreur){
                console.log(erreur);
            }, 
            complete : function(resultat, statut){
                console.log(resultat);
            }
        });
    });


    $(document.body).on("click", ".remove", function(){
        var customer_id = $(this).parent().find('.customer_id').val();
        var token =  $('input[name="_csrfToken"]').val();
        var element = $(this);
        $.ajax({
            url : ROOT_DIRECTORY+'/caravanes/removecustomer/',
            type : 'POST',
            dataType : 'json',
            data : {customer_id : customer_id, type : 1},
            headers : {
                'X-CSRF-Token': token 
            },
            success : function(data, statut){
                console.log(data);
                element.removeClass('btn-danger');
                element.removeClass('remove');
                element.addClass('btn-success');
                element.addClass('add');
                element.empty();
                element.append('<i class="ti ti-plus"></i>');
            },
            error : function(resultat, statut, erreur){
                console.log(erreur);
            }, 
            complete : function(resultat, statut){
                console.log(resultat);
            }
        });
    });

    // $('button.remove').click(function(){
    //     var customer_id = $(this).parent().find('.customer_id').val();
    //     var token =  $('input[name="_csrfToken"]').val();
    //     var element = $(this);
    //     $.ajax({
    //         url : ROOT_DIRECTORY+'/caravanes/removecustomer/',
    //         type : 'POST',
    //         dataType : 'json',
    //         data : {customer_id : customer_id, type : 1},
    //         headers : {
    //             'X-CSRF-Token': token 
    //         },
    //         success : function(data, statut){
    //             console.log(data);
    //             element.removeClass('btn-danger');
    //             element.removeClass('remove');
    //             element.addClass('btn-success');
    //             element.addClass('add');
    //             element.empty();
    //             element.append('<i class="ti ti-plus"></i>');
    //         },
    //         error : function(resultat, statut, erreur){
    //             console.log(erreur);
    //         }, 
    //         complete : function(resultat, statut){
    //             console.log(resultat);
    //         }
    //     });
    // })

    // $('button.add').click(function(){
    //     var customer_id = $(this).parent().find('.customer_id').val();
    //     var token =  $('input[name="_csrfToken"]').val();
    //     var element = $(this);
    //     $.ajax({
    //         url : ROOT_DIRECTORY+'/caravanes/addcustomer/',
    //         type : 'POST',
    //         dataType : 'json',
    //         data : {customer_id : customer_id, type : 1},
    //         headers : {
    //             'X-CSRF-Token': token 
    //         },
    //         success : function(data, statut){
    //             element.removeClass('btn-success');
    //             element.removeClass('add');
    //             element.addClass('btn-danger');
    //             element.addClass('remove');
    //             element.empty();
    //             element.append('<i class="ti ti-minus"></i>');

    //         },
    //         error : function(resultat, statut, erreur){
    //             console.log(erreur);
    //         }, 
    //         complete : function(resultat, statut){
    //             console.log(resultat);
    //         }
    //     });
    // })
})
</script>