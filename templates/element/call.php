<?= $this->Form->create(null, array("url" => "/calls/save")) ?>
<div class="row">
    <div class="col-md-4 col-lg-4">
        <?= $this->Form->control('answered', array("empty" => "--Choisissez --", "label" => "Le client à t'il répondu à l'appel ?", "options" => array(1 => 'OUI', 0 => 'NON'), 'class' => "form-control", 'required' => true)); ?>
    </div>
    <div class="col-md-8 col-lg-8">
        <?= $this->Form->control('note', array("placeholder" => "Ajouter un commentaire pour cet appel", "label" => "Commentaire", 'class' => "form-control")); ?>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-md-9 col-lg-9">
        <?= $this->Form->control('customer_note', array("placeholder" => "Ajouter un commentaire pour ce client", "label" => "Commentaire Client", 'class' => "form-control")); ?>
    </div>
    <div class="col-md-3 col-lg-3">
        <?= $this->Form->control('proforma', array("empty" => "--Choisissez --", "label" => "Créer un proforma ?", "options" => array(1 => 'OUI', 0 => 'NON'), 'class' => "form-control", 'required' => true)); ?>
    </div>
</div>
<hr>
<?= $this->Form->button('', array(
    "class" => "btn btn-success float-right ti-check", 
    'escape' => false
)); ?>
<?= $this->Form->end() ?>