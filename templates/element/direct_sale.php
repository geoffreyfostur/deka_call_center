<?php  
$types = array(0 => "Dégustation", 1 => "Baisse de Prix", 2 => "Promotion Croisée", 3 => "Article Promotionel");
$total = 0;
if(!empty($sale['products'])){
    foreach($sale['products'] as $id => $product){
        $total = $total + $product['quantity']*$product['price'];
    }
}

if(!empty($sale['articles'])){
    foreach($sale['articles'] as $id => $article){
        $total = $total + $article['price'];
    }
}

?>
<div class="sales form content">
    <div class="row">
        <div class="col-md-12 col-xl-12">
            <div class="row">
                <div class="col-md-6">
                    <?= $this->Form->control('category_id', array("empty" => "-- Filtrer par Catégorie --", "label" => false, "options" => $categories_list, 'class' => "form-control category_filter")); ?>
                </div>
                <div class="col-md-6">
                    <label class="btn btn-success" data-toggle="modal" data-target="#submitModal" style="float:right;cursor:pointer;margin-left:7px;margin-right:7px">Valider | <strong>$<span id="sale_total"><?= number_format($total, "2", ",", ",") ?></span></strong></label>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" style="float:right">
                      Détails
                    </button>
                    <a class="btn btn-warning" style="float:right;margin-right:7px" href="<?= ROOT_DIREC ?>/sales/initializeDS">Rafraichir</a>
                </div>
            </div>
            <hr>
            <div class="table-responsive">
                <table class="table datatable_show_all table-striped table-hover" style="width:100%;margin-right:7px">
                    <thead>
                        <tr>
                            <th>Produit</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody class="products_table" style="overflow-y:scroll;height:400px!important">
                        <?php foreach ($categories as $category): ?>
                            <?php foreach($category->products as $product) : ?>
                                <?php if(!empty($sale['products'][$product->id])) : ?>
                                    <tr class="<?= $category->id ?>" style="background:#d9edf7">
                                <?php else : ?>
                                    <tr class="<?= $category->id ?>">
                                <?php endif; ?>
                                
                                    <?php 
                                        $price = $product->price;
                                        $is_promo = false;
                                        if(!empty($promotions[$product->id])){
                                            $promo = $promotions[$product->id];
                                            if(empty($promo['zone_id']) || $promo['zone_id'] == $call['customer_info']['zone_id']){
                                                $is_promo = true;
                                                $promotion_label = "";
                                                $promotion_label .= $types[$promo['type']]." : ";
                                                if($promo['type'] == 0 || $promo['type'] == 1){
                                                    $promotion_label .= $promo['promotion_price'];
                                                    $price = $promo['promotion_price'];
                                                }else{
                                                    $promotion_label .= "Achetez ".$promo['start_quantity']. " ";
                                                    $promotion_label .= "obtenez ".$promo['end_quantity']." ";
                                                    $promotion_label .= $promo['article']->name;
                                                    if($promo['article']->price != 0){
                                                        $promotion_label .= " à ".number_format($promo['article']->price*$promo['end_quantity'], 2, ".", ","). "USD";
                                                    }
                                                }
                                            }
                                            
                                        }
                                    ?>
                                    <?php foreach($product->products_centers as $key => $pc) : ?>
                                        <?php 
                                            if($pc->center_id == $center_id){
                                                $price = $pc->price;
                                            }
                                        ?>
                                    <?php endforeach; ?>
                                    <?php if($is_promo) : ?>
                                    <td>
                                        <a href="#!" data-toggle="tooltip" data-placement="top" data-trigger="hover" title="" data-original-title="<?= $promotion_label ?>"><i class="ti-medall" style="background:#4680ff;color:white;padding:3px"></i></a>
                                         <?= $product->name ?></td>
                                    <?php else : ?>
                                        <td><?= $product->name ?></td>
                                    <?php endif; ?>
                                    <td><?= number_format($price, 2, ".", ",") ?> USD</td>
                                    <td>
                                        <?php if(!empty($sale['products'][$product->id])) : ?>
                                            <input type="text" name="quantity" class="form-control quantity_edit" style="width:30px;height:15px;text-align:center;float:right;" placeholder="0" value = '<?= $sale['products'][$product->id]['quantity'] ?>'> 
                                        <?php else : ?>
                                            <input type="text" name="quantity" class="form-control quantity_edit" style="width:30px;height:15px;text-align:center;float:right;" placeholder="0"> 
                                        <?php endif; ?>
                                        <input type="hidden" name="product_id" class="product_id" value="<?= $product->id ?>"> <input type="hidden" name="product_name" class="product_name" value="<?= $product->name ?>"> <input type="hidden" name="price" class="price" value="<?= $price ?>"></td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Détails Fiche</h5>
      </div>
      <div class="modal-body">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Produit</th>
                    <th>Prix</th>
                    <th class="text-right">Total</th>
                </tr>
            </thead>
            <tbody id="sale_details">
                <?php if(!empty($sale['products'])) : ?>
                <?php $total = 0; foreach($sale['products'] as $id => $product) : ?>
                <?php $total = $total + $product['quantity'] *$product['price']; ?>
                <tr style="vertical-align:middle">
                    <td style="vertical-align:middle"><span style="cursor: pointer;padding:0px 8px;font-weight:bold;font-size:20px;float:left;margin-right:5px;background:red;color:white" class="delete_product" id = "<?= $product['product_id'] ?>"> - </span> <?= $product['name'] ?> <strong>(X<?= $product['quantity'] ?>)</strong></td>
                    <td  style="vertical-align:middle"><?= $product['price'] ?> USD</td>
                    <td style="vertical-align:middle" class="text-right"><?= number_format($product['quantity'] *$product['price'], 2, ".", "," ) ?> USD</td>
                </tr>
                <?php endforeach; ?>
            <?php endif; ?>
            </tbody>
            <tfoot>
                <tr>
                    <th colspan="2">TOTAL</th>
                    <th class="text-right"><span id = "sale_details_total"><?= number_format($total, 2, ".", ",") ?></span> USD</th>
                </tr>
            </tfoot>
        </table>
        <hr>
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Articles Promotionnels</th>
                    <th>Prix</th>
                    <th class="text-right">Total</th>
                </tr>
            </thead>
            <tbody id="articles_details">
                <?php $atotal = 0; if(!empty($sale['articles'])) : ?>
                <?php  foreach($sale['articles'] as $id => $article) : ?>
                <?php $atotal = $atotal + $article['price']; ?>
                <tr style="vertical-align:middle">
                    <td style="vertical-align:middle"><span style="cursor: pointer;padding:0px 8px;font-weight:bold;font-size:20px;float:left;margin-right:5px;background:red;color:white" class="delete_article" id = "article_<?= $id ?>"> - </span> <?= $article['name'] ?> <strong>(X<?= $article['quantity'] ?>)</strong></td>
                    <td  style="vertical-align:middle"><?= $article['price'] ?> USD</td>
                    <td style="vertical-align:middle" class="text-right"><?= number_format($article['price'], 2, ".", "," ) ?> USD</td>
                </tr>
                <?php endforeach; ?>
            <?php endif; ?>
            </tbody>
            <tfoot>
                <tr>
                    <th colspan="2">TOTAL</th>
                    <th class="text-right"><span id = "articles_details_total"><?= number_format($atotal, 2, ".", ",") ?></span> USD</th>
                </tr>
            </tfoot>
        </table>
        <hr>
        <span class="label label-success" style="font-size:20px;float:right">Total : <span id="grand_total"><?= number_format($atotal+$total, 2, ".", ",") ?></span> USD</span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="submitModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Validation Proforma</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?= $this->Form->create(null, array("url" => "/sales/savedirect")) ?>
      <div class="modal-body">
        <div class="row">
                <div class="col-md-12">
                    <?= $this->Form->control('created', array("placeholder" => "Date de Création", "label" => "Date de Création *", "type" => "date", 'class' => "form-control", "value" => date("Y-m-d"))); ?>
                </div>
            </div>
            <hr>
        <div class="row">
                <div class="col-md-12">
                    <?= $this->Form->control('delivery_date', array("placeholder" => "Date de Livraison", "label" => "Date de Livraison *", "type" => "date", 'class' => "form-control", "value" => date("Y-m-d", strtotime('+1 day')))); ?>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-12">
                    <?= $this->Form->control('note', array("placeholder" => "Ajouter une note", "label" => "Note *", "type" => "text", 'class' => "form-control")); ?>
                </div>
            </div>
            
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
        <button type="submit" class="btn btn-success">Valider</button>
      </div>
      <?= $this->Form->end() ?>
    </div>
  </div>
</div>


<script type="text/javascript">
    $(".category_filter").change(function(){
        var to_hide = $(this).val();
        $(".products_table").find("tr").show();
        $(".products_table tr."+to_hide).show()
        $(".products_table tr").not("."+to_hide).hide();
    })

    $(".delete_article").click(function(){
        element = $(this);
        var article_id = $(this).attr("id").split("_")[1]; 
        alert(article_id)
        var token =  $('input[name="_csrfToken"]').val();
        $.ajax({
            url : ROOT_DIRECTORY+'/sales/deletearticle',
            type : 'POST',
            dataType: 'json',
            data : {article_id: article_id},
            headers : {
                'X-CSRF-Token': token 
            },
            success : function(data, statut){
                console.log(data);
                update_details(data);
            },
            error : function(resultat, statut, erreur){
                console.log(erreur);
            }, 
            complete : function(resultat, statut){
                console.log(resultat);
            }
        });
    })

    $(".quantity_edit").change(function(){
        element = $(this);
        var quantity = $(this).val();
        var product_id = $(this).parent().find(".product_id").val();
        var price = $(this).parent().find(".price").val();
        var product_name = $(this).parent().find(".product_name").val();
        var token =  $('input[name="_csrfToken"]').val();
        if(isNaN(parseInt(quantity))){
            $(this).val("");
            $.ajax({
                url : ROOT_DIRECTORY+'/sales/addproduct',
                type : 'POST',
                dataType: 'json',
                data : {price : price, product_id : product_id, quantity: 0, product_name: product_name},
                headers : {
                    'X-CSRF-Token': token 
                },
                success : function(data, statut){
                    console.log(data);
                    element.parent().parent().css("background", "")
                    update_details(data);
                },
                error : function(resultat, statut, erreur){
                    console.log(erreur);
                }, 
                complete : function(resultat, statut){
                    console.log(resultat);
                }
            });
        }else{
            $.ajax({
                url : ROOT_DIRECTORY+'/sales/addproduct',
                type : 'POST',
                dataType: 'json',
                data : {price : price, product_id : product_id, quantity: quantity, product_name: product_name},
                headers : {
                    'X-CSRF-Token': token 
                },
                success : function(data, statut){
                    console.log(data);
                    $("#sale_total").text(parseFloat(data.total).toFixed(2))
                    element.parent().parent().css("background", "#d9edf7");
                    update_details(data);
                },
                error : function(resultat, statut, erreur){
                    console.log(erreur);
                }, 
                complete : function(resultat, statut){
                    console.log(resultat);
                }
            });
        }
    })

    function update_details(sale){
        var element = $("#sale_details");
        var articles_element = $("#articles_details")
        var products = sale.products;
        var articles = sale.articles
        element.empty();
        articles_element.empty();
        Object.values(products).forEach(product => {
            var total = parseFloat(product.price)*parseFloat(product.quantity);
            var row = '<tr><td><span style="cursor: pointer;padding:0px 8px;font-weight:bold;font-size:20px;float:left;margin-right:5px;background:red;color:white" class="delete_product" id = "'+product.product_id+'"> - </span>'+product.name+' <strong>(X'+product.quantity+')</strong></td><td>'+product.price+' USD</td><td>'+total.toFixed(2)+' USD</td></tr>';
            element.append(row);
        });
        if(articles != undefined){
           Object.values(articles).forEach(article => {
            var row = '<tr><td><span style="cursor: pointer;padding:0px 8px;font-weight:bold;font-size:20px;float:left;margin-right:5px;background:red;color:white" class="delete_article" id = "'+article.article_id+'"> - </span>'+article.name+' <strong>(X'+article.quantity+')</strong></td><td>'+article.price+' USD</td><td>'+article.price.toFixed(2)+' USD</td></tr>';
            articles_element.append(row);
        }); 
        }
        

        $("#sale_details_total").text(parseFloat(sale.products_total).toFixed(2));
        $("#grand_total").text(parseFloat(sale.total).toFixed(2));
        $("#sale_total").text(parseFloat(sale.total).toFixed(2));
        $("#articles_details_total").text(parseFloat(sale.articles_total).toFixed(2));

        $(".delete_product").click(function(){
            var product_id = $(this).attr("id"); 
            delete_product(product_id);
        })
    }

    function delete_product(product_id){
        var token =  $('input[name="_csrfToken"]').val();
        $.ajax({
            url : ROOT_DIRECTORY+'/sales/addproduct',
            type : 'POST',
            dataType: 'json',
            data : {price : 0, product_id : product_id, quantity: 0, product_name: 'product_name'},
            headers : {
                'X-CSRF-Token': token 
            },
            success : function(data, statut){
                console.log(data);
                update_details(data);
            },
            error : function(resultat, statut, erreur){
                console.log(erreur);
            }, 
            complete : function(resultat, statut){
                console.log(resultat);
            }
        });
    }

    $(".delete_product").click(function(){
        var product_id = $(this).attr("id"); 
        delete_product(product_id);
    })
</script>
