<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ZonesCaravane $zonesCaravane
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Zones Caravane'), ['action' => 'edit', $zonesCaravane->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Zones Caravane'), ['action' => 'delete', $zonesCaravane->id], ['confirm' => __('Are you sure you want to delete # {0}?', $zonesCaravane->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Zones Caravanes'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Zones Caravane'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="zonesCaravanes view content">
            <h3><?= h($zonesCaravane->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Zone') ?></th>
                    <td><?= $zonesCaravane->has('zone') ? $this->Html->link($zonesCaravane->zone->name, ['controller' => 'Zones', 'action' => 'view', $zonesCaravane->zone->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Caravane') ?></th>
                    <td><?= $zonesCaravane->has('caravane') ? $this->Html->link($zonesCaravane->caravane->name, ['controller' => 'Caravanes', 'action' => 'view', $zonesCaravane->caravane->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($zonesCaravane->id) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
