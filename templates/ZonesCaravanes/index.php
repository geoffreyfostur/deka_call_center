<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ZonesCaravane[]|\Cake\Collection\CollectionInterface $zonesCaravanes
 */
?>
<div class="zonesCaravanes index content">
    <?= $this->Html->link(__('New Zones Caravane'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Zones Caravanes') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('zone_id') ?></th>
                    <th><?= $this->Paginator->sort('caravane_id') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($zonesCaravanes as $zonesCaravane): ?>
                <tr>
                    <td><?= $this->Number->format($zonesCaravane->id) ?></td>
                    <td><?= $zonesCaravane->has('zone') ? $this->Html->link($zonesCaravane->zone->name, ['controller' => 'Zones', 'action' => 'view', $zonesCaravane->zone->id]) : '' ?></td>
                    <td><?= $zonesCaravane->has('caravane') ? $this->Html->link($zonesCaravane->caravane->name, ['controller' => 'Caravanes', 'action' => 'view', $zonesCaravane->caravane->id]) : '' ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $zonesCaravane->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $zonesCaravane->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $zonesCaravane->id], ['confirm' => __('Are you sure you want to delete # {0}?', $zonesCaravane->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
