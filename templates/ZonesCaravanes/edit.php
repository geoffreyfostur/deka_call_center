<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ZonesCaravane $zonesCaravane
 * @var string[]|\Cake\Collection\CollectionInterface $zones
 * @var string[]|\Cake\Collection\CollectionInterface $caravanes
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $zonesCaravane->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $zonesCaravane->id), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('List Zones Caravanes'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="zonesCaravanes form content">
            <?= $this->Form->create($zonesCaravane) ?>
            <fieldset>
                <legend><?= __('Edit Zones Caravane') ?></legend>
                <?php
                    echo $this->Form->control('zone_id', ['options' => $zones]);
                    echo $this->Form->control('caravane_id', ['options' => $caravanes]);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
