<div class="row">
    <div class="col-md-12">
            <?= $this->Form->create() ?>
                <div class="row">   
                    <div class="col-md-3 col-lg-3">
                        <?= $this->Form->control('caravane_id', array("empty" => "-- Choisissez une Caravane --", "label" => 'Caravane', 'class' => "form-control", "options" => $caravanes, 'value' => $caravane_id)); ?>
                    </div>
                    <div class="col-md-3">
                        <?= $this->Form->control('delivery_date', array("placeholder" => "Date de Livraison", "label" => "Date de Livraison *", "type" => "date", 'class' => "form-control", "value" => $delivery_date)); ?>
                    </div>
                    <div class="col-md-1"> 
                        <?= $this->Form->button('', array(
                            "class" => "btn btn-success float-left ti-check", 
                            'style' => "padding:9px 20px;margin-top:27px",
                            'escape' => false
                        )); ?>
                    </div>  
                    <?php if(!empty($caravane_id)) : ?>
                    <div class="col-md-5">
                        <a href="<?= ROOT_DIREC ?>/caravanes/export" class="btn btn-warning" target = "_blank" style="float:right;color:white;margin-right:10px;margin-top:27px"><i class="ti ti-download"></i></a>
                    </div>
                <?php endif; ?>
                </div> 

            <?= $this->Form->end() ?>
        <!-- <a class = "btn btn-primary" href="<?= ROOT_DIREC ?>/caravanes/refresh" style="float:right"><i class="ti ti-reload"></i></a> -->
    </div>
</div>
<hr>
<div class="row">
    <div class="col-lg-12 col-xl-12">
        <?php if(!empty($caravane_id)) : ?>
        <ul class="nav nav-tabs md-tabs " role="tablist">
            <li class="nav-item">
                <a class="nav-link <?= ($type == 0) ? 'active' : '' ?>" data-toggle="tab" href="#home7" role="tab"><i class="ti ti-truck"></i> Proformas</a>
                <div class="slide"></div>
            </li>
            <li class="nav-item">
                <a class="nav-link <?= ($type == 1) ? 'active' : '' ?>" data-toggle="tab" href="#profile7" role="tab"><i class="ti ti-user"></i> Clients en Attente</a>
                <div class="slide"></div>
            </li>
            
            <li class="nav-item">
                <a class="nav-link <?= ($type == 2) ? 'active' : '' ?>" data-toggle="tab" href="#messages7" role="tab"><i class="ti ti-user"></i> Clients à convaincre</a>
                <div class="slide"></div>
            </li>
        </ul>
        <div class="tab-content card-block">
            <div class="tab-pane  <?= ($type == 0) ? 'active' : '' ?>" id="home7" role="tabpanel">
                <?php echo $this->element('presales', array('sales' => $sales)); ?>
            </div>

            <div class="tab-pane  <?= ($type == 1) ? 'active' : '' ?>" id="profile7" role="tabpanel">
                <?php echo $this->element('awaiting_customers', array('customers' => $customers, 'included' => $routing['awaiting_customers'])); ?>
            </div>
            
            <div class="tab-pane  <?= ($type == 2) ? 'active' : '' ?>" id="messages7" role="tabpanel">
                <?php echo $this->element('customers_to_convince', array('customers' => $customers, 'included' => $routing['customers_to_convince'])); ?>
            </div>
        </div>
        <?php endif; ?>
    </div>
</div>