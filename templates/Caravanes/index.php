<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Caravane[]|\Cake\Collection\CollectionInterface $caravanes
 */
?>
<div class="caravanes index content">
    <div class="table-responsive mt-10">
        <table class="table datatable table-striped table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nom</th>
                    <th>Vendeur</th>
                    <th>Téléphone</th>
                    
                    <th>Immatriculation</th>
                    <th>Statut</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($caravanes as $caravane): ?>
                <tr>
                    <td><?= h($caravane->identification) ?></td>
                    <td><a href="<?= ROOT_DIREC ?>/customers/filterbyurl/false/<?= $caravane->id ?>"><?= h($caravane->name) ?></a></td>
                    <td><?= h($caravane->seller_name) ?></td>
                    <td><?= h($caravane->seller_phone) ?></td>
                    
                    <td><?= h($caravane->registration) ?></td>
                    <?php if($caravane->status == 1) : ?>
                        <td><span class="label label-success">Actif</span></td>
                    <?php else : ?>
                        <td><span class="label label-danger">Inactif</span></td>
                    <?php endif; ?>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>


<?php foreach ($caravanes as $caravane): ?>
<div class="modal fade" id="view_zones_<?= $caravane->id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Zones : <?= $caravane->name ?></h5>
      </div>
      <div class="modal-body text-center">
        <?php foreach($caravane->zones as $zone) : ?>
            <a href="<?= ROOT_DIREC ?>/customers/filterbyurl/<?= $zone->id ?>/<?= $caravane->id ?>"><span class="label label-primary mt-10"><?= $zone->name ?></span></a><br><br>
        <?php endforeach; ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<?php endforeach; ?>
