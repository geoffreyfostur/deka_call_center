<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArticlesSale $articlesSale
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Articles Sale'), ['action' => 'edit', $articlesSale->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Articles Sale'), ['action' => 'delete', $articlesSale->id], ['confirm' => __('Are you sure you want to delete # {0}?', $articlesSale->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Articles Sales'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Articles Sale'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="articlesSales view content">
            <h3><?= h($articlesSale->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Products Sale') ?></th>
                    <td><?= $articlesSale->has('products_sale') ? $this->Html->link($articlesSale->products_sale->id, ['controller' => 'ProductsSales', 'action' => 'view', $articlesSale->products_sale->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Sale') ?></th>
                    <td><?= $articlesSale->has('sale') ? $this->Html->link($articlesSale->sale->id, ['controller' => 'Sales', 'action' => 'view', $articlesSale->sale->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Article') ?></th>
                    <td><?= $articlesSale->has('article') ? $this->Html->link($articlesSale->article->name, ['controller' => 'Articles', 'action' => 'view', $articlesSale->article->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Promotion') ?></th>
                    <td><?= $articlesSale->has('promotion') ? $this->Html->link($articlesSale->promotion->id, ['controller' => 'Promotions', 'action' => 'view', $articlesSale->promotion->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($articlesSale->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Price') ?></th>
                    <td><?= $this->Number->format($articlesSale->price) ?></td>
                </tr>
                <tr>
                    <th><?= __('Quantity') ?></th>
                    <td><?= $this->Number->format($articlesSale->quantity) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created') ?></th>
                    <td><?= $this->Number->format($articlesSale->created) ?></td>
                </tr>
                <tr>
                    <th><?= __('Modified') ?></th>
                    <td><?= $this->Number->format($articlesSale->modified) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
