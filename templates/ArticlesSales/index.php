<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArticlesSale[]|\Cake\Collection\CollectionInterface $articlesSales
 */
?>
<div class="articlesSales index content">
    <?= $this->Html->link(__('New Articles Sale'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Articles Sales') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('products_sale_id') ?></th>
                    <th><?= $this->Paginator->sort('sale_id') ?></th>
                    <th><?= $this->Paginator->sort('article_id') ?></th>
                    <th><?= $this->Paginator->sort('promotion_id') ?></th>
                    <th><?= $this->Paginator->sort('price') ?></th>
                    <th><?= $this->Paginator->sort('quantity') ?></th>
                    <th><?= $this->Paginator->sort('created') ?></th>
                    <th><?= $this->Paginator->sort('modified') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($articlesSales as $articlesSale): ?>
                <tr>
                    <td><?= $this->Number->format($articlesSale->id) ?></td>
                    <td><?= $articlesSale->has('products_sale') ? $this->Html->link($articlesSale->products_sale->id, ['controller' => 'ProductsSales', 'action' => 'view', $articlesSale->products_sale->id]) : '' ?></td>
                    <td><?= $articlesSale->has('sale') ? $this->Html->link($articlesSale->sale->id, ['controller' => 'Sales', 'action' => 'view', $articlesSale->sale->id]) : '' ?></td>
                    <td><?= $articlesSale->has('article') ? $this->Html->link($articlesSale->article->name, ['controller' => 'Articles', 'action' => 'view', $articlesSale->article->id]) : '' ?></td>
                    <td><?= $articlesSale->has('promotion') ? $this->Html->link($articlesSale->promotion->id, ['controller' => 'Promotions', 'action' => 'view', $articlesSale->promotion->id]) : '' ?></td>
                    <td><?= $this->Number->format($articlesSale->price) ?></td>
                    <td><?= $this->Number->format($articlesSale->quantity) ?></td>
                    <td><?= $this->Number->format($articlesSale->created) ?></td>
                    <td><?= $this->Number->format($articlesSale->modified) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $articlesSale->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $articlesSale->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $articlesSale->id], ['confirm' => __('Are you sure you want to delete # {0}?', $articlesSale->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
