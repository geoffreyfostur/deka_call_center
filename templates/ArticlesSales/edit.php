<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArticlesSale $articlesSale
 * @var string[]|\Cake\Collection\CollectionInterface $productsSales
 * @var string[]|\Cake\Collection\CollectionInterface $sales
 * @var string[]|\Cake\Collection\CollectionInterface $articles
 * @var string[]|\Cake\Collection\CollectionInterface $promotions
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $articlesSale->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $articlesSale->id), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('List Articles Sales'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="articlesSales form content">
            <?= $this->Form->create($articlesSale) ?>
            <fieldset>
                <legend><?= __('Edit Articles Sale') ?></legend>
                <?php
                    echo $this->Form->control('products_sale_id', ['options' => $productsSales]);
                    echo $this->Form->control('sale_id', ['options' => $sales]);
                    echo $this->Form->control('article_id', ['options' => $articles]);
                    echo $this->Form->control('promotion_id', ['options' => $promotions]);
                    echo $this->Form->control('price');
                    echo $this->Form->control('quantity');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
