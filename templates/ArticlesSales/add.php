<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArticlesSale $articlesSale
 * @var \Cake\Collection\CollectionInterface|string[] $productsSales
 * @var \Cake\Collection\CollectionInterface|string[] $sales
 * @var \Cake\Collection\CollectionInterface|string[] $articles
 * @var \Cake\Collection\CollectionInterface|string[] $promotions
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Articles Sales'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="articlesSales form content">
            <?= $this->Form->create($articlesSale) ?>
            <fieldset>
                <legend><?= __('Add Articles Sale') ?></legend>
                <?php
                    echo $this->Form->control('products_sale_id', ['options' => $productsSales]);
                    echo $this->Form->control('sale_id', ['options' => $sales]);
                    echo $this->Form->control('article_id', ['options' => $articles]);
                    echo $this->Form->control('promotion_id', ['options' => $promotions]);
                    echo $this->Form->control('price');
                    echo $this->Form->control('quantity');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
