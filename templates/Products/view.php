<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Product $product
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Product'), ['action' => 'edit', $product->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Product'), ['action' => 'delete', $product->id], ['confirm' => __('Are you sure you want to delete # {0}?', $product->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Products'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Product'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="products view content">
            <h3><?= h($product->name) ?></h3>
            <table>
                <tr>
                    <th><?= __('Name') ?></th>
                    <td><?= h($product->name) ?></td>
                </tr>
                <tr>
                    <th><?= __('Category') ?></th>
                    <td><?= $product->has('category') ? $this->Html->link($product->category->name, ['controller' => 'Categories', 'action' => 'view', $product->category->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Grouping') ?></th>
                    <td><?= $product->has('grouping') ? $this->Html->link($product->grouping->name, ['controller' => 'Groupings', 'action' => 'view', $product->grouping->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('User') ?></th>
                    <td><?= $product->has('user') ? $this->Html->link($product->user->name, ['controller' => 'Users', 'action' => 'view', $product->user->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($product->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Price') ?></th>
                    <td><?= $this->Number->format($product->price) ?></td>
                </tr>
                <tr>
                    <th><?= __('Promotion Price') ?></th>
                    <td><?= $this->Number->format($product->promotion_price) ?></td>
                </tr>
                <tr>
                    <th><?= __('Visible') ?></th>
                    <td><?= $this->Number->format($product->visible) ?></td>
                </tr>
                <tr>
                    <th><?= __('Status') ?></th>
                    <td><?= $this->Number->format($product->status) ?></td>
                </tr>
                <tr>
                    <th><?= __('Promotion Start') ?></th>
                    <td><?= h($product->promotion_start) ?></td>
                </tr>
                <tr>
                    <th><?= __('Promotion End') ?></th>
                    <td><?= h($product->promotion_end) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created') ?></th>
                    <td><?= h($product->created) ?></td>
                </tr>
                <tr>
                    <th><?= __('Modified') ?></th>
                    <td><?= h($product->modified) ?></td>
                </tr>
            </table>
            <div class="related">
                <h4><?= __('Related Centers') ?></h4>
                <?php if (!empty($product->centers)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('Name') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($product->centers as $centers) : ?>
                        <tr>
                            <td><?= h($centers->id) ?></td>
                            <td><?= h($centers->name) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Centers', 'action' => 'view', $centers->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Centers', 'action' => 'edit', $centers->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Centers', 'action' => 'delete', $centers->id], ['confirm' => __('Are you sure you want to delete # {0}?', $centers->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
            <div class="related">
                <h4><?= __('Related Sales') ?></h4>
                <?php if (!empty($product->sales)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('Sale Number') ?></th>
                            <th><?= __('Customer Id') ?></th>
                            <th><?= __('Caravane Id') ?></th>
                            <th><?= __('Zone Id') ?></th>
                            <th><?= __('Route Id') ?></th>
                            <th><?= __('Status') ?></th>
                            <th><?= __('Delivery Date') ?></th>
                            <th><?= __('Delivered') ?></th>
                            <th><?= __('Type') ?></th>
                            <th><?= __('Note') ?></th>
                            <th><?= __('Problem') ?></th>
                            <th><?= __('Total Ordered') ?></th>
                            <th><?= __('Total Sold') ?></th>
                            <th><?= __('Created') ?></th>
                            <th><?= __('Modified') ?></th>
                            <th><?= __('User Id') ?></th>
                            <th><?= __('Last Delivery Date') ?></th>
                            <th><?= __('Visible') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($product->sales as $sales) : ?>
                        <tr>
                            <td><?= h($sales->id) ?></td>
                            <td><?= h($sales->sale_number) ?></td>
                            <td><?= h($sales->customer_id) ?></td>
                            <td><?= h($sales->caravane_id) ?></td>
                            <td><?= h($sales->zone_id) ?></td>
                            <td><?= h($sales->route_id) ?></td>
                            <td><?= h($sales->status) ?></td>
                            <td><?= h($sales->delivery_date) ?></td>
                            <td><?= h($sales->delivered) ?></td>
                            <td><?= h($sales->type) ?></td>
                            <td><?= h($sales->note) ?></td>
                            <td><?= h($sales->problem) ?></td>
                            <td><?= h($sales->total_ordered) ?></td>
                            <td><?= h($sales->total_sold) ?></td>
                            <td><?= h($sales->created) ?></td>
                            <td><?= h($sales->modified) ?></td>
                            <td><?= h($sales->user_id) ?></td>
                            <td><?= h($sales->last_delivery_date) ?></td>
                            <td><?= h($sales->visible) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Sales', 'action' => 'view', $sales->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Sales', 'action' => 'edit', $sales->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Sales', 'action' => 'delete', $sales->id], ['confirm' => __('Are you sure you want to delete # {0}?', $sales->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
            <div class="related">
                <h4><?= __('Related Promotions') ?></h4>
                <?php if (!empty($product->promotions)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('Type') ?></th>
                            <th><?= __('Start Date') ?></th>
                            <th><?= __('End Date') ?></th>
                            <th><?= __('Product Id') ?></th>
                            <th><?= __('Promotion Price') ?></th>
                            <th><?= __('Target Product Id') ?></th>
                            <th><?= __('Start Quantity') ?></th>
                            <th><?= __('End Quantity') ?></th>
                            <th><?= __('User Id') ?></th>
                            <th><?= __('Created') ?></th>
                            <th><?= __('Modified') ?></th>
                            <th><?= __('Keep Active') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($product->promotions as $promotions) : ?>
                        <tr>
                            <td><?= h($promotions->id) ?></td>
                            <td><?= h($promotions->type) ?></td>
                            <td><?= h($promotions->start_date) ?></td>
                            <td><?= h($promotions->end_date) ?></td>
                            <td><?= h($promotions->product_id) ?></td>
                            <td><?= h($promotions->promotion_price) ?></td>
                            <td><?= h($promotions->target_product_id) ?></td>
                            <td><?= h($promotions->start_quantity) ?></td>
                            <td><?= h($promotions->end_quantity) ?></td>
                            <td><?= h($promotions->user_id) ?></td>
                            <td><?= h($promotions->created) ?></td>
                            <td><?= h($promotions->modified) ?></td>
                            <td><?= h($promotions->keep_active) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Promotions', 'action' => 'view', $promotions->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Promotions', 'action' => 'edit', $promotions->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Promotions', 'action' => 'delete', $promotions->id], ['confirm' => __('Are you sure you want to delete # {0}?', $promotions->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
