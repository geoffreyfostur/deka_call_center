<?= $this->Form->create() ?>
<div class="row">
    <div class="col-md-2"><?= $this->Form->control('caravane_id', array("empty" => "-- Caravane --", "label" => false, 'class' => "form-control", 'options' => $caravanes, 'value' => $caravane_id)); ?></div>
    <div class="col-md-2"><?= $this->Form->control('zone_id', array("empty" => "-- Zone --", "label" => false, 'class' => "form-control", 'options' => $zones, 'value' => $zone_id)); ?></div>
    <div class="col-md-1"><?= $this->Form->button('', array(
    "class" => "btn btn-success float-right ti-check", 
    'escape' => false,
    'style' => 'height:39px'
)); ?></div>
</div>
<?= $this->Form->end() ?>
<div class="zones index content">
    <div class="table-responsive mt-10">
        <table class="table datatable">
            <thead>
                <tr>
                    <th>Catégorie</th>
                    <th>Produit</th>
                    <th>Qté Commandée</th>
                    <th>Qté Livrée</th>
                    <th>Total Commandée</th>
                    <th>Total Livrée</th>
                </tr>
            </thead>
            <tbody>
                <?php $ordered = 0; $delivered = 0; $total_ordered = 0; $total_delivered = 0; foreach ($products as $product): ?>
                <?php 
                $ordered = $ordered + $product['quantity_ordered'];
                $delivered = $delivered + $product['quantity_delivered'];
                $total_ordered = $total_ordered + $product['total_ordered'];
                $total_delivered = $total_delivered + $product['total_delivered'];

                ?>
                <tr>
                    <td><?= $product['cat_name'] ?></td>
                    <td><?= $product['name'] ?></td>
                    <td><?= number_format($product['quantity_ordered'], 2, ".", ",")  ?></td>
                    <td><?= number_format($product['quantity_delivered'], 2, ".", ",")  ?></td>
                    <td><?= number_format($product['total_ordered'], 2, ".", ",") ?> USD</td>
                    <td><?= number_format($product['total_delivered'], 2, ".", ",") ?> USD</td>
                </tr>
                <?php endforeach; ?>
            </tbody>
            <tfoot>
                <tr>
                    <th>Total</th>
                    <th></th>
                    <th><?= number_format($ordered, 2, ".", ",")  ?></th>
                    <th><?= number_format($delivered, 2, ".", ",")  ?></th>
                    <th><?= number_format($total_ordered, 2, ".", ",") ?> USD</th>
                    <th><?= number_format($total_delivered, 2, ".", ",") ?> USD</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
