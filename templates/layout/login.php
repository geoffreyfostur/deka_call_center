<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 * @var \App\View\AppView $this
 */

$cakeDescription = 'DEKA';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?= $cakeDescription ?> - 
        <?= $this->fetch('title') ?>
    </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="description" content="CodedThemes">
    <meta name="keywords" content="flat ui, admin  Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="CodedThemes">
    <!-- Favicon icon -->
    <link rel="icon" href="/deka/images/favicon.ico" type="image/x-icon">

    <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
    <!-- themify-icons line icon -->
    <link rel="stylesheet" type="text/css" href="/deka/icon/themify-icons/themify-icons.css">
    
    <!-- ico font -->
    <link rel="stylesheet" type="text/css" href="/deka/icon/icofont/css/icofont.css">
    <!-- Style.css -->
    <?= $this->Html->css('bootstrap/css/bootstrap.min.css') ?>
    <?= $this->Html->script("jquery/jquery.min.js") ?>
    <?= $this->Html->css('style.css') ?>
    <?= $this->Html->css('jquery.mCustomScrollbar.css') ?>
</head>
<body>
    <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="ball-scale">
            <div class='contain'>
                <div class="ring"><div class="frame"></div></div>
                <div class="ring"><div class="frame"></div></div>
                <div class="ring"><div class="frame"></div></div>
                <div class="ring"><div class="frame"></div></div>
                <div class="ring"><div class="frame"></div></div>
                <div class="ring"><div class="frame"></div></div>
                <div class="ring"><div class="frame"></div></div>
                <div class="ring"><div class="frame"></div></div>
                <div class="ring"><div class="frame"></div></div>
                <div class="ring"><div class="frame"></div></div>
            </div>
        </div>
    </div>
    <!-- Pre-loader end -->
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper" style="height:100vh">

            <nav class="navbar header-navbar pcoded-header">
                <div class="navbar-wrapper">

                    <div class="navbar-logo">
                        <a href="index.html">
                            <h3 style="margin-top:3px">DEKA</h3>
                        </a>
                    </div>

                    <div class="navbar-container container-fluid">
                        
                    </div>
                </div>
            </nav>

                            <!-- Page-header start -->
            <?= $this->fetch("content") ?>
        </div>
    </div>


<!-- Required Jquery -->

<?= $this->Html->script("jquery-ui/jquery-ui.min.js") ?>
<?= $this->Html->script("popper.js/popper.min.js") ?>
<?= $this->Html->script("bootstrap/js/bootstrap.min.js") ?>
<!-- jquery slimscroll js -->
<?= $this->Html->script("jquery-slimscroll/jquery.slimscroll.js") ?>
<!-- modernizr js -->
<?= $this->Html->script("modernizr/modernizr.js") ?>
<?= $this->Html->script("modernizr/css-scrollbars.js") ?>
<!-- classie js -->
<?= $this->Html->script("classie/classie.js") ?>
<!-- Custom js -->
<?= $this->Html->script("script.js") ?>
<?= $this->Html->script("pcoded.min.js") ?>
<?= $this->Html->script("demo-12.js") ?>
<?= $this->Html->script("jquery.mCustomScrollbar.concat.min.js") ?>
</body>
</html>
