<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 * @var \App\View\AppView $this
 */

$cakeDescription = 'DEKA';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?= $cakeDescription ?> - 
        <?= $this->fetch('title') ?>
    </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="description" content="CodedThemes">
    <meta name="keywords" content="flat ui, admin  Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="CodedThemes">
    <!-- Favicon icon -->
    <link rel="icon" href="<?= ROOT_DIREC ?>/images/favicon.ico" type="image/x-icon">

    <!-- <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet"> -->

    <!-- themify-icons line icon -->
    <link rel="stylesheet" type="text/css" href="<?= ROOT_DIREC ?>/icon/themify-icons/themify-icons.css">
    
    <!-- ico font -->
    <link rel="stylesheet" type="text/css" href="<?= ROOT_DIREC ?>/icon/icofont/css/icofont.css">
    <!-- Style.css -->
    <?= $this->Html->css('bootstrap/css/bootstrap.min.css') ?>
    <?= $this->Html->css('jquery.dataTables.min.css') ?>
    <?= $this->Html->css('jquery.dataTables2.min.css') ?>
    <?= $this->Html->script("jquery/jquery.min.js") ?>
    <?= $this->Html->css('style.css') ?>
    <?= $this->Html->css('jquery.mCustomScrollbar.css') ?>


    <?php  

    echo "<script>";
    echo "var ROOT_DIRECTORY = '".ROOT_DIREC."'";
    echo "</script>";

    ?>
</head>
<body>
    <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="ball-scale">
            <div class='contain'>
                <div class="ring"><div class="frame"></div></div>
                <div class="ring"><div class="frame"></div></div>
                <div class="ring"><div class="frame"></div></div>
                <div class="ring"><div class="frame"></div></div>
                <div class="ring"><div class="frame"></div></div>
                <div class="ring"><div class="frame"></div></div>
                <div class="ring"><div class="frame"></div></div>
                <div class="ring"><div class="frame"></div></div>
                <div class="ring"><div class="frame"></div></div>
                <div class="ring"><div class="frame"></div></div>
            </div>
        </div>
    </div>
    <!-- Pre-loader end -->
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">

            <nav class="navbar header-navbar pcoded-header">
                <div class="navbar-wrapper">

                    <div class="navbar-logo">
                        <a class="mobile-menu" id="mobile-collapse" href="#!">
                            <i class="ti-menu"></i>
                        </a>
                        <a class="mobile-search morphsearch-search" href="#">
                            <i class="ti-search"></i>
                        </a>
                        <a href="index.html">
                            <h3 style="margin-top:3px">DEKA</h3>
                        </a>
                        <a class="mobile-options">
                            <i class="ti-more"></i>
                        </a>
                    </div>

                    <div class="navbar-container container-fluid">
                        <ul class="nav-left">
                            <li>
                                <div class="sidebar_toggle"><a href="javascript:void(0)"><i class="ti-menu"></i></a></div>
                            </li>
                        </ul>
                        <ul class="nav-right">
                            <li>
                                <?= $this->Form->create(null, array("url" => "/sales/set_dates")) ?>
                                <input value="<?= $filterfrom ?>" type="date" name="from" style="height: 40px;width: 150px;color: black;border: 1px solid #ddd;margin-right: 5px;padding:5px;margin-top:20px">
                                <input value="<?= $filterto ?>" type="date" name="to" style="height: 40px;width: 150px;color: black;border: 1px solid #ddd;margin-right: 5px;padding:5px;margin-top:5px">
                                <button class="btn btn-success" style="padding: 5px 12px!important;
                margin-top: -2px!important;height:40px;width:50px"><i class="ti ti-check"></i></button>
                                <?= $this->Form->end() ?>
                            </li>
                            <li class="user-profile header-notification">
                                <a href="#!">
                                    <span><?= $connected_user['name'] ?></span>
                                    <i class="ti-angle-down"></i>
                                </a>
                                <ul class="show-notification profile-notification">
                                    <li>
                                        <a href="#">
                                            <i class="ti-user"></i> Profil
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?= ROOT_DIREC ?>/users/logout">
                                            <i class="ti-power-off"></i> Déconnexion
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        
                    </div>
                </div>
            </nav>

            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    <nav class="pcoded-navbar">
                        <div class="sidebar_toggle"><a href="#"><i class="icon-close icons"></i></a></div>
                        <div class="pcoded-inner-navbar main-menu">
                            <div class="pcoded-search">
                                <?= $this->Form->create(null, array("url" => "/sales/search")) ?>
                                <span class="searchbar-toggle">  </span>
                                <div class="pcoded-search-box ">
                                    <input type="text" name="sale_number" placeholder="Rechercher">
                                    <span class="search-icon"><i class="ti-search" aria-hidden="true"></i></span>
                                </div>
                                <?= $this->Form->end() ?>
                            </div>
                            <div class="pcoded-navigatio-lavel" data-i18n="nav.category.forms">Actions</div>
                            <ul class="pcoded-item pcoded-left-item">
                                <li class="">
                                    <a href="<?= ROOT_DIREC ?>/calls/add">
                                        <span class="pcoded-micon"><i class="ti-headphone"></i><b>D</b></span>
                                        <span class="pcoded-mtext" data-i18n="nav.dash.main">Nouvel Appel</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>
                                <li class="">
                                    <a href="<?= ROOT_DIREC ?>/sales/direct">
                                        <span class="pcoded-micon"><i class="ti-shopping-cart"></i><b>D</b></span>
                                        <span class="pcoded-mtext" data-i18n="nav.dash.main">Nouvelle Vente</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>
                            </ul>
                            <div class="pcoded-navigatio-lavel" data-i18n="nav.category.forms">Rapports</div>
                            <ul class="pcoded-item pcoded-left-item">
                                <li class="">
                                    <a href="<?= ROOT_DIREC ?>/calls">
                                        <span class="pcoded-micon"><i class="ti-control-forward"></i><b>D</b></span>
                                        <span class="pcoded-mtext" data-i18n="nav.dash.main">Appels</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>
                                <li class="">
                                    <a href="<?= ROOT_DIREC ?>/caravanes/routing">
                                        <span class="pcoded-micon"><i class="ti-notepad"></i><b>D</b></span>
                                        <span class="pcoded-mtext" data-i18n="nav.dash.main">Feuille de Route</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>
                                <li class="">
                                    <a href="<?= ROOT_DIREC ?>/sales">
                                        <span class="pcoded-micon"><i class="ti-credit-card"></i><b>D</b></span>
                                        <span class="pcoded-mtext" data-i18n="nav.dash.main">Ventes</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>
                                <li class="">
                                    <a href="<?= ROOT_DIREC ?>/products/report">
                                        <span class="pcoded-micon"><i class="ti-notepad"></i><b>D</b></span>
                                        <span class="pcoded-mtext" data-i18n="nav.dash.main">Produits</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>
                            </ul>

                            <div class="pcoded-navigatio-lavel" data-i18n="nav.category.forms">Listes</div>
                            <ul class="pcoded-item pcoded-left-item">
                                <li>
                                    <a href="<?= ROOT_DIREC ?>/customers">
                                        <span class="pcoded-micon"><i class="ti-user"></i><b>FC</b></span>
                                        <span class="pcoded-mtext" data-i18n="nav.form-components.main">Clients</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= ROOT_DIREC ?>/caravanes">
                                        <span class="pcoded-micon"><i class="ti-truck"></i><b>FC</b></span>
                                        <span class="pcoded-mtext" data-i18n="nav.form-components.main">Caravanes</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= ROOT_DIREC ?>/zones">
                                        <span class="pcoded-micon"><i class="ti-location-pin"></i><b>FC</b></span>
                                        <span class="pcoded-mtext" data-i18n="nav.form-components.main">Zones</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                    <div class="pcoded-content">
                        <div class="pcoded-inner-content">
                        <nav aria-label="breadcrumb"  class="breadcrumb">
                          <ol  class="breadcrumb">
                            <?php foreach($breadcrumb as $title => $link) : ?>
                                <?php if($link == '#') : ?>
                                <li class="breadcrumb-item active" aria-current="page"><a href="#"><?= $title ?></a></li>
                                <?php else : ?>
                                    <li class="breadcrumb-item active" aria-current="page"><a href="<?= ROOT_DIREC.$link ?>"><?= $title ?></a></li>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            
                          </ol>
                        </nav>
                            <div class="main-body">
                                <div class="page-wrapper">
                                    <!-- Page-header start -->

                                    <div class="page-body">
                                        <?= $this->fetch("content") ?>
                                    </div>
                                </div>
                            </div>
                            <div id="styleSelector">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<style type="text/css">
    .message.error{
    background: red;
    color: white;
    font-weight: bold;
    padding: 5px;
    text-align: center;
    margin-bottom: 10px;
    }
</style>

<!-- Required Jquery -->
<?= $this->Html->script("jquery.dataTables.min.js") ?>
<?= $this->Html->script("jquery-ui/jquery-ui.min.js") ?>
<?= $this->Html->script("custom.js") ?>
<?= $this->Html->script("popper.js/popper.min.js") ?>
<?= $this->Html->script("bootstrap/js/bootstrap.min.js") ?>
<!-- jquery slimscroll js -->
<?= $this->Html->script("jquery-slimscroll/jquery.slimscroll.js") ?>
<!-- modernizr js -->
<?= $this->Html->script("modernizr/modernizr.js") ?>
<?= $this->Html->script("modernizr/css-scrollbars.js") ?>
<!-- classie js -->
<?= $this->Html->script("classie/classie.js") ?>

<?= $this->Html->script("raphael/raphael.min.js") ?>
<!-- Custom js -->
<?= $this->Html->script("script.js") ?>

<?= $this->Html->script("pcoded.min.js") ?>
<?= $this->Html->script("demo-12.js") ?>
<?= $this->Html->script("jquery.mCustomScrollbar.concat.min.js") ?>
</body>
</html>
