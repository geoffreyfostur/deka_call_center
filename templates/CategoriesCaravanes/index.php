<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CategoriesCaravane[]|\Cake\Collection\CollectionInterface $categoriesCaravanes
 */
?>
<div class="categoriesCaravanes index content">
    <?= $this->Html->link(__('New Categories Caravane'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Categories Caravanes') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('category_id') ?></th>
                    <th><?= $this->Paginator->sort('caravane_id') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($categoriesCaravanes as $categoriesCaravane): ?>
                <tr>
                    <td><?= $this->Number->format($categoriesCaravane->id) ?></td>
                    <td><?= $categoriesCaravane->has('category') ? $this->Html->link($categoriesCaravane->category->name, ['controller' => 'Categories', 'action' => 'view', $categoriesCaravane->category->id]) : '' ?></td>
                    <td><?= $categoriesCaravane->has('caravane') ? $this->Html->link($categoriesCaravane->caravane->name, ['controller' => 'Caravanes', 'action' => 'view', $categoriesCaravane->caravane->id]) : '' ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $categoriesCaravane->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $categoriesCaravane->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $categoriesCaravane->id], ['confirm' => __('Are you sure you want to delete # {0}?', $categoriesCaravane->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
