<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CategoriesCaravane $categoriesCaravane
 * @var string[]|\Cake\Collection\CollectionInterface $categories
 * @var string[]|\Cake\Collection\CollectionInterface $caravanes
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $categoriesCaravane->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $categoriesCaravane->id), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('List Categories Caravanes'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="categoriesCaravanes form content">
            <?= $this->Form->create($categoriesCaravane) ?>
            <fieldset>
                <legend><?= __('Edit Categories Caravane') ?></legend>
                <?php
                    echo $this->Form->control('category_id', ['options' => $categories]);
                    echo $this->Form->control('caravane_id', ['options' => $caravanes]);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
