<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CategoriesCaravane $categoriesCaravane
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Categories Caravane'), ['action' => 'edit', $categoriesCaravane->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Categories Caravane'), ['action' => 'delete', $categoriesCaravane->id], ['confirm' => __('Are you sure you want to delete # {0}?', $categoriesCaravane->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Categories Caravanes'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Categories Caravane'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="categoriesCaravanes view content">
            <h3><?= h($categoriesCaravane->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Category') ?></th>
                    <td><?= $categoriesCaravane->has('category') ? $this->Html->link($categoriesCaravane->category->name, ['controller' => 'Categories', 'action' => 'view', $categoriesCaravane->category->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Caravane') ?></th>
                    <td><?= $categoriesCaravane->has('caravane') ? $this->Html->link($categoriesCaravane->caravane->name, ['controller' => 'Caravanes', 'action' => 'view', $categoriesCaravane->caravane->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($categoriesCaravane->id) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
