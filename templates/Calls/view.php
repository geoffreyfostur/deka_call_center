<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Call $call
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Call'), ['action' => 'edit', $call->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Call'), ['action' => 'delete', $call->id], ['confirm' => __('Are you sure you want to delete # {0}?', $call->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Calls'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Call'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="calls view content">
            <h3><?= h($call->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('User') ?></th>
                    <td><?= $call->has('user') ? $this->Html->link($call->user->name, ['controller' => 'Users', 'action' => 'view', $call->user->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Customer') ?></th>
                    <td><?= $call->has('customer') ? $this->Html->link($call->customer->name, ['controller' => 'Customers', 'action' => 'view', $call->customer->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Zone') ?></th>
                    <td><?= $call->has('zone') ? $this->Html->link($call->zone->name, ['controller' => 'Zones', 'action' => 'view', $call->zone->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Route') ?></th>
                    <td><?= $call->has('route') ? $this->Html->link($call->route->name, ['controller' => 'Routes', 'action' => 'view', $call->route->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Caravane') ?></th>
                    <td><?= $call->has('caravane') ? $this->Html->link($call->caravane->name, ['controller' => 'Caravanes', 'action' => 'view', $call->caravane->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Note') ?></th>
                    <td><?= h($call->note) ?></td>
                </tr>
                <tr>
                    <th><?= __('Sale') ?></th>
                    <td><?= $call->has('sale') ? $this->Html->link($call->sale->id, ['controller' => 'Sales', 'action' => 'view', $call->sale->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($call->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Answered') ?></th>
                    <td><?= $this->Number->format($call->answered) ?></td>
                </tr>
                <tr>
                    <th><?= __('Visible') ?></th>
                    <td><?= $this->Number->format($call->visible) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created') ?></th>
                    <td><?= h($call->created) ?></td>
                </tr>
                <tr>
                    <th><?= __('Modified') ?></th>
                    <td><?= h($call->modified) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
