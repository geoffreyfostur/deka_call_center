<div class="row">
    <div class="col-md-12">
        <a class = "btn btn-primary" href="<?= ROOT_DIREC ?>/calls/refresh" style="float:right"><i class="ti ti-reload"></i></a>
    </div>
</div>

<div class="row">
    <div class="col-lg-12 col-xl-12">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs md-tabs " role="tablist">
            <li class="nav-item">
                <?php if(empty($call['caravane_id'])) : ?>
                    <a class="nav-link active" data-toggle="tab" href="#home7" role="tab"><i class="ti ti-truck"></i> Caravane</a>
                <?php else : ?>
                    <a class="nav-link" data-toggle="tab" href="#home7" role="tab"><i class="ti ti-truck"></i> Caravane (<?= $call['caravane_info']->identification ?>)</a>
                <?php endif; ?>
                
                <div class="slide"></div>
            </li>

            <li class="nav-item">
                <?php if(empty($call['zone_id'])) : ?>
                    <?php if(empty($call['caravane_id'])) : ?>
                        <a class="nav-link" href="#" role="tab"><i class="ti ti-map"></i> Zone</a>
                    <?php else : ?>
                        <a class="nav-link active" data-toggle="tab" href="#zonetab" role="tab"><i class="ti ti-user"></i> Zone</a>
                    <?php endif; ?>
                    
                <?php else : ?>
                    <a class="nav-link" data-toggle="tab" href="#zonetab" role="tab"><i class="ti ti-user"></i> Zone (<?= $call['zone_info']->name ?>)</a>
                <?php endif; ?>
                <div class="slide"></div>
            </li>

            <li class="nav-item">
                <?php if(empty($call['customer_id'])) : ?>
                    <?php if(empty($call['zone_id'])) : ?>
                        <a class="nav-link" href="#" role="tab"><i class="ti ti-user"></i> Client</a>
                    <?php else : ?>
                        <a class="nav-link active" data-toggle="tab" href="#profile7" role="tab"><i class="ti ti-user"></i> Client</a>
                    <?php endif; ?>
                    
                <?php else : ?>
                    <a class="nav-link" data-toggle="tab" href="#profile7" role="tab"><i class="ti ti-user"></i> Client (<?= $call['customer_info']->name ?>)</a>
                <?php endif; ?>
                <div class="slide"></div>
            </li>
            
                <?php if(!empty($call['customer_id']) && !empty($call['caravane_id'])) : ?>
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#messages7" role="tab"><i class="ti ti-microphone"></i> Appel</a>
                <?php else : ?>
                    <li class="nav-item">
                    <a class="nav-link" href="#" role="tab"><i class="ti ti-microphone"></i> Appel</a>
                <?php endif; ?>
                
                <div class="slide"></div>
            </li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content card-block">
            <?php if(empty($call['caravane_id'])) : ?>
                <div class="tab-pane active" id="home7" role="tabpanel">
                    <?php echo $this->element('caravanes', array('caravanes' => $caravanes, 'selected' => '')); ?>
                </div>
            <?php else : ?>
                <div class="tab-pane" id="home7" role="tabpanel">
                    <?php echo $this->element('caravanes', array('caravanes' => $caravanes, 'selected' => $call['caravane_id'])); ?>
                </div>
            <?php endif; ?>


           <?php if(empty($call['zone_id'])) : ?>
                <?php if(empty($call['caravane_id'])) : ?>
                    <div class="tab-pane" id="zonetab" role="tabpanel">
                    <?php echo $this->element('zones', array('zones' => $zones, 'selected' => '')); ?>
                </div>
                <?php else : ?>
                    <div class="tab-pane active" id="zonetab" role="tabpanel">
                        <?php echo $this->element('zones', array('zones' => $zones, 'selected' => '')); ?>
                    </div>
                <?php endif; ?>
            <?php else : ?>
                <div class="tab-pane" id="zonetab" role="tabpanel">
                    <?php echo $this->element('zones', array('zones' => $zones, 'selected' => $call['zone_id'])); ?>
                </div>
            <?php endif; ?>


            <?php if(empty($call['customer_id'])) : ?>
                <?php if(empty($call['zone_id'])) : ?>
                    <div class="tab-pane" id="profile7" role="tabpanel">
                        <?php echo $this->element('customers', array('customers' => $customers, 'selected' => '')); ?>
                    </div>
                <?php else : ?>
                    <div class="tab-pane active" id="profile7" role="tabpanel">
                        <?php echo $this->element('customers', array('customers' => $customers, 'selected' => '')); ?>
                    </div>
                <?php endif; ?>
                
            <?php else : ?>
                <div class="tab-pane" id="profile7" role="tabpanel">
                    <?php echo $this->element('customers', array('customers' => $customers, 'selected' => $call['customer_id'])); ?>
                </div>
            <?php endif; ?>


            <?php if(!empty($call['customer_id']) && !empty($call['caravane_id'])) : ?>
                <div class="tab-pane active" id="messages7" role="tabpanel">
                    <?php echo $this->element('call', array('call' => $call)); ?>
                </div>
            <?php else : ?>
                <div class="tab-pane" id="messages7" role="tabpanel">
                    <?php echo $this->element('call', array('call' => $call)); ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>