<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Call $call
 * @var string[]|\Cake\Collection\CollectionInterface $users
 * @var string[]|\Cake\Collection\CollectionInterface $customers
 * @var string[]|\Cake\Collection\CollectionInterface $zones
 * @var string[]|\Cake\Collection\CollectionInterface $routes
 * @var string[]|\Cake\Collection\CollectionInterface $caravanes
 * @var string[]|\Cake\Collection\CollectionInterface $sales
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $call->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $call->id), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('List Calls'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="calls form content">
            <?= $this->Form->create($call) ?>
            <fieldset>
                <legend><?= __('Edit Call') ?></legend>
                <?php
                    echo $this->Form->control('user_id', ['options' => $users]);
                    echo $this->Form->control('customer_id', ['options' => $customers]);
                    echo $this->Form->control('zone_id', ['options' => $zones]);
                    echo $this->Form->control('route_id', ['options' => $routes]);
                    echo $this->Form->control('caravane_id', ['options' => $caravanes]);
                    echo $this->Form->control('answered');
                    echo $this->Form->control('note');
                    echo $this->Form->control('sale_id', ['options' => $sales, 'empty' => true]);
                    echo $this->Form->control('visible');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
