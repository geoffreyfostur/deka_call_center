<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Customer[]|\Cake\Collection\CollectionInterface $customers
 */
?>
<div class="caravanes index content">
    <div class="table-responsive mt-10">
        <table class="table datatable">
            <thead>
                <tr>
                    <th>Client</th>
                    <th>Caravane</th>
                    <th>Zone</th>
                    <th>Répondu</th>
                    <th>Proforma</th>
                    <th>Date</th>
                    <th>Heure</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($calls as $call): ?>
                <tr>
                    <td data-toggle="tooltip" data-placement="top" data-trigger="hover" title="" data-original-title="<?= $call->note ?>"><?= h($call->customer->name) ?></td>
                    <td><?= h($call->caravane->identification) ?></td>
                    <td><?= h($call->zone->name) ?></td>
                    <?php if($call->answered == 1) : ?>
                        <td><span class="label label-success">OUI</span></td>
                    <?php else : ?>
                        <td><span class="label label-danger">NON</span></td>
                    <?php endif; ?>
                    <td>
                        <?php if(!empty($call->sale)) : ?>
                            <a href="<?= ROOT_DIREC ?>/sales/view/<?= $call->sale_id ?>"><span class="label label-primary"><?= $call->sale->sale_number ?></span></a>
                        <?php else : ?>
                            -
                        <?php endif; ?>
                    </td>
                    <td><?= date("Y-m-d", strtotime($call->created)) ?></td>
                    <td><?= date("H:i", strtotime($call->created)) ?></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>