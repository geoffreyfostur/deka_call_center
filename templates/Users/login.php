<div class="row" style="margin-top:100px;padding-bottom:30px">
	<div class="col-md-4"></div>
	<div class="col-md-4 col-md-offset-4" style="padding:15px;
  background:white;
  box-shadow: 0 2px 7px -3px rgba(0, 0, 0, 0.5);
  border-radius:4px;">
		<div class="login-panel panel panel-default">
			<div class="panel-heading">Connexion</div>
			<div class="panel-body">
				<?= $this->Form->create() ?>
					<div class="form-group">
						<?= $this->Form->control("username", array('label' => false, "class" => "form-control loginForm", "placeholder" => "Nom d'Utilisateur")) ?>
					</div>
					<div class="form-group">
						<?= $this->Form->control("password", array('label' => false, "class" => "form-control loginForm", "placeholder" => "Mot de Passe")) ?>
					</div>
					<?= $this->Flash->render() ?>
            		<?= $this->Form->button("Submit", array('class' => "btn btn-success loginForm")) ?>
        		<?= $this->Form->end() ?>
			</div>
		</div>
	</div><!-- /.col-->
</div><!-- /.row -->	