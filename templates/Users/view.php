<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit User'), ['action' => 'edit', $user->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete User'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Users'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New User'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="users view content">
            <h3><?= h($user->name) ?></h3>
            <table>
                <tr>
                    <th><?= __('Name') ?></th>
                    <td><?= h($user->name) ?></td>
                </tr>
                <tr>
                    <th><?= __('Username') ?></th>
                    <td><?= h($user->username) ?></td>
                </tr>
                <tr>
                    <th><?= __('Password') ?></th>
                    <td><?= h($user->password) ?></td>
                </tr>
                <tr>
                    <th><?= __('Email') ?></th>
                    <td><?= h($user->email) ?></td>
                </tr>
                <tr>
                    <th><?= __('Role') ?></th>
                    <td><?= $user->has('role') ? $this->Html->link($user->role->name, ['controller' => 'Roles', 'action' => 'view', $user->role->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Phone') ?></th>
                    <td><?= h($user->phone) ?></td>
                </tr>
                <tr>
                    <th><?= __('Center') ?></th>
                    <td><?= $user->has('center') ? $this->Html->link($user->center->name, ['controller' => 'Centers', 'action' => 'view', $user->center->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($user->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Status') ?></th>
                    <td><?= $this->Number->format($user->status) ?></td>
                </tr>
                <tr>
                    <th><?= __('Visible') ?></th>
                    <td><?= $this->Number->format($user->visible) ?></td>
                </tr>
                <tr>
                    <th><?= __('Caravane Id') ?></th>
                    <td><?= $this->Number->format($user->caravane_id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created') ?></th>
                    <td><?= h($user->created) ?></td>
                </tr>
                <tr>
                    <th><?= __('Modified') ?></th>
                    <td><?= h($user->modified) ?></td>
                </tr>
            </table>
            <div class="related">
                <h4><?= __('Related Caravanes') ?></h4>
                <?php if (!empty($user->caravanes)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('Name') ?></th>
                            <th><?= __('Seller Name') ?></th>
                            <th><?= __('Seller Phone') ?></th>
                            <th><?= __('Identification') ?></th>
                            <th><?= __('Registration') ?></th>
                            <th><?= __('User Id') ?></th>
                            <th><?= __('Status') ?></th>
                            <th><?= __('Created') ?></th>
                            <th><?= __('Modified') ?></th>
                            <th><?= __('Visible') ?></th>
                            <th><?= __('Center Id') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($user->caravanes as $caravanes) : ?>
                        <tr>
                            <td><?= h($caravanes->id) ?></td>
                            <td><?= h($caravanes->name) ?></td>
                            <td><?= h($caravanes->seller_name) ?></td>
                            <td><?= h($caravanes->seller_phone) ?></td>
                            <td><?= h($caravanes->identification) ?></td>
                            <td><?= h($caravanes->registration) ?></td>
                            <td><?= h($caravanes->user_id) ?></td>
                            <td><?= h($caravanes->status) ?></td>
                            <td><?= h($caravanes->created) ?></td>
                            <td><?= h($caravanes->modified) ?></td>
                            <td><?= h($caravanes->visible) ?></td>
                            <td><?= h($caravanes->center_id) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Caravanes', 'action' => 'view', $caravanes->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Caravanes', 'action' => 'edit', $caravanes->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Caravanes', 'action' => 'delete', $caravanes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $caravanes->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
            <div class="related">
                <h4><?= __('Related Calls') ?></h4>
                <?php if (!empty($user->calls)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('User Id') ?></th>
                            <th><?= __('Customer Id') ?></th>
                            <th><?= __('Zone Id') ?></th>
                            <th><?= __('Route Id') ?></th>
                            <th><?= __('Caravane Id') ?></th>
                            <th><?= __('Answered') ?></th>
                            <th><?= __('Note') ?></th>
                            <th><?= __('Sale Id') ?></th>
                            <th><?= __('Created') ?></th>
                            <th><?= __('Modified') ?></th>
                            <th><?= __('Visible') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($user->calls as $calls) : ?>
                        <tr>
                            <td><?= h($calls->id) ?></td>
                            <td><?= h($calls->user_id) ?></td>
                            <td><?= h($calls->customer_id) ?></td>
                            <td><?= h($calls->zone_id) ?></td>
                            <td><?= h($calls->route_id) ?></td>
                            <td><?= h($calls->caravane_id) ?></td>
                            <td><?= h($calls->answered) ?></td>
                            <td><?= h($calls->note) ?></td>
                            <td><?= h($calls->sale_id) ?></td>
                            <td><?= h($calls->created) ?></td>
                            <td><?= h($calls->modified) ?></td>
                            <td><?= h($calls->visible) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Calls', 'action' => 'view', $calls->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Calls', 'action' => 'edit', $calls->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Calls', 'action' => 'delete', $calls->id], ['confirm' => __('Are you sure you want to delete # {0}?', $calls->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
            <div class="related">
                <h4><?= __('Related Customers') ?></h4>
                <?php if (!empty($user->customers)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('Name') ?></th>
                            <th><?= __('Phone') ?></th>
                            <th><?= __('Address') ?></th>
                            <th><?= __('Lattitude') ?></th>
                            <th><?= __('Longitude') ?></th>
                            <th><?= __('Note') ?></th>
                            <th><?= __('Status') ?></th>
                            <th><?= __('Zone Id') ?></th>
                            <th><?= __('Route Id') ?></th>
                            <th><?= __('User Id') ?></th>
                            <th><?= __('Created') ?></th>
                            <th><?= __('Modified') ?></th>
                            <th><?= __('Visible') ?></th>
                            <th><?= __('Second Phone') ?></th>
                            <th><?= __('Nif') ?></th>
                            <th><?= __('Company') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($user->customers as $customers) : ?>
                        <tr>
                            <td><?= h($customers->id) ?></td>
                            <td><?= h($customers->name) ?></td>
                            <td><?= h($customers->phone) ?></td>
                            <td><?= h($customers->address) ?></td>
                            <td><?= h($customers->lattitude) ?></td>
                            <td><?= h($customers->longitude) ?></td>
                            <td><?= h($customers->note) ?></td>
                            <td><?= h($customers->status) ?></td>
                            <td><?= h($customers->zone_id) ?></td>
                            <td><?= h($customers->route_id) ?></td>
                            <td><?= h($customers->user_id) ?></td>
                            <td><?= h($customers->created) ?></td>
                            <td><?= h($customers->modified) ?></td>
                            <td><?= h($customers->visible) ?></td>
                            <td><?= h($customers->second_phone) ?></td>
                            <td><?= h($customers->nif) ?></td>
                            <td><?= h($customers->company) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Customers', 'action' => 'view', $customers->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Customers', 'action' => 'edit', $customers->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Customers', 'action' => 'delete', $customers->id], ['confirm' => __('Are you sure you want to delete # {0}?', $customers->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
            <div class="related">
                <h4><?= __('Related Products') ?></h4>
                <?php if (!empty($user->products)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('Name') ?></th>
                            <th><?= __('Category Id') ?></th>
                            <th><?= __('Grouping Id') ?></th>
                            <th><?= __('Price') ?></th>
                            <th><?= __('Promotion Price') ?></th>
                            <th><?= __('Promotion Start') ?></th>
                            <th><?= __('Promotion End') ?></th>
                            <th><?= __('User Id') ?></th>
                            <th><?= __('Created') ?></th>
                            <th><?= __('Modified') ?></th>
                            <th><?= __('Visible') ?></th>
                            <th><?= __('Status') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($user->products as $products) : ?>
                        <tr>
                            <td><?= h($products->id) ?></td>
                            <td><?= h($products->name) ?></td>
                            <td><?= h($products->category_id) ?></td>
                            <td><?= h($products->grouping_id) ?></td>
                            <td><?= h($products->price) ?></td>
                            <td><?= h($products->promotion_price) ?></td>
                            <td><?= h($products->promotion_start) ?></td>
                            <td><?= h($products->promotion_end) ?></td>
                            <td><?= h($products->user_id) ?></td>
                            <td><?= h($products->created) ?></td>
                            <td><?= h($products->modified) ?></td>
                            <td><?= h($products->visible) ?></td>
                            <td><?= h($products->status) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Products', 'action' => 'view', $products->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Products', 'action' => 'edit', $products->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Products', 'action' => 'delete', $products->id], ['confirm' => __('Are you sure you want to delete # {0}?', $products->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
            <div class="related">
                <h4><?= __('Related Promotions') ?></h4>
                <?php if (!empty($user->promotions)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('Type') ?></th>
                            <th><?= __('Start Date') ?></th>
                            <th><?= __('End Date') ?></th>
                            <th><?= __('Product Id') ?></th>
                            <th><?= __('Promotion Price') ?></th>
                            <th><?= __('Target Product Id') ?></th>
                            <th><?= __('Start Quantity') ?></th>
                            <th><?= __('End Quantity') ?></th>
                            <th><?= __('User Id') ?></th>
                            <th><?= __('Created') ?></th>
                            <th><?= __('Modified') ?></th>
                            <th><?= __('Keep Active') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($user->promotions as $promotions) : ?>
                        <tr>
                            <td><?= h($promotions->id) ?></td>
                            <td><?= h($promotions->type) ?></td>
                            <td><?= h($promotions->start_date) ?></td>
                            <td><?= h($promotions->end_date) ?></td>
                            <td><?= h($promotions->product_id) ?></td>
                            <td><?= h($promotions->promotion_price) ?></td>
                            <td><?= h($promotions->target_product_id) ?></td>
                            <td><?= h($promotions->start_quantity) ?></td>
                            <td><?= h($promotions->end_quantity) ?></td>
                            <td><?= h($promotions->user_id) ?></td>
                            <td><?= h($promotions->created) ?></td>
                            <td><?= h($promotions->modified) ?></td>
                            <td><?= h($promotions->keep_active) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Promotions', 'action' => 'view', $promotions->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Promotions', 'action' => 'edit', $promotions->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Promotions', 'action' => 'delete', $promotions->id], ['confirm' => __('Are you sure you want to delete # {0}?', $promotions->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
            <div class="related">
                <h4><?= __('Related Sales') ?></h4>
                <?php if (!empty($user->sales)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('Sale Number') ?></th>
                            <th><?= __('Customer Id') ?></th>
                            <th><?= __('Caravane Id') ?></th>
                            <th><?= __('Zone Id') ?></th>
                            <th><?= __('Route Id') ?></th>
                            <th><?= __('Status') ?></th>
                            <th><?= __('Delivery Date') ?></th>
                            <th><?= __('Delivered') ?></th>
                            <th><?= __('Type') ?></th>
                            <th><?= __('Note') ?></th>
                            <th><?= __('Problem') ?></th>
                            <th><?= __('Total Ordered') ?></th>
                            <th><?= __('Total Sold') ?></th>
                            <th><?= __('Created') ?></th>
                            <th><?= __('Modified') ?></th>
                            <th><?= __('User Id') ?></th>
                            <th><?= __('Last Delivery Date') ?></th>
                            <th><?= __('Visible') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($user->sales as $sales) : ?>
                        <tr>
                            <td><?= h($sales->id) ?></td>
                            <td><?= h($sales->sale_number) ?></td>
                            <td><?= h($sales->customer_id) ?></td>
                            <td><?= h($sales->caravane_id) ?></td>
                            <td><?= h($sales->zone_id) ?></td>
                            <td><?= h($sales->route_id) ?></td>
                            <td><?= h($sales->status) ?></td>
                            <td><?= h($sales->delivery_date) ?></td>
                            <td><?= h($sales->delivered) ?></td>
                            <td><?= h($sales->type) ?></td>
                            <td><?= h($sales->note) ?></td>
                            <td><?= h($sales->problem) ?></td>
                            <td><?= h($sales->total_ordered) ?></td>
                            <td><?= h($sales->total_sold) ?></td>
                            <td><?= h($sales->created) ?></td>
                            <td><?= h($sales->modified) ?></td>
                            <td><?= h($sales->user_id) ?></td>
                            <td><?= h($sales->last_delivery_date) ?></td>
                            <td><?= h($sales->visible) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Sales', 'action' => 'view', $sales->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Sales', 'action' => 'edit', $sales->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Sales', 'action' => 'delete', $sales->id], ['confirm' => __('Are you sure you want to delete # {0}?', $sales->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
