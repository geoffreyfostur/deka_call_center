<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Article $article
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Article'), ['action' => 'edit', $article->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Article'), ['action' => 'delete', $article->id], ['confirm' => __('Are you sure you want to delete # {0}?', $article->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Articles'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Article'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="articles view content">
            <h3><?= h($article->name) ?></h3>
            <table>
                <tr>
                    <th><?= __('Name') ?></th>
                    <td><?= h($article->name) ?></td>
                </tr>
                <tr>
                    <th><?= __('Category') ?></th>
                    <td><?= $article->has('category') ? $this->Html->link($article->category->name, ['controller' => 'Categories', 'action' => 'view', $article->category->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($article->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Price') ?></th>
                    <td><?= $this->Number->format($article->price) ?></td>
                </tr>
            </table>
            <div class="related">
                <h4><?= __('Related Sales') ?></h4>
                <?php if (!empty($article->sales)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('Sale Number') ?></th>
                            <th><?= __('Customer Id') ?></th>
                            <th><?= __('Caravane Id') ?></th>
                            <th><?= __('Zone Id') ?></th>
                            <th><?= __('Route Id') ?></th>
                            <th><?= __('Status') ?></th>
                            <th><?= __('Delivery Date') ?></th>
                            <th><?= __('Delivered') ?></th>
                            <th><?= __('Type') ?></th>
                            <th><?= __('Note') ?></th>
                            <th><?= __('Problem') ?></th>
                            <th><?= __('Total Ordered') ?></th>
                            <th><?= __('Total Sold') ?></th>
                            <th><?= __('Created') ?></th>
                            <th><?= __('Modified') ?></th>
                            <th><?= __('User Id') ?></th>
                            <th><?= __('Last Delivery Date') ?></th>
                            <th><?= __('Visible') ?></th>
                            <th><?= __('Center Id') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($article->sales as $sales) : ?>
                        <tr>
                            <td><?= h($sales->id) ?></td>
                            <td><?= h($sales->sale_number) ?></td>
                            <td><?= h($sales->customer_id) ?></td>
                            <td><?= h($sales->caravane_id) ?></td>
                            <td><?= h($sales->zone_id) ?></td>
                            <td><?= h($sales->route_id) ?></td>
                            <td><?= h($sales->status) ?></td>
                            <td><?= h($sales->delivery_date) ?></td>
                            <td><?= h($sales->delivered) ?></td>
                            <td><?= h($sales->type) ?></td>
                            <td><?= h($sales->note) ?></td>
                            <td><?= h($sales->problem) ?></td>
                            <td><?= h($sales->total_ordered) ?></td>
                            <td><?= h($sales->total_sold) ?></td>
                            <td><?= h($sales->created) ?></td>
                            <td><?= h($sales->modified) ?></td>
                            <td><?= h($sales->user_id) ?></td>
                            <td><?= h($sales->last_delivery_date) ?></td>
                            <td><?= h($sales->visible) ?></td>
                            <td><?= h($sales->center_id) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Sales', 'action' => 'view', $sales->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Sales', 'action' => 'edit', $sales->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Sales', 'action' => 'delete', $sales->id], ['confirm' => __('Are you sure you want to delete # {0}?', $sales->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
