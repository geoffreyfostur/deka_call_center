<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Promotion $promotion
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Promotion'), ['action' => 'edit', $promotion->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Promotion'), ['action' => 'delete', $promotion->id], ['confirm' => __('Are you sure you want to delete # {0}?', $promotion->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Promotions'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Promotion'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="promotions view content">
            <h3><?= h($promotion->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Product') ?></th>
                    <td><?= $promotion->has('product') ? $this->Html->link($promotion->product->name, ['controller' => 'Products', 'action' => 'view', $promotion->product->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('User') ?></th>
                    <td><?= $promotion->has('user') ? $this->Html->link($promotion->user->name, ['controller' => 'Users', 'action' => 'view', $promotion->user->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($promotion->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Type') ?></th>
                    <td><?= $this->Number->format($promotion->type) ?></td>
                </tr>
                <tr>
                    <th><?= __('Promotion Price') ?></th>
                    <td><?= $this->Number->format($promotion->promotion_price) ?></td>
                </tr>
                <tr>
                    <th><?= __('Target Product Id') ?></th>
                    <td><?= $this->Number->format($promotion->target_product_id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Start Quantity') ?></th>
                    <td><?= $this->Number->format($promotion->start_quantity) ?></td>
                </tr>
                <tr>
                    <th><?= __('End Quantity') ?></th>
                    <td><?= $this->Number->format($promotion->end_quantity) ?></td>
                </tr>
                <tr>
                    <th><?= __('Keep Active') ?></th>
                    <td><?= $this->Number->format($promotion->keep_active) ?></td>
                </tr>
                <tr>
                    <th><?= __('Start Date') ?></th>
                    <td><?= h($promotion->start_date) ?></td>
                </tr>
                <tr>
                    <th><?= __('End Date') ?></th>
                    <td><?= h($promotion->end_date) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created') ?></th>
                    <td><?= h($promotion->created) ?></td>
                </tr>
                <tr>
                    <th><?= __('Modified') ?></th>
                    <td><?= h($promotion->modified) ?></td>
                </tr>
            </table>
            <div class="related">
                <h4><?= __('Related Products Sales') ?></h4>
                <?php if (!empty($promotion->products_sales)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('Product Id') ?></th>
                            <th><?= __('Sale Id') ?></th>
                            <th><?= __('List Price') ?></th>
                            <th><?= __('Price') ?></th>
                            <th><?= __('Quantity Ordered') ?></th>
                            <th><?= __('Quantity Delivered') ?></th>
                            <th><?= __('Total Ordered') ?></th>
                            <th><?= __('Total Delivered') ?></th>
                            <th><?= __('Promotion Id') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($promotion->products_sales as $productsSales) : ?>
                        <tr>
                            <td><?= h($productsSales->id) ?></td>
                            <td><?= h($productsSales->product_id) ?></td>
                            <td><?= h($productsSales->sale_id) ?></td>
                            <td><?= h($productsSales->list_price) ?></td>
                            <td><?= h($productsSales->price) ?></td>
                            <td><?= h($productsSales->quantity_ordered) ?></td>
                            <td><?= h($productsSales->quantity_delivered) ?></td>
                            <td><?= h($productsSales->total_ordered) ?></td>
                            <td><?= h($productsSales->total_delivered) ?></td>
                            <td><?= h($productsSales->promotion_id) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'ProductsSales', 'action' => 'view', $productsSales->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'ProductsSales', 'action' => 'edit', $productsSales->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'ProductsSales', 'action' => 'delete', $productsSales->id], ['confirm' => __('Are you sure you want to delete # {0}?', $productsSales->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
