<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Promotion[]|\Cake\Collection\CollectionInterface $promotions
 */
?>
<div class="promotions index content">
    <?= $this->Html->link(__('New Promotion'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Promotions') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('type') ?></th>
                    <th><?= $this->Paginator->sort('start_date') ?></th>
                    <th><?= $this->Paginator->sort('end_date') ?></th>
                    <th><?= $this->Paginator->sort('product_id') ?></th>
                    <th><?= $this->Paginator->sort('promotion_price') ?></th>
                    <th><?= $this->Paginator->sort('target_product_id') ?></th>
                    <th><?= $this->Paginator->sort('start_quantity') ?></th>
                    <th><?= $this->Paginator->sort('end_quantity') ?></th>
                    <th><?= $this->Paginator->sort('user_id') ?></th>
                    <th><?= $this->Paginator->sort('created') ?></th>
                    <th><?= $this->Paginator->sort('modified') ?></th>
                    <th><?= $this->Paginator->sort('keep_active') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($promotions as $promotion): ?>
                <tr>
                    <td><?= $this->Number->format($promotion->id) ?></td>
                    <td><?= $this->Number->format($promotion->type) ?></td>
                    <td><?= h($promotion->start_date) ?></td>
                    <td><?= h($promotion->end_date) ?></td>
                    <td><?= $promotion->has('product') ? $this->Html->link($promotion->product->name, ['controller' => 'Products', 'action' => 'view', $promotion->product->id]) : '' ?></td>
                    <td><?= $this->Number->format($promotion->promotion_price) ?></td>
                    <td><?= $this->Number->format($promotion->target_product_id) ?></td>
                    <td><?= $this->Number->format($promotion->start_quantity) ?></td>
                    <td><?= $this->Number->format($promotion->end_quantity) ?></td>
                    <td><?= $promotion->has('user') ? $this->Html->link($promotion->user->name, ['controller' => 'Users', 'action' => 'view', $promotion->user->id]) : '' ?></td>
                    <td><?= h($promotion->created) ?></td>
                    <td><?= h($promotion->modified) ?></td>
                    <td><?= $this->Number->format($promotion->keep_active) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $promotion->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $promotion->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $promotion->id], ['confirm' => __('Are you sure you want to delete # {0}?', $promotion->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
