<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Center $center
 * @var string[]|\Cake\Collection\CollectionInterface $products
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $center->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $center->id), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('List Centers'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="centers form content">
            <?= $this->Form->create($center) ?>
            <fieldset>
                <legend><?= __('Edit Center') ?></legend>
                <?php
                    echo $this->Form->control('name');
                    echo $this->Form->control('products._ids', ['options' => $products]);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
