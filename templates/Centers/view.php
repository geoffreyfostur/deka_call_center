<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Center $center
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Center'), ['action' => 'edit', $center->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Center'), ['action' => 'delete', $center->id], ['confirm' => __('Are you sure you want to delete # {0}?', $center->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Centers'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Center'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="centers view content">
            <h3><?= h($center->name) ?></h3>
            <table>
                <tr>
                    <th><?= __('Name') ?></th>
                    <td><?= h($center->name) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($center->id) ?></td>
                </tr>
            </table>
            <div class="related">
                <h4><?= __('Related Products') ?></h4>
                <?php if (!empty($center->products)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('Name') ?></th>
                            <th><?= __('Category Id') ?></th>
                            <th><?= __('Grouping Id') ?></th>
                            <th><?= __('Price') ?></th>
                            <th><?= __('Promotion Price') ?></th>
                            <th><?= __('Promotion Start') ?></th>
                            <th><?= __('Promotion End') ?></th>
                            <th><?= __('User Id') ?></th>
                            <th><?= __('Created') ?></th>
                            <th><?= __('Modified') ?></th>
                            <th><?= __('Visible') ?></th>
                            <th><?= __('Status') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($center->products as $products) : ?>
                        <tr>
                            <td><?= h($products->id) ?></td>
                            <td><?= h($products->name) ?></td>
                            <td><?= h($products->category_id) ?></td>
                            <td><?= h($products->grouping_id) ?></td>
                            <td><?= h($products->price) ?></td>
                            <td><?= h($products->promotion_price) ?></td>
                            <td><?= h($products->promotion_start) ?></td>
                            <td><?= h($products->promotion_end) ?></td>
                            <td><?= h($products->user_id) ?></td>
                            <td><?= h($products->created) ?></td>
                            <td><?= h($products->modified) ?></td>
                            <td><?= h($products->visible) ?></td>
                            <td><?= h($products->status) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Products', 'action' => 'view', $products->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Products', 'action' => 'edit', $products->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Products', 'action' => 'delete', $products->id], ['confirm' => __('Are you sure you want to delete # {0}?', $products->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
            <div class="related">
                <h4><?= __('Related Caravanes') ?></h4>
                <?php if (!empty($center->caravanes)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('Name') ?></th>
                            <th><?= __('Seller Name') ?></th>
                            <th><?= __('Seller Phone') ?></th>
                            <th><?= __('Identification') ?></th>
                            <th><?= __('Registration') ?></th>
                            <th><?= __('User Id') ?></th>
                            <th><?= __('Status') ?></th>
                            <th><?= __('Created') ?></th>
                            <th><?= __('Modified') ?></th>
                            <th><?= __('Visible') ?></th>
                            <th><?= __('Center Id') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($center->caravanes as $caravanes) : ?>
                        <tr>
                            <td><?= h($caravanes->id) ?></td>
                            <td><?= h($caravanes->name) ?></td>
                            <td><?= h($caravanes->seller_name) ?></td>
                            <td><?= h($caravanes->seller_phone) ?></td>
                            <td><?= h($caravanes->identification) ?></td>
                            <td><?= h($caravanes->registration) ?></td>
                            <td><?= h($caravanes->user_id) ?></td>
                            <td><?= h($caravanes->status) ?></td>
                            <td><?= h($caravanes->created) ?></td>
                            <td><?= h($caravanes->modified) ?></td>
                            <td><?= h($caravanes->visible) ?></td>
                            <td><?= h($caravanes->center_id) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Caravanes', 'action' => 'view', $caravanes->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Caravanes', 'action' => 'edit', $caravanes->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Caravanes', 'action' => 'delete', $caravanes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $caravanes->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
            <div class="related">
                <h4><?= __('Related Users') ?></h4>
                <?php if (!empty($center->users)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('Name') ?></th>
                            <th><?= __('Username') ?></th>
                            <th><?= __('Password') ?></th>
                            <th><?= __('Email') ?></th>
                            <th><?= __('Status') ?></th>
                            <th><?= __('Role Id') ?></th>
                            <th><?= __('Created') ?></th>
                            <th><?= __('Modified') ?></th>
                            <th><?= __('Visible') ?></th>
                            <th><?= __('Phone') ?></th>
                            <th><?= __('Caravane Id') ?></th>
                            <th><?= __('Center Id') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($center->users as $users) : ?>
                        <tr>
                            <td><?= h($users->id) ?></td>
                            <td><?= h($users->name) ?></td>
                            <td><?= h($users->username) ?></td>
                            <td><?= h($users->password) ?></td>
                            <td><?= h($users->email) ?></td>
                            <td><?= h($users->status) ?></td>
                            <td><?= h($users->role_id) ?></td>
                            <td><?= h($users->created) ?></td>
                            <td><?= h($users->modified) ?></td>
                            <td><?= h($users->visible) ?></td>
                            <td><?= h($users->phone) ?></td>
                            <td><?= h($users->caravane_id) ?></td>
                            <td><?= h($users->center_id) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Users', 'action' => 'view', $users->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Users', 'action' => 'edit', $users->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Users', 'action' => 'delete', $users->id], ['confirm' => __('Are you sure you want to delete # {0}?', $users->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
