<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Category $category
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Category'), ['action' => 'edit', $category->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Category'), ['action' => 'delete', $category->id], ['confirm' => __('Are you sure you want to delete # {0}?', $category->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Categories'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Category'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="categories view content">
            <h3><?= h($category->name) ?></h3>
            <table>
                <tr>
                    <th><?= __('Name') ?></th>
                    <td><?= h($category->name) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($category->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Visible') ?></th>
                    <td><?= $this->Number->format($category->visible) ?></td>
                </tr>
            </table>
            <div class="related">
                <h4><?= __('Related Caravanes') ?></h4>
                <?php if (!empty($category->caravanes)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('Name') ?></th>
                            <th><?= __('Seller Name') ?></th>
                            <th><?= __('Seller Phone') ?></th>
                            <th><?= __('Identification') ?></th>
                            <th><?= __('Registration') ?></th>
                            <th><?= __('User Id') ?></th>
                            <th><?= __('Status') ?></th>
                            <th><?= __('Created') ?></th>
                            <th><?= __('Modified') ?></th>
                            <th><?= __('Visible') ?></th>
                            <th><?= __('Center Id') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($category->caravanes as $caravanes) : ?>
                        <tr>
                            <td><?= h($caravanes->id) ?></td>
                            <td><?= h($caravanes->name) ?></td>
                            <td><?= h($caravanes->seller_name) ?></td>
                            <td><?= h($caravanes->seller_phone) ?></td>
                            <td><?= h($caravanes->identification) ?></td>
                            <td><?= h($caravanes->registration) ?></td>
                            <td><?= h($caravanes->user_id) ?></td>
                            <td><?= h($caravanes->status) ?></td>
                            <td><?= h($caravanes->created) ?></td>
                            <td><?= h($caravanes->modified) ?></td>
                            <td><?= h($caravanes->visible) ?></td>
                            <td><?= h($caravanes->center_id) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Caravanes', 'action' => 'view', $caravanes->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Caravanes', 'action' => 'edit', $caravanes->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Caravanes', 'action' => 'delete', $caravanes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $caravanes->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
            <div class="related">
                <h4><?= __('Related Products') ?></h4>
                <?php if (!empty($category->products)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('Name') ?></th>
                            <th><?= __('Category Id') ?></th>
                            <th><?= __('Grouping Id') ?></th>
                            <th><?= __('Price') ?></th>
                            <th><?= __('Promotion Price') ?></th>
                            <th><?= __('Promotion Start') ?></th>
                            <th><?= __('Promotion End') ?></th>
                            <th><?= __('User Id') ?></th>
                            <th><?= __('Created') ?></th>
                            <th><?= __('Modified') ?></th>
                            <th><?= __('Visible') ?></th>
                            <th><?= __('Status') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($category->products as $products) : ?>
                        <tr>
                            <td><?= h($products->id) ?></td>
                            <td><?= h($products->name) ?></td>
                            <td><?= h($products->category_id) ?></td>
                            <td><?= h($products->grouping_id) ?></td>
                            <td><?= h($products->price) ?></td>
                            <td><?= h($products->promotion_price) ?></td>
                            <td><?= h($products->promotion_start) ?></td>
                            <td><?= h($products->promotion_end) ?></td>
                            <td><?= h($products->user_id) ?></td>
                            <td><?= h($products->created) ?></td>
                            <td><?= h($products->modified) ?></td>
                            <td><?= h($products->visible) ?></td>
                            <td><?= h($products->status) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Products', 'action' => 'view', $products->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Products', 'action' => 'edit', $products->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Products', 'action' => 'delete', $products->id], ['confirm' => __('Are you sure you want to delete # {0}?', $products->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
