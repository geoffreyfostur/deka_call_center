<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Sale $sale
 * @var \Cake\Collection\CollectionInterface|string[] $customers
 * @var \Cake\Collection\CollectionInterface|string[] $caravanes
 * @var \Cake\Collection\CollectionInterface|string[] $zones
 * @var \Cake\Collection\CollectionInterface|string[] $routes
 * @var \Cake\Collection\CollectionInterface|string[] $users
 * @var \Cake\Collection\CollectionInterface|string[] $products
 */

$types = array(0 => "Dégustation", 1 => "Baisse de Prix", 2 => "Promotion Croisée", 3 => "Article Promotionel");
$total_ordered = 0;
$total_delivered = 0;
if(!empty($sale->products_sales)){
    foreach($sale->products_sales as $id => $product){
        $total_ordered = $total_ordered + $product->quantity_ordered*$product->price;
        $total_delivered = $total_delivered + $product->quantity_delivered*$product->price;
    }
}
?>
<div class="sales form content">
    <div class="row">
        <div class="col-md-12 col-xl-12">
            <div class="row">
                <div class="col-md-6">
                    <?= $this->Form->control('category_id', array("empty" => "-- Filtrer par Catégorie --", "label" => false, "options" => $categories_list, 'class' => "form-control category_filter")); ?>
                </div>
                <div class="col-md-6">
                    <label class="btn btn-success" data-toggle="modal" data-target="#submitModal" style="float:right;cursor:pointer;margin-left:7px">Valider</label>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" style="float:right">
                      Détails
                    </button>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive" style="width:100%">
                <table class="table datatable_show_all_edit table-striped table-hover" style="width:100%;margin-right:7px">
                    <thead>
                        <tr>
                            <th style="width:55%!important">Produit</th>
                            <th style="width:15%!important"></th>
                            <th style="width:15%!important;text-align:right">Commandé ($<span id="sale_total_ordered"><?= number_format($total_ordered, "2", ",", ",") ?></span>)</th>
                            <th style="width:15%!important;text-align:right">Livré ($<span id="sale_total_delivered"><?= number_format($total_delivered, "2", ",", ",") ?></span>)</th>
                        </tr>
                    </thead>
                    <tbody class="products_table" style="overflow-y:scroll;height:400px!important">
                        <?php foreach ($categories as $category): ?>
                            <?php foreach($category->products as $product) : ?>
                                <?php
                                $condition = false;  
                                $existing_product = null;
                                foreach($sale->products_sales as $ps){
                                    if($ps->product_id == $product->id){
                                        $condition = true;
                                        $existing_product = $ps;
                                    }
                                }
                                ?>
                                <?php if($condition) : ?>
                                    <tr class="<?= $category->id ?>" style="background:#d9edf7">
                                <?php else : ?>
                                    <tr class="<?= $category->id ?>">
                                <?php endif; ?>
                                
                                    <?php 
                                        $price = $product->price;
                                        $is_promo = false;
                                        if(!empty($promotions[$product->id])){
                                            $promo = $promotions[$product->id];
                                            if(empty($promo['zone_id']) || $promo['zone_id'] == $call['customer_info']['zone_id']){
                                                $is_promo = true;
                                                $promotion_label = "";
                                                $promotion_label .= $types[$promo['type']]." : ";
                                                if($promo['type'] == 0 || $promo['type'] == 1){
                                                    $promotion_label .= $promo['promotion_price'];
                                                }else{
                                                    $promotion_label .= "Achetez ".$promo['start_quantity']. " ";
                                                    $promotion_label .= "obtenez ".$promo['end_quantity']." ";
                                                    $promotion_label .= $promo['article']->name;
                                                }
                                            }
                                            
                                        }
                                    ?>
                                    <?php foreach($product->products_centers as $key => $pc) : ?>
                                        <?php 
                                            if($pc->center_id == $center_id){
                                                $price = $pc->price;
                                            }
                                        ?>
                                    <?php endforeach; ?>
                                    <?php if($is_promo) : ?>
                                    <td style="width:55%">
                                        <a href="#!" data-toggle="tooltip" data-placement="top" data-trigger="hover" title="" data-original-title="<?= $promotion_label ?>"><i class="ti-medall" style="background:#4680ff;color:white;padding:3px"></i></a>
                                         <?= $product->name ?></td>
                                    <?php else : ?>
                                        <td><?= $product->name ?></td>
                                    <?php endif; ?>
                                    <td style="width:15%"><?= number_format($price, 2, ".", ",") ?> USD</td>
                                    <td style="width:15%;text-align:center">
                                        <?php if($condition) : ?>
                                            <input type="text" name="quantity" class="form-control quantity_ordered quantity_edit" style="width:30px;height:15px;text-align:center;float:right;" placeholder="0" value = '<?= $existing_product->quantity_ordered ?>'> 
                                        <?php else : ?>
                                            <input type="text" name="quantity" class="form-control quantity_ordered quantity_edit" style="width:30px;height:15px;text-align:center;float:right;" placeholder="0"> 
                                        <?php endif; ?>
                                        <input type="hidden" name="product_id" class="product_id" value="<?= $product->id ?>"> <input type="hidden" name="product_name" class="product_name" value="<?= $product->name ?>"> <input type="hidden" name="price" class="price" value="<?= $price ?>">
                                    </td>
                                    <td style="width:15%">
                                        <?php if($condition) : ?>
                                            <input type="text" name="quantity_delivered" class="form-control quantity_edit quantity_delivered" style="width:30px;height:15px;text-align:center;float:right;" placeholder="0" value = '<?= $existing_product->quantity_delivered ?>'> 
                                        <?php else : ?>
                                            <input type="text" name="quantity_delivered" class="form-control quantity_edit quantity_delivered" style="width:30px;height:15px;text-align:center;float:right;" placeholder="0"> 
                                        <?php endif; ?>
                                        <input type="hidden" name="product_id" class="product_id" value="<?= $product->id ?>"> <input type="hidden" name="product_name" class="product_name" value="<?= $product->name ?>"> <input type="hidden" name="price" class="price" value="<?= $price ?>"></td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
                </div>
            </div>
            
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Détails Fiche</h5>
      </div>
      <div class="modal-body">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Produit</th>
                    <th class="text-center">Prix</th>
                    <th class="text-center">Commandé</th>
                    <th class="text-right">Livré</th>
                </tr>
            </thead>
            <tbody id="sale_details">
                <?php if(!empty($sale->products_sales)) : ?>
                    <?php foreach($sale->products_sales as $prd) : ?>
                        <tr>
                            <td><span style="cursor: pointer;padding:0px 10px;font-weight:bold;font-size:20px;float:left;margin-right:5px;background:red;color:white" class="delete_product" id = "<?= $prd->id ?>"> - </span><?= $prd->product->name ?></td>
                            <td class="text-center"><?= number_format($prd->price, 2, ".", ",") ?> USD</td>
                            <td class="text-center"><input type="text" name="quantity_ordered" class="form-control quantity_edit quantity_ordered" style="width:50px;height:25px;text-align:center;margin:auto" placeholder="0" value = '<?= $prd->quantity_ordered ?>'>
                                <input type="hidden" name="product_id" class="product_id" value="<?= $prd->product_id ?>"> <input type="hidden" name="product_name" class="product_name" value="<?= $prd->product->name ?>"> <input type="hidden" name="price" class="price" value="<?= $prd->price ?>"></td>
                            <td class="text-right"><input type="text" name="quantity_delivered" class="form-control quantity_edit quantity_delivered" style="width:50px;height:15px;text-align:center;margin:auto;float:right" placeholder="0" value = '<?= $prd->quantity_delivered ?>'><input type="hidden" name="product_id" class="product_id" value="<?= $prd->product_id ?>"> <input type="hidden" name="product_name" class="product_name" value="<?= $prd->product->name ?>"> <input type="hidden" name="price" class="price" value="<?= $prd->price ?>"></td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
            </tbody>
            <tfoot>
                <tr>
                    <th colspan="2">TOTAL</th>
                    <th class="text-center"><span id = "sale_details_total"><?= number_format($total_ordered, 2, ".", ",") ?></span> USD</th>
                    <th class="text-right"><span id = "sale_details_total2"><?= number_format($total_delivered, 2, ".", ",") ?></span> USD</th>
                </tr>
            </tfoot>
        </table>
        <hr>
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Articles Promotionnels</th>
                    <th class="text-center">Prix</th>
                    <th class="text-center">Commandé</th>
                    <th class="text-right">Livré</th>
                </tr>
            </thead>
            <tbody id="articles_details">
                <?php $atotal = 0; $atotal_delivered=0; if(!empty($sale->articles_sales)) : ?>
                <?php  foreach($sale->articles_sales as $article) : ?>
                <?php $atotal = $atotal + $article->price; $atotal_delivered = $atotal_delivered + $article->price_delivered;  ?>
                <tr style="vertical-align:middle">
                    <td style="vertical-align:middle"><input type="hidden" name="as_id" class="as_id" value = '<?= $article->id ?>'><span style="cursor: pointer;padding:0px 8px;font-weight:bold;font-size:20px;float:left;margin-right:5px;background:red;color:white" class="delete_article" id = "article_<?= $article->id ?>"> - </span> <?= $article->article->name ?></td>
                    <td  style="vertical-align:middle;text-align:center"><?= $article->article->price ?> USD</td>
                    <td class="text-center"><input type="text" name="article_qty_ordered" class="form-control article_edit article_qty_ordered" style="width:50px;height:25px;text-align:center;margin:auto" placeholder="0" value = '<?= $article->quantity ?>'></td>
                    <td class="text-right"><input type="text" name="article_qty_delivered" class="form-control article_edit article_qty_delivered" style="width:50px;height:25px;text-align:center;margin:auto;float:right" placeholder="0" value = '<?= $article->quantity_delivered ?>'></td>
                </tr>
                <?php endforeach; ?>
            <?php endif; ?>
            </tbody>
            <tfoot>
                <tr>
                    <th colspan="2">TOTAL</th>
                    <th class="text-center"><span id = "articles_details_total"><?= number_format($atotal, 2, ".", ",") ?></span> USD</th>
                    <th class="text-right"><span id = "articles_details_total_delivered"><?= number_format($atotal_delivered, 2, ".", ",") ?></span> USD</th>
                </tr>
            </tfoot>
        </table>
        <hr>
        <span class="label label-success" style="font-size:20px;float:left">Total Commandé : <span id="grand_total"><?= number_format($atotal+$total_ordered, 2, ".", ",") ?></span> USD</span>
        <span class="label label-success" style="font-size:20px;float:right">Total Livré : <span id="grand_total_delivered"><?= number_format($atotal_delivered+$total_delivered, 2, ".", ",") ?></span> USD</span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="submitModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Validation Proforma</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?= $this->Form->create(null, array("url" => "/sales/update")) ?>
      <input type="hidden" name="sale_id" value = "<?= $sale->id ?>" id = "sale_id">
      <div class="modal-body">
        <div class="row">
                <div class="col-md-12">
                    <?= $this->Form->control('created', array("placeholder" => "Date de Création", "label" => "Date de Création *", "type" => "date", 'class' => "form-control", "value" => date("Y-m-d", strtotime($sale->created)))); ?>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-6">
                    <?= $this->Form->control('delivered', array("label" => "Livré *", 'value' => $sale->delivered, 'class' => "form-control", "options" => array(0 => "Non", 1 => "Oui"))); ?>
                </div>
                <div class="col-md-6">
                    <?= $this->Form->control('delivery_date', array("placeholder" => "Date de Livraison", "label" => "Date de Livraison *", "type" => "date", 'class' => "form-control", "value" => date("Y-m-d", strtotime($sale->delivery_date)))); ?>
                </div>
            </div>
            <div class="row" style="margin-top:10px"><div class="col-md-6"></div>
                <div class="col-md-6">
                    <?php if(!empty($sale->last_delivery_date)) : ?>
                        <?= $this->Form->control('last_delivery_date', array("placeholder" => "Dernière Date de Livraison", "label" => "Dernière Date de Livraison *", "type" => "date", 'class' => "form-control", "value" => date("Y-m-d", strtotime($sale->last_delivery_date)))); ?>
                    <?php else : ?>
                        <?= $this->Form->control('last_delivery_date', array("placeholder" => "Dernière Date de Livraison", "label" => "Dernière Date de Livraison", "type" => "date", 'class' => "form-control")); ?>
                    <?php endif; ?>
                    
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-6 col-lg-6">
                    <?= $this->Form->control('problem', array("label" => "Problème *", 'value' => $sale->problem, 'class' => "form-control", "options" => array(0 => "Non", 1 => "Oui"))); ?>
                </div>
                <div class="col-md-6">
                    <?= $this->Form->control('note', array("placeholder" => "Ajouter une note", 'value' => $sale->note, "label" => "Note", "type" => "text", 'class' => "form-control")); ?>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-12">
                    <?= $this->Form->control('status', array("label" => "Statut *", 'value' => $sale->status, 'class' => "form-control", "options" => array(0 => "Annulé", 1 => "Actif"))); ?>
                </div>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
        <button type="submit" class="btn btn-success">Valider</button>
      </div>
      <?= $this->Form->end() ?>
    </div>
  </div>
</div>


<script type="text/javascript">
    $(".category_filter").change(function(){
        var to_hide = $(this).val();
        $(".products_table").find("tr").show();
        $(".products_table tr."+to_hide).show()
        $(".products_table tr").not("."+to_hide).hide();
    })

    $(".article_edit").change(function(){
        var element = $(this);
        var sale_id = $("#sale_id").val();
        var quantity = $(this).parent().parent().find(".article_qty_ordered").val();
        var quantity_delivered = $(this).parent().parent().find(".article_qty_delivered").val();
        var as_id = $(this).parent().parent().find(".as_id").val();
        var token =  $('input[name="_csrfToken"]').val();
        $.ajax({
            url : ROOT_DIRECTORY+'/sales/editarticle',
            type : 'POST',
            dataType: 'json',
            data : {as_id : as_id, sale_id : sale_id, quantity: quantity, quantity_delivered: quantity_delivered },
            headers : {
                'X-CSRF-Token': token 
            },
            success : function(data, statut){
                console.log(data)
                update_details(data);
                // $("#sale_total").text(parseFloat(data.total).toFixed(2))
                // element.parent().parent().css("background", "#d9edf7");
                // update_details(data);
            },
            error : function(resultat, statut, erreur){
                console.log(erreur);
            }, 
            complete : function(resultat, statut){
                console.log(resultat);
            }
        });
    })

    $(".quantity_edit").change(function(){
        element = $(this);
        var sale_id = $("#sale_id").val();
        var quantity = $(this).parent().parent().find(".quantity_ordered").val();
        var quantity_delivered = $(this).parent().parent().find(".quantity_delivered").val();
        var product_id = $(this).parent().find(".product_id").val();
        var price = $(this).parent().find(".price").val();
        var product_name = $(this).parent().find(".product_name").val();
        var token =  $('input[name="_csrfToken"]').val();
        if(isNaN(parseInt(quantity)) && isNaN(parseInt(quantity_delivered))){
            $(this).val("");
            $.ajax({
                url : ROOT_DIRECTORY+'/sales/addproductedit',
                type : 'POST',
                dataType: 'json',
                data : {price : price, product_id : product_id, quantity: 0, product_name: product_name, quantity_delivered : 0, sale_id : sale_id },
                headers : {
                    'X-CSRF-Token': token 
                },
                success : function(data, statut){
                    update_details(data);
                    // element.parent().parent().css("background", "")
                    // update_details(data);
                },
                error : function(resultat, statut, erreur){
                    console.log(erreur);
                }, 
                complete : function(resultat, statut){
                    console.log(resultat);
                }
            });
        }else{
            $.ajax({
                url : ROOT_DIRECTORY+'/sales/addproductedit',
                type : 'POST',
                dataType: 'json',
                data : {price : price, product_id : product_id, quantity: quantity, product_name: product_name, quantity_delivered : quantity_delivered, sale_id : sale_id },
                headers : {
                    'X-CSRF-Token': token 
                },
                success : function(data, statut){
                    update_details(data);
                    // $("#sale_total").text(parseFloat(data.total).toFixed(2))
                    // element.parent().parent().css("background", "#d9edf7");
                    // update_details(data);
                },
                error : function(resultat, statut, erreur){
                    console.log(erreur);
                }, 
                complete : function(resultat, statut){
                    console.log(resultat);
                }
            });
        }
    })

    function update_details(sale){
        var element = $("#sale_details");
        var articles_element = $("#articles_details");
        var products = sale.products_sales;
        var articles = sale.articles_sales;
        element.empty();

        articles_element.empty();

        var total_ordered = 0;
        var total_delivered = 0;

        var atotal_ordered = 0;
        var atotal_delivered = 0;

        Object.values(products).forEach(product => {
            total_ordered = total_ordered + parseFloat(product.price)*parseFloat(product.quantity_ordered);
            total_delivered = total_delivered + parseFloat(product.price)*parseFloat(product.quantity_delivered);
            var row = '<tr><td><span style="cursor: pointer;padding:0px 10px;font-weight:bold;font-size:20px;float:left;margin-right:5px;background:red;color:white" class="delete_product" id = "'+product.id+'"> - </span>'+product.product.name+'</td><td class="text-center">'+parseFloat(product.price).toFixed(2)+' USD</td><td><input type="text" name="quantity_ordered" class="form-control quantity_edit quantity_ordered" style="width:50px;height:25px;text-align:center;margin:auto" placeholder="0" value = '+product.quantity_ordered+'><input type="hidden" name="product_id" class="product_id" value="'+product.product.id+'"> <input type="hidden" name="product_name" class="product_name" value="'+product.product.name+'>"> <input type="hidden" name="price" class="price" value="'+product.product.price+'"></td><td class="text-right"><input type="text" name="quantity_delivered" class="form-control quantity_edit quantity_delivered" style="width:50px;height:25px;text-align:center;float:right" placeholder="0" value = '+product.quantity_delivered+'><input type="hidden" name="product_id" class="product_id" value="'+product.product.id+'"> <input type="hidden" name="product_name" class="product_name" value="'+product.product.name+'>"> <input type="hidden" name="price" class="price" value="'+product.product.price+'"></td></tr>';
            element.append(row);
        });


        Object.values(articles).forEach(article => {
            atotal_ordered = atotal_ordered + parseFloat(article.price);
            atotal_delivered = atotal_delivered + parseFloat(article.price_delivered);
            var row = '<tr style="vertical-align:middle"><td style="vertical-align:middle"><input type="hidden" name="as_id" class="as_id" value = "'+article.id+'"><span style="cursor: pointer;padding:0px 8px;font-weight:bold;font-size:20px;float:left;margin-right:5px;background:red;color:white" class="delete_article" id = "article_'+article.id+'"> - </span>'+article.article.name+'</td><td  style="vertical-align:middle;text-align:center">'+article.article.price+' USD</td><td class="text-center"><input type="text" name="article_qty_ordered" class="form-control article_edit article_qty_ordered" style="width:50px;height:25px;text-align:center;margin:auto" placeholder="0" value = "'+article.quantity+'"></td><td class="text-right"><input type="text" name="article_qty_delivered" class="form-control article_edit article_qty_delivered" style="width:50px;height:25px;text-align:center;margin:auto;float:right" placeholder="0" value = "'+article.quantity_delivered+'"></td></tr>';
            articles_element.append(row);
        });

        $("#sale_details_total").text(parseFloat(total_ordered).toFixed(2));
        $("#sale_details_total2").text(parseFloat(total_delivered).toFixed(2));

        $("#articles_details_total").text(parseFloat(atotal_ordered).toFixed(2));
        $("#articles_details_total_delivered").text(parseFloat(atotal_delivered).toFixed(2));

        $("#grand_total").text(parseFloat(sale.total_ordered).toFixed(2));
        $("#grand_total_delivered").text(parseFloat(sale.total_sold).toFixed(2));

        $("#sale_total_ordered").text(parseFloat(sale.total_ordered).toFixed(2));
        $("#sale_total_delivered").text(parseFloat(sale.total_sold).toFixed(2));

        $(".delete_product").click(function(){
            var ps_id = $(this).attr("id"); 
            delete_product(ps_id);
        })

        $(".delete_article").click(function(){
            var as_id = $(this).attr("id").split("_")[1]; 
            delete_article(as_id);
        })

        $(".article_edit").change(function(){
        var element = $(this);
        var sale_id = $("#sale_id").val();
        var quantity = $(this).parent().parent().find(".article_qty_ordered").val();
        var quantity_delivered = $(this).parent().parent().find(".article_qty_delivered").val();
        var as_id = $(this).parent().parent().find(".as_id").val();
        var token =  $('input[name="_csrfToken"]').val();
        $.ajax({
            url : ROOT_DIRECTORY+'/sales/editarticle',
            type : 'POST',
            dataType: 'json',
            data : {as_id : as_id, sale_id : sale_id, quantity: quantity, quantity_delivered: quantity_delivered },
            headers : {
                'X-CSRF-Token': token 
            },
            success : function(data, statut){
                console.log(data)
                update_details(data);
                // $("#sale_total").text(parseFloat(data.total).toFixed(2))
                // element.parent().parent().css("background", "#d9edf7");
                // update_details(data);
            },
            error : function(resultat, statut, erreur){
                console.log(erreur);
            }, 
            complete : function(resultat, statut){
                console.log(resultat);
            }
        });
    })

        $(".quantity_edit").change(function(){
        var element = $(this);
        var sale_id = $("#sale_id").val();
        var quantity = $(this).parent().parent().find(".quantity_ordered").val();
        var quantity_delivered = $(this).parent().parent().find(".quantity_delivered").val();
        var product_id = $(this).parent().find(".product_id").val();
        var price = $(this).parent().find(".price").val();
        var product_name = $(this).parent().find(".product_name").val();
        var token =  $('input[name="_csrfToken"]').val();
        if(isNaN(parseInt(quantity)) && isNaN(parseInt(quantity_delivered))){
            $(this).val("");
            $.ajax({
                url : ROOT_DIRECTORY+'/sales/addproductedit',
                type : 'POST',
                dataType: 'json',
                data : {price : price, product_id : product_id, quantity: 0, product_name: product_name, quantity_delivered : 0, sale_id : sale_id },
                headers : {
                    'X-CSRF-Token': token 
                },
                success : function(data, statut){
                    update_details(data);
                    // element.parent().parent().css("background", "")
                    // update_details(data);
                },
                error : function(resultat, statut, erreur){
                    console.log(erreur);
                }, 
                complete : function(resultat, statut){
                    console.log(resultat);
                }
            });
        }else{
            $.ajax({
                url : ROOT_DIRECTORY+'/sales/addproductedit',
                type : 'POST',
                dataType: 'json',
                data : {price : price, product_id : product_id, quantity: quantity, product_name: product_name, quantity_delivered : quantity_delivered, sale_id : sale_id },
                headers : {
                    'X-CSRF-Token': token 
                },
                success : function(data, statut){
                    update_details(data);
                    // $("#sale_total").text(parseFloat(data.total).toFixed(2))
                    // element.parent().parent().css("background", "#d9edf7");
                    // update_details(data);
                },
                error : function(resultat, statut, erreur){
                    console.log(erreur);
                }, 
                complete : function(resultat, statut){
                    console.log(resultat);
                }
            });
        }
    })
    }

    function delete_product(ps_id){
        var token =  $('input[name="_csrfToken"]').val();
        $.ajax({
            url : ROOT_DIRECTORY+'/sales/deleteproductedit',
            type : 'POST',
            dataType : 'json',
            data : {ps_id : ps_id},
            headers : {
                'X-CSRF-Token': token 
            },
            success : function(data, statut){
                update_details(data);
            },
            error : function(resultat, statut, erreur){
                console.log(erreur);
            }, 
            complete : function(resultat, statut){
                console.log(resultat);
            }
        });
    }


    function delete_article(as_id){
        var token =  $('input[name="_csrfToken"]').val();
        $.ajax({
            url : ROOT_DIRECTORY+'/sales/deletearticleedit',
            type : 'POST',
            dataType : 'json',
            data : {as_id : as_id},
            headers : {
                'X-CSRF-Token': token 
            },
            success : function(data, statut){
                update_details(data);
            },
            error : function(resultat, statut, erreur){
                console.log(erreur);
            }, 
            complete : function(resultat, statut){
                console.log(resultat);
            }
        });
    }

    $(".delete_product").click(function(){
        var ps_id = $(this).attr("id"); 
        delete_product(ps_id);
    })

    $(".delete_article").click(function(){
        var as_id = $(this).attr("id").split("_")[1]; 
        delete_article(as_id);
    })
</script>

<style type="text/css">
    .dataTables_scroll
{
    overflow:auto;
}
</style>