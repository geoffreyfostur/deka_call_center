<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Sale $sale
 * @var \Cake\Collection\CollectionInterface|string[] $customers
 * @var \Cake\Collection\CollectionInterface|string[] $caravanes
 * @var \Cake\Collection\CollectionInterface|string[] $zones
 * @var \Cake\Collection\CollectionInterface|string[] $routes
 * @var \Cake\Collection\CollectionInterface|string[] $users
 * @var \Cake\Collection\CollectionInterface|string[] $products
 */
$types = array(0 => "Dégustation", 1 => "Baisse de Prix", 2 => "Promotion Croisée", 3 => "Article Promotionel");
$total = 0;
if(!empty($sale['products'])){
    foreach($sale['products'] as $id => $product){
        $total = $total + $product['quantity']*$product['price'];
    }
}
?>
<div class="row">
    <div class="col-lg-12 col-xl-12">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs md-tabs " role="tablist">
            <li class="nav-item">
                <?php if(empty($sale['caravane_id'])) : ?>
                    <a class="nav-link active" data-toggle="tab" href="#home7" role="tab"><i class="ti ti-truck"></i> Caravane</a>
                <?php else : ?>
                    <a class="nav-link" data-toggle="tab" href="#home7" role="tab"><i class="ti ti-truck"></i> Caravane (<?= $sale['caravane_info']->identification ?>)</a>
                <?php endif; ?>
                
                <div class="slide"></div>
            </li>
            <li class="nav-item">
                <?php if(empty($sale['customer_id'])) : ?>
                    <?php if(empty($sale['caravane_id'])) : ?>
                        <a class="nav-link" href="#" role="tab"><i class="ti ti-user"></i> Client</a>
                    <?php else : ?>
                        <a class="nav-link active" data-toggle="tab" href="#profile7" role="tab"><i class="ti ti-user"></i> Client</a>
                    <?php endif; ?>
                    
                <?php else : ?>
                    <a class="nav-link" data-toggle="tab" href="#profile7" role="tab"><i class="ti ti-user"></i> Client (<?= $sale['customer_info']->name ?>)</a>
                <?php endif; ?>
                <div class="slide"></div>
            </li>
            <li class="nav-item">
                <?php if(!empty($sale['customer_id']) && !empty($sale['caravane_id'])) : ?>
                    <a class="nav-link active" data-toggle="tab" href="#messages7" role="tab"><i class="ti ti-microphone"></i> Vente</a>
                <?php else : ?>
                    <a class="nav-link" href="#" role="tab"><i class="ti ti-microphone"></i> Vente</a>
                <?php endif; ?>
                
                <div class="slide"></div>
            </li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content card-block">
            <?php if(empty($sale['caravane_id'])) : ?>
                <div class="tab-pane active" id="home7" role="tabpanel">
                    <?php echo $this->element('sale_caravanes', array('caravanes' => $caravanes, 'selected' => '')); ?>
                </div>
            <?php else : ?>
                <div class="tab-pane" id="home7" role="tabpanel">
                    <?php echo $this->element('sale_caravanes', array('caravanes' => $caravanes, 'selected' => $sale['caravane_id'])); ?>
                </div>
            <?php endif; ?>


            <?php if(empty($sale['customer_id'])) : ?>
                <?php if(empty($sale['caravane_id'])) : ?>
                    <div class="tab-pane" id="profile7" role="tabpanel">
                    <?php echo $this->element('sale_customers', array('customers' => $customers, 'selected' => '')); ?>
                </div>
                <?php else : ?>
                    <div class="tab-pane active" id="profile7" role="tabpanel">
                    <?php echo $this->element('sale_customers', array('customers' => $customers, 'selected' => '')); ?>
                </div>
                <?php endif; ?>
                
            <?php else : ?>
                <div class="tab-pane" id="profile7" role="tabpanel">
                    <?php echo $this->element('sale_customers', array('customers' => $customers, 'selected' => $sale['customer_id'])); ?>
                </div>
            <?php endif; ?>

            <?php if(!empty($sale['customer_id']) && !empty($sale['caravane_id'])) : ?>
                <div class="tab-pane active" id="messages7" role="tabpanel">
                    <?php echo $this->element('direct_sale', array('sale' => $sale, 'customer' => $sale['customer_info'])); ?>
                </div>
            <?php else : ?>
                <div class="tab-pane" id="messages7" role="tabpanel">
                    <?php echo $this->element('direct_sale', array('sale' => $sale, 'customer' => $sale['customer_info'])); ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>