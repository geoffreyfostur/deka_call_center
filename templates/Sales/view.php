<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Sale $sale
 */
$status = array(0 => "Canceled", 1 => "Active");
$types = array(0 => "Dégustation", 1 => "Baisse de Prix", 2 => "Promotion Croisée", 3 => "Article Promotionel");
$sale_types = array(1 => "Presale", 2 => "Vente", 3 => "Vente Directe");
?>
<div class="row">
    <div class="col-md-12">
        <div class="sales view content">
            <table class="table table-striped">
                <tr>
                    <th>#</th>
                    <td><?= h($sale->sale_number) ?></td>
                </tr>
                <tr>
                    <th><?= __('Type') ?></th>
                    <?php if($sale->type == 1) : ?>
                        <td><span class="label label-primary">PROFORMA</span></td>
                    <?php elseif($sale->type == 2) : ?>
                        <td><span class="label label-success">VENTE</span></td>
                    <?php else : ?>
                        <td><span class="label label-warning">VENTE DIRECTE</span></td>
                    <?php endif; ?>
                </tr>
                <tr>
                    <th>Client</th>
                    <td><?= $sale->customer->name ?></td>
                </tr>
                <tr>
                    <th><?= __('Caravane') ?></th>
                    <td><?= $sale->caravane->identification ?></td>
                </tr>
                <tr>
                    <th><?= __('Zone') ?></th>
                    <td><?= $sale->zone->name ?></td>
                </tr>
                <?php if(!empty($sale->note)) : ?>
                <tr>
                    <th><?= __('Note') ?></th>
                    <td><?= h($sale->note) ?></td>
                </tr>
                <?php if($sale->problem == 1) : ?>
                <tr>
                    <th><?= __('Problem') ?></th>
                    <td><span class="label label-danger">OUI</span></td>
                </tr>
                <?php endif; ?>
                <?php endif; ?>
                <tr>
                    <th>Statut</th>
                    <?php if($sale->status == 1) : ?>
                    <td><span class="label label-success">ACTIF</span></td>
                    <?php else : ?>
                        <td><span class="label label-danger">ANNULE</span></td>
                    <?php endif; ?>
                </tr>
                <tr>
                    <th>Livré</th>
                    <?php if($sale->delivered == 1) : ?>
                    <td><span class="label label-success">OUI</span></td>
                    <?php else : ?>
                        <td><span class="label label-danger">NON</span></td>
                    <?php endif; ?>
                </tr>
                <tr>
                    <th>Date de Livraison</th>
                    <td><?= h($sale->delivery_date) ?></td>
                </tr>
                <?php if(!empty($sale->last_delivery_date)) : ?>
                <tr>
                    <th>Dernière tentative de Livraison</th>
                    <td><?= h($sale->last_delivery_date) ?></td>
                </tr>
            <?php endif; ?>
                <tr>
                    <th>Total Commandé</th>
                    <td><?= number_format($sale->total_ordered, 2, ".", ",") ?> USD</td>
                </tr>
                
                <tr>
                    <th>Total Livré</th>
                    <td><?= number_format($sale->total_sold, 2, ".", ",") ?> USD</td>
                </tr>
            </table>
            <hr>
            <div class="related">
                <?php if (!empty($sale->products)) : ?>
                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <tr style="    background: #4680ff;
    color: white;">
                            <th>Produit</th>
                            <th class="text-center">Prix Listé</th>
                            <th class="text-center">Prix Réel</th>
                            <th class="text-center">Quantité Comandé</th>
                            <th class="text-center">Quantité Livré</th>
                            <th class="text-center">Total Commandé</th>
                            <th class="text-right">Total Livré</th>
                        </tr>
                        <?php foreach ($sale->products as $product) : ?>
                        <tr>
                            <td><?= h($product->name) ?></td>
                            <td class="text-center"><?= number_format($product['_joinData']->list_price, 2, ".", ",") ?></td>
                            <td class="text-center"><?= number_format($product['_joinData']->price, 2, ".", ",") ?></td>
                            <td class="text-center"><?= h($product['_joinData']->quantity_ordered) ?></td>
                            <td class="text-center"><?= h($product['_joinData']->quantity_delivered) ?></td>
                            <td class="text-center"><?= number_format($product['_joinData']->total_ordered, 2, ".", ",") ?></td>
                            <td class="text-right"><?= number_format($product['_joinData']->total_delivered, 2, ".", ",") ?></td>
                         
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>

        </div> 
        <hr>
        <div class="related">
            <?php if (!empty($sale->articles_sales)) : ?>
            <div class="table-responsive">
                <table class="table table-striped table-hover">
                    <tr>
                        <th>Articles Promotionnels</th>
                        <th class="text-center">Quantité Comandé</th>
                        <th class="text-center">Quantité Livrée</th>
                        <th class="text-center">Total Comandé</th>
                        <th class="text-right">Total Livré</th>
                    </tr>
                    <?php foreach ($sale->articles_sales as $article) : ?>
                    <tr>
                        <td><?= h($article->article->name) ?></td>
                        <td class="text-center"><?= $article->quantity ?></td>
                        <td class="text-center"><?= $article->quantity_delivered ?></td>
                        <td class="text-center"><?= number_format($article->price, 2, ".", ",") ?></td>
                        <td class="text-right"><?= number_format($article->price_delivered, 2, ".", ",") ?></td>
                    </tr>
                    <?php endforeach; ?>
                </table>
            </div>
            <?php endif; ?>
        </div>            
    </div>
       
    </div>
</div>
