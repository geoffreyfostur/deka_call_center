<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Sale[]|\Cake\Collection\CollectionInterface $sales
 */
$types = array(1 => "PROFORMA", 2 => "VENTE", 3 => "VENTE DIRECTE", 4 => "ANNULE");
?>
<?= $this->Form->create() ?>
<div class="row">
    <div class="col-md-3"><?= $this->Form->control('caravane_id', array("empty" => "-- Caravane --", "label" => false, 'class' => "form-control", 'options' => $caravanes, 'value' => $caravane_id)); ?></div>
    <div class="col-md-3"><?= $this->Form->control('type', array("empty" => "-- Type --", "label" => false, 'class' => "form-control", 'options' => $types, 'value' => $type)); ?></div>
    <div class="col-md-3"><?= $this->Form->control('customer_id', array("empty" => "-- Client --", "label" => false, 'class' => "form-control selectpicker", 'options' => $customers, 'value' => $customer_id)); ?></div>
    <div class="col-md-1"><?= $this->Form->button('', array(
    "class" => "btn btn-success float-right ti-check", 
    'escape' => false,
    'style' => 'height:39px'
)); ?></div>
</div>
<?= $this->Form->end() ?>
<div class="caravanes index content">
    <div class="table-responsive mt-10">
        <table class="table datatable">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Type</th>
                    <th>Client</th>
                    <th>Agent</th>
                    <th>Caravane</th>
                    <th>Zone</th>
                    <th>Total Commandé</th>
                    <th>Total Livré</th>
                    <th>Date</th>
                    <th>Heure</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php $count = 0; $ordered = 0; $sold = 0; foreach ($sales as $sale): ?>
                <?php 
                $count = $count + 1;
                    $ordered = $ordered + $sale->total_ordered;
                    $sold = $sold + $sale->total_sold;
                ?>
                <tr>
                    <td data-toggle="tooltip" data-placement="top" data-trigger="hover" title="" data-original-title="<?= $sale->note ?>"><a href="<?= ROOT_DIREC ?>/sales/view/<?= $sale->id ?>"><?= h($sale->sale_number) ?></a></td>
                    <?php if($sale->type == 1) : ?>
                        <td><span class="label label-primary">PROFORMA</span></td>
                    <?php elseif($sale->type == 2) : ?>
                        <td><span class="label label-success">VENTE</span></td>
                    <?php else : ?>
                        <td><span class="label label-warning">VENTE DIRECTE</span></td>
                    <?php endif; ?>
                    <td><?= h($sale->customer->name) ?></td>
                    <td><?= h($sale->user->username) ?></td>
                    <td><?= h($sale->caravane->identification) ?></td>
                    <td><?= h($sale->zone->name) ?></td>
                    <td><?= number_format($sale->total_ordered, 2, ".", ",") ?> USD</td>
                    <td><?= number_format($sale->total_sold, 2, ".", ",") ?> USD</td>
                    <td><?= date("Y-m-d", strtotime($sale->created)) ?></td>
                    <td><?= date("H:i", strtotime($sale->created)) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('<i class="ti-pencil"></i>'), ['action' => 'edit', $sale->id], ['class' => 'edit_link', 'escape' => false]) ?>
                        <?= $this->Form->postLink(__('<i class="ti-trash"></i>'), ['action' => 'delete', $sale->id], ['class' => 'delete_link', 'escape' => false, 'confirm' => __('Etes-vous sur de vouloir supprimer la vente {0} ?', $sale->sale_number)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
            <tfoot> 
                <tr>    
                    <th>Total (<?= $count ?>)</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>

                    <th><?= number_format($ordered, 2, ".", ",") ?> USD</th>
                    <th><?= number_format($sold, 2, ".", ",") ?> USD</th>

                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
