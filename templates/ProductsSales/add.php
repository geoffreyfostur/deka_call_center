<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ProductsSale $productsSale
 * @var \Cake\Collection\CollectionInterface|string[] $products
 * @var \Cake\Collection\CollectionInterface|string[] $sales
 * @var \Cake\Collection\CollectionInterface|string[] $promotions
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Products Sales'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="productsSales form content">
            <?= $this->Form->create($productsSale) ?>
            <fieldset>
                <legend><?= __('Add Products Sale') ?></legend>
                <?php
                    echo $this->Form->control('product_id', ['options' => $products]);
                    echo $this->Form->control('sale_id', ['options' => $sales]);
                    echo $this->Form->control('list_price');
                    echo $this->Form->control('price');
                    echo $this->Form->control('quantity_ordered');
                    echo $this->Form->control('quantity_delivered');
                    echo $this->Form->control('total_ordered');
                    echo $this->Form->control('total_delivered');
                    echo $this->Form->control('promotion_id', ['options' => $promotions, 'empty' => true]);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
