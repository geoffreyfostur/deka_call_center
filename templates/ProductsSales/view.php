<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ProductsSale $productsSale
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Products Sale'), ['action' => 'edit', $productsSale->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Products Sale'), ['action' => 'delete', $productsSale->id], ['confirm' => __('Are you sure you want to delete # {0}?', $productsSale->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Products Sales'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Products Sale'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="productsSales view content">
            <h3><?= h($productsSale->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Product') ?></th>
                    <td><?= $productsSale->has('product') ? $this->Html->link($productsSale->product->name, ['controller' => 'Products', 'action' => 'view', $productsSale->product->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Sale') ?></th>
                    <td><?= $productsSale->has('sale') ? $this->Html->link($productsSale->sale->id, ['controller' => 'Sales', 'action' => 'view', $productsSale->sale->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Promotion') ?></th>
                    <td><?= $productsSale->has('promotion') ? $this->Html->link($productsSale->promotion->id, ['controller' => 'Promotions', 'action' => 'view', $productsSale->promotion->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($productsSale->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('List Price') ?></th>
                    <td><?= $this->Number->format($productsSale->list_price) ?></td>
                </tr>
                <tr>
                    <th><?= __('Price') ?></th>
                    <td><?= $this->Number->format($productsSale->price) ?></td>
                </tr>
                <tr>
                    <th><?= __('Quantity Ordered') ?></th>
                    <td><?= $this->Number->format($productsSale->quantity_ordered) ?></td>
                </tr>
                <tr>
                    <th><?= __('Quantity Delivered') ?></th>
                    <td><?= $this->Number->format($productsSale->quantity_delivered) ?></td>
                </tr>
                <tr>
                    <th><?= __('Total Ordered') ?></th>
                    <td><?= $this->Number->format($productsSale->total_ordered) ?></td>
                </tr>
                <tr>
                    <th><?= __('Total Delivered') ?></th>
                    <td><?= $this->Number->format($productsSale->total_delivered) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
