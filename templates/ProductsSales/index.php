<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ProductsSale[]|\Cake\Collection\CollectionInterface $productsSales
 */
?>
<div class="productsSales index content">
    <?= $this->Html->link(__('New Products Sale'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Products Sales') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('product_id') ?></th>
                    <th><?= $this->Paginator->sort('sale_id') ?></th>
                    <th><?= $this->Paginator->sort('list_price') ?></th>
                    <th><?= $this->Paginator->sort('price') ?></th>
                    <th><?= $this->Paginator->sort('quantity_ordered') ?></th>
                    <th><?= $this->Paginator->sort('quantity_delivered') ?></th>
                    <th><?= $this->Paginator->sort('total_ordered') ?></th>
                    <th><?= $this->Paginator->sort('total_delivered') ?></th>
                    <th><?= $this->Paginator->sort('promotion_id') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($productsSales as $productsSale): ?>
                <tr>
                    <td><?= $this->Number->format($productsSale->id) ?></td>
                    <td><?= $productsSale->has('product') ? $this->Html->link($productsSale->product->name, ['controller' => 'Products', 'action' => 'view', $productsSale->product->id]) : '' ?></td>
                    <td><?= $productsSale->has('sale') ? $this->Html->link($productsSale->sale->id, ['controller' => 'Sales', 'action' => 'view', $productsSale->sale->id]) : '' ?></td>
                    <td><?= $this->Number->format($productsSale->list_price) ?></td>
                    <td><?= $this->Number->format($productsSale->price) ?></td>
                    <td><?= $this->Number->format($productsSale->quantity_ordered) ?></td>
                    <td><?= $this->Number->format($productsSale->quantity_delivered) ?></td>
                    <td><?= $this->Number->format($productsSale->total_ordered) ?></td>
                    <td><?= $this->Number->format($productsSale->total_delivered) ?></td>
                    <td><?= $productsSale->has('promotion') ? $this->Html->link($productsSale->promotion->id, ['controller' => 'Promotions', 'action' => 'view', $productsSale->promotion->id]) : '' ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $productsSale->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $productsSale->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $productsSale->id], ['confirm' => __('Are you sure you want to delete # {0}?', $productsSale->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
