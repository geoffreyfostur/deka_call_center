<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ProductsSale $productsSale
 * @var string[]|\Cake\Collection\CollectionInterface $products
 * @var string[]|\Cake\Collection\CollectionInterface $sales
 * @var string[]|\Cake\Collection\CollectionInterface $promotions
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $productsSale->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $productsSale->id), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('List Products Sales'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="productsSales form content">
            <?= $this->Form->create($productsSale) ?>
            <fieldset>
                <legend><?= __('Edit Products Sale') ?></legend>
                <?php
                    echo $this->Form->control('product_id', ['options' => $products]);
                    echo $this->Form->control('sale_id', ['options' => $sales]);
                    echo $this->Form->control('list_price');
                    echo $this->Form->control('price');
                    echo $this->Form->control('quantity_ordered');
                    echo $this->Form->control('quantity_delivered');
                    echo $this->Form->control('total_ordered');
                    echo $this->Form->control('total_delivered');
                    echo $this->Form->control('promotion_id', ['options' => $promotions, 'empty' => true]);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
