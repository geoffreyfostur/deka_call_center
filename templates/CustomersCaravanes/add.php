<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CustomersCaravane $customersCaravane
 * @var \Cake\Collection\CollectionInterface|string[] $caravanes
 * @var \Cake\Collection\CollectionInterface|string[] $customers
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Customers Caravanes'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="customersCaravanes form content">
            <?= $this->Form->create($customersCaravane) ?>
            <fieldset>
                <legend><?= __('Add Customers Caravane') ?></legend>
                <?php
                    echo $this->Form->control('caravane_id', ['options' => $caravanes]);
                    echo $this->Form->control('customer_id', ['options' => $customers]);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
