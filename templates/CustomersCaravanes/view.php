<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CustomersCaravane $customersCaravane
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Customers Caravane'), ['action' => 'edit', $customersCaravane->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Customers Caravane'), ['action' => 'delete', $customersCaravane->id], ['confirm' => __('Are you sure you want to delete # {0}?', $customersCaravane->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Customers Caravanes'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Customers Caravane'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="customersCaravanes view content">
            <h3><?= h($customersCaravane->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Caravane') ?></th>
                    <td><?= $customersCaravane->has('caravane') ? $this->Html->link($customersCaravane->caravane->name, ['controller' => 'Caravanes', 'action' => 'view', $customersCaravane->caravane->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Customer') ?></th>
                    <td><?= $customersCaravane->has('customer') ? $this->Html->link($customersCaravane->customer->name, ['controller' => 'Customers', 'action' => 'view', $customersCaravane->customer->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($customersCaravane->id) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
