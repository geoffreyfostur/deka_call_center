<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CustomersCaravane $customersCaravane
 * @var string[]|\Cake\Collection\CollectionInterface $caravanes
 * @var string[]|\Cake\Collection\CollectionInterface $customers
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $customersCaravane->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $customersCaravane->id), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('List Customers Caravanes'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="customersCaravanes form content">
            <?= $this->Form->create($customersCaravane) ?>
            <fieldset>
                <legend><?= __('Edit Customers Caravane') ?></legend>
                <?php
                    echo $this->Form->control('caravane_id', ['options' => $caravanes]);
                    echo $this->Form->control('customer_id', ['options' => $customers]);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
