<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CustomersCaravane[]|\Cake\Collection\CollectionInterface $customersCaravanes
 */
?>
<div class="customersCaravanes index content">
    <?= $this->Html->link(__('New Customers Caravane'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Customers Caravanes') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('caravane_id') ?></th>
                    <th><?= $this->Paginator->sort('customer_id') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($customersCaravanes as $customersCaravane): ?>
                <tr>
                    <td><?= $this->Number->format($customersCaravane->id) ?></td>
                    <td><?= $customersCaravane->has('caravane') ? $this->Html->link($customersCaravane->caravane->name, ['controller' => 'Caravanes', 'action' => 'view', $customersCaravane->caravane->id]) : '' ?></td>
                    <td><?= $customersCaravane->has('customer') ? $this->Html->link($customersCaravane->customer->name, ['controller' => 'Customers', 'action' => 'view', $customersCaravane->customer->id]) : '' ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $customersCaravane->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $customersCaravane->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $customersCaravane->id], ['confirm' => __('Are you sure you want to delete # {0}?', $customersCaravane->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
